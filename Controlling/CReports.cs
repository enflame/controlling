﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using Word = Microsoft.Office.Interop.Word;
using System.Reflection;
using System.Data;
using Modules;

using System.Windows.Media.TextFormatting;
using CustomControls;
using MySql;
using GeneralClasses;
namespace Controlling
{
  class CReports
  {
    Word._Application _application;
    Word._Document _document;

    Object missingObj = System.Reflection.Missing.Value;
    Object trueObj = true;
    Object falseObj = false;
    List<MergeData> _mergeData;

    string _pathReports;
    public CReports(string pathReports)
    {
      _application = null;
      _document = null;

      _pathReports = pathReports;
    }
    //public void CellMerge(ref Word.Table tbl, Rectangle rect)
    //{

    //  string text = tbl.Cell(rect.Y, rect.X).Range.Text;
    //  Word.Font font = tbl.Cell(rect.Y, rect.X).Range.Font;
    //  Word.WdParagraphAlignment alignement = tbl.Cell(rect.Y, rect.X).Range.ParagraphFormat.Alignment;
    //  tbl.Cell(rect.Y, rect.X).Merge(tbl.Cell(rect.Bottom, rect.Right));
    //  tbl.Cell(rect.Y, rect.X).Range.Text = text;
    //  tbl.Cell(rect.Y, rect.X).Range.Font = font;
    //  tbl.Cell(rect.Y, rect.X).Range.ParagraphFormat.Alignment = alignement;
    //}

    public bool FindReplace(string find, string replacement)
    {
      try
      {
        object replaceAll = Word.WdReplace.wdReplaceAll;
        _application.Selection.Find.ClearFormatting();
        _application.Selection.Find.Text = find;
        _application.Selection.Find.Replacement.ClearFormatting();
        _application.Selection.Find.Replacement.Text = replacement;
        _application.Selection.Find.Execute(
            ref missingObj, ref missingObj, ref missingObj, ref missingObj, ref missingObj,
            ref missingObj, ref missingObj, ref missingObj, ref missingObj, ref missingObj,
            ref replaceAll, ref missingObj, ref missingObj, ref missingObj, ref missingObj);
        return true;
      }
      catch
      {
        return false;
      }
    }
    
    List<RowData> FillActions(int num_collumn, string idSC, CMySql mySql, bool isMerge)
    {
      Word.Table table = _document.Tables[1];

      string sql = "SELECT form.fio, SC_Actions.id, SC_Actions.idSC, SC_Actions.name1, SC_Actions.task, SC_Actions.BeginDate, SC_Actions.EndDate FROM form " +
        " INNER JOIN SC_Actions ON form.id = SC_Actions.IDOwnPerson where idSC=" + idSC + " and SC_Actions.targetyear = " + UserInfo.TargetYear.ToString(); ;

      //string sql = "select * from SC_Actions where idSC=" + idSC;
      DataTable dt_actions = mySql.SqlSelect(sql);

      Word.Row row = _document.Tables[1].Rows[_document.Tables[1].Rows.Count];
      List<RowData> res = new List<RowData>();

      for (int i = 0; i < dt_actions.Rows.Count; i++)
      {
        DataRow dr_action = dt_actions.Rows[i];
        row.Cells[num_collumn].Range.Text = dr_action["name1"].ToString();
        row.Cells[num_collumn+1].Range.Text = dr_action["task"].ToString();
        row.Cells[num_collumn + 2].Range.Text = DateTime.Parse(dr_action["BeginDate"].ToString()).ToShortDateString() + "\r\n" + DateTime.Parse(dr_action["EndDate"].ToString()).ToShortDateString();
        row.Cells[num_collumn + 3].Range.Text = dr_action["fio"].ToString();

        res.Add(new RowData() { index = _document.Tables[1].Rows.Count, id_parent = dr_action["id"].ToString() });

        if (i < dt_actions.Rows.Count - 1) row = table.Rows.Add();
      }

      if ((isMerge) & (dt_actions.Rows.Count > 0))
      {
        _mergeData.Add(new MergeData() { r_from = res[0].index, c_from = num_collumn - 1, r_to = res[res.Count - 1].index, c_to = num_collumn - 1 });
       }
      return res;
    }
    List<RowData> FillScorecardBranch(int num_collumn, string id_root, CMySql mySql, bool isMerge)
    {
      Word.Table table = _document.Tables[1];

      string sql = "select * from SCTree where id_parent=" + id_root + " and targetyear = " + UserInfo.TargetYear.ToString(); ;
      DataTable dt_goals = mySql.SqlSelect(sql);

      Word.Row row = _document.Tables[1].Rows[_document.Tables[1].Rows.Count];
      int counter = 1;
      List<RowData> res = new List<RowData>();

      for (int i = 0; i < dt_goals.Rows.Count; i++)
      {
        DataRow dr_goal = dt_goals.Rows[i];
        sql = "select * from SC where id=" + dr_goal["id_SC"].ToString() + " where targetyear = " + UserInfo.TargetYear.ToString(); 
        DataTable dt_SC_goal = mySql.SqlSelect(sql);

        DataRow dr_SC_goal = dt_SC_goal.Rows[0];//одна строка
        row.Cells[num_collumn].Range.Text = dr_SC_goal["name1"].ToString();
        res.Add(new RowData() { index = _document.Tables[1].Rows.Count, id_parent = dr_goal["id_SC"].ToString() });

        FillActions(num_collumn + 1, dr_goal["id_SC"].ToString(), mySql, true);
        List<RowData> rds = FillScorecardBranch(num_collumn + 1, dr_goal["id_SC"].ToString(), mySql, true);


        if (i < dt_goals.Rows.Count - 1) row = table.Rows.Add();
        //        table.Cell(row.Index, 1).Merge(table.Cell(row.Index + colllumns-1, 1));
        counter++;
      }

      if ((isMerge) & (dt_goals.Rows.Count > 0))
      {
        _mergeData.Add(new MergeData() { r_from = res[0].index, c_from = num_collumn - 1, r_to = res[res.Count - 1].index, c_to = num_collumn - 1 });
      }
      return res;
    }

    public void MakePlanIDO(CMySql mySql)
    {
      _application = new Word.Application();
      // создаем путь к файлу
      Object templatePathObj = _pathReports + "\\Templates\\План работы 2.dotx";

      // если вылетим не этом этапе, приложение останется открытым
      try
      {
        _document = _application.Documents.Add(ref  templatePathObj, ref missingObj, ref missingObj, ref missingObj);
      }
      catch (Exception error)
      {
        _document.Close(ref falseObj, ref  missingObj, ref missingObj);
        _application.Quit(ref missingObj, ref  missingObj, ref missingObj);
        _document = null;
        _application = null;
        throw error;
      }
      _application.Visible = true;
      Word.Table table = _document.Tables[1];
      _mergeData = new List<MergeData>();
      _mergeData.Add(new MergeData() { r_from = 1, c_from = 1, r_to = 1, c_to = 2 });

      string sql = "select * from SCTree where id_parent=0 and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt_persp = mySql.SqlSelect(sql);
      int counter = 1;
      Word.Row row = table.Rows[2];
      foreach (DataRow dr in dt_persp.Rows)
      {
        sql = "select * from SC where id=" + dr["id_SC"].ToString() + " and targetyear = " + UserInfo.TargetYear.ToString();
        DataTable dt_SC = mySql.SqlSelect(sql);
        DataRow dr_SC = dt_SC.Rows[0];//одна строка
        row.Cells[1].Range.Text = "Стратегическая цель №" + counter.ToString() + ": " + dr_SC["name1"].ToString();
        _mergeData.Add(new MergeData() { r_from = row.Index, c_from = 1, r_to = row.Index, c_to = row.Cells.Count });

        row = table.Rows.Add();

        List<RowData> goals = FillScorecardBranch(1, dr["id_SC"].ToString(), mySql, false);

        object beforeRow = table.Rows[1];

        row = table.Rows.Add();

        counter++;
      }
      foreach (MergeData mg in _mergeData)
      {
        table.Cell(mg.r_from, mg.c_from).Merge(table.Cell(mg.r_to, mg.c_to));
      }

      FindReplace("@year@", DateTime.Now.Year.ToString());
    }
    class RowData
    {
      public int index { get; set; }
      public string id_parent { get; set; }
    }
    class MergeData
    {
      public int r_from { get; set; }
      public int c_from { get; set; }
      public int r_to { get; set; }
      public int c_to { get; set; }
    }
  }
}
