﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Net;
using GeneralClasses;
namespace Controlling
{
  /// <summary>
  /// Логика взаимодействия для wndSettings.xaml
  /// </summary>
  public partial class wndSettings : Window
  {

    public wndSettings()
    {
      InitializeComponent();
      mailSettings1.Set(ref UserInfo.SendMail);
    }

    private void b_apply_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = true;
    }

    private void b_close_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = false;
    }
  }
}
