﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

using Modules;
using System.IO;
namespace Controlling
{
  /// <summary>
  /// Логика взаимодействия для P_DepEdit.xaml
  /// </summary>
  public partial class P_DepEdit : Window
  {
    readonly CMySql _mySql;

    ComboBox cb_staff;
    ComboBox cb_positions;
    TextBox tb_email = new TextBox();
    StackPanel sp_staff_edit_item;

    DataTable _dt_form;
    DataTable _dt_positions;
    bool isAddingNewStaff; //активируется при добвалении нового пользователя
    bool isChangingNewStaff; //активируется при изменении нового пользователя

    TreeViewItem tViewSelectedItem;
    TreeViewItem tViewEditingItem;
    ListBoxItem lbSelectedItem;
    StackPanel sp_lbSelectedItem; //панель чтения

    public P_DepEdit(CMySql mySql)
    {
      InitializeComponent();

      _mySql = mySql;
      isAddingNewStaff = false;
      isChangingNewStaff = false;
      string sql;
      {
        //_mySql.SqlExec("delete from form");
        //string[] str = File.ReadAllLines("fio.txt", Encoding.GetEncoding(1251));
        //foreach (string s in str)
        //{
        //  sql = "insert into form (fio) values ('" + s + "')";
        //  _mySql.SqlExec(sql);
        //}
      }

      lBox.Items.Clear();
      cb_staff = new ComboBox(); cb_staff.Name = "cb_staff";
      cb_positions = new ComboBox(); cb_positions.Name = "cb_positions";

      sql = "select * from form order by fio";
      _dt_form = _mySql.SqlSelect(sql).Tables[0];
      _dt_form.PrimaryKey = new DataColumn[1] { _dt_form.Columns["fio"] };
      foreach (DataRow dr in _dt_form.Rows)
      {
        UInfo userInfo = new UInfo() { Email = dr["email"].ToString() };
        ComboBoxItem item = new ComboBoxItem();

        item.Name = "I" + dr["id"].ToString();
        item.Content = dr["fio"].ToString();
        item.Tag = userInfo;

        cb_staff.Items.Add(item);
      }
      cb_staff.SelectedIndex = 0;

      sql = "select * from positions order by priority";
      _dt_positions = _mySql.SqlSelect(sql).Tables[0];
      _dt_positions.PrimaryKey = new DataColumn[1] { _dt_positions.Columns["id"] };
      foreach (DataRow dr in _dt_positions.Rows)
      {
        ComboBoxItem item = new ComboBoxItem();

        item.Content = dr["name1"].ToString();
        item.Name = "I" + dr["id"].ToString();
        cb_positions.Items.Add(item);
      }
      cb_staff.SelectedIndex = 0;
      cb_positions.SelectedIndex = 0;

      //
      sp_staff_edit_item = new StackPanel(); sp_staff_edit_item.Orientation = Orientation.Horizontal;
      Button b_ok = new Button(); b_ok.Content = "Применить"; b_ok.Click += new RoutedEventHandler(b_ok_addS_Click);
      Button b_cancel = new Button(); b_cancel.Content = "Отменить"; b_cancel.Click += new RoutedEventHandler(b_cancel_addS_Click);
      tb_email = new TextBox();
      sp_staff_edit_item.Children.Add(cb_staff);
      sp_staff_edit_item.Children.Add(cb_positions);
      sp_staff_edit_item.Children.Add(tb_email);
      sp_staff_edit_item.Children.Add(b_ok);
      sp_staff_edit_item.Children.Add(b_cancel);

      FillTree();
    }

    void FillTreeNode(TreeViewItem root)
    {
      string id = root.Name.Substring(1);
      string sql = "select * from IDOTree where id_parent=" + id + " order by priority, nodename";
      DataTable dt = _mySql.SqlSelect(sql).Tables[0];
      foreach (DataRow dr in dt.Rows)
      {
        TreeViewItem item = new TreeViewItem();
        item.Name = "I" + dr["id"].ToString(); item.Header = dr["nodename"].ToString(); item.Selected += new RoutedEventHandler(TViewItem_Selected);
        root.Items.Add(item);
        FillTreeNode(item);
      }
    }
    void FillTree()
    {
      tView.Items.Clear();
      string sql = "select * from IDOTree where id_parent=0 order by priority, nodename";
      DataTable dt = _mySql.SqlSelect(sql).Tables[0];
      foreach (DataRow dr in dt.Rows)
      {
        TreeViewItem item = new TreeViewItem();
        item.Name = "I" + dr["id"].ToString(); item.Header = dr["nodename"].ToString(); item.Selected += new RoutedEventHandler(TViewItem_Selected);
        tView.Items.Add(item);

        FillTreeNode(item);
      }
    }

    StackPanel NewLb_staffItem(string id_staff, string id_fio, string fio, string id_position, string position, string email)
    {
      if (email == "") email = "E-mail отсутствует";
      StackPanel sp = new StackPanel(); sp.Name = "S" + id_staff;
      TextBlock t_fio = new TextBlock(); t_fio.Name = "F" + id_fio; t_fio.Text = fio; t_fio.FontWeight = FontWeights.UltraBold;
      TextBlock t_pos = new TextBlock(); t_pos.Name = "P" + id_position; t_pos.Text = position; t_pos.Margin = new Thickness(5, 1, 1, 0);
      TextBlock t_email = new TextBlock(); t_email.Name = "E" + id_position; t_email.Text = email; t_email.Margin = new Thickness(5, 1, 1, 0);
      sp.Children.Add(t_fio); sp.Children.Add(t_pos); sp.Children.Add(t_email);

      return sp;
    }
    private void TViewItem_Selected(object sender, RoutedEventArgs e)
    {
      //отображение сотруднико подразделения
      if (tView.SelectedItem == null) return;
      b_cancel_addS_Click(sender, e);//отмена возможных начал изменений сотрудника

      tViewSelectedItem = (TreeViewItem)tView.SelectedItem;
      string id_dep = tViewSelectedItem.Name.Substring(1);

      L_dep_name.Text = "\"" + (string)tViewSelectedItem.Header + "\"";


      //заполненеие списка сотрудников
      lBox.Items.Clear();
      string sql = "select * from staff where id_dep = " + id_dep + " order by priority";
      DataTable dt_s = _mySql.SqlSelect(sql).Tables[0];

      if (dt_s.Rows.Count > 0)
      {
        //можно использовать _dt_form
        sql = "select * from form where id = " + dt_s.Rows[0]["id_form"].ToString();
        for (int i = 1; i < dt_s.Rows.Count; i++)
          sql += " or id = " + dt_s.Rows[i]["id_form"].ToString();
        DataTable dt_f = _mySql.SqlSelect(sql).Tables[0];

        for (int i = 0; i < dt_f.Rows.Count; i++)
        {
          DataRow r = _dt_positions.Rows.Find((int)dt_s.Rows[i]["id_position"]);
          ListBoxItem item = new ListBoxItem(); item.Name = "I" + dt_s.Rows[i]["id"].ToString();

          item.Content = NewLb_staffItem(dt_s.Rows[i]["id"].ToString(), dt_f.Rows[i]["id"].ToString(),
                         dt_f.Rows[i]["fio"].ToString(), r["id"].ToString(), r["name1"].ToString(), dt_f.Rows[i]["email"].ToString());
          //item.Content = dt_f.Rows[i]["fio"] + "\r\n    " + r["name1"];
          lBox.Items.Add(item);
        }

      }
    }

    private void TextBox_LostFocus(object sender, RoutedEventArgs e)
    {
      //обновление структуры
      if (tViewEditingItem == null) return;
      TextBox E = (TextBox)sender;
      string id = E.Name.Substring(1);
      string sql = "Update IDOTree set nodename = '" + E.Text + "' where id=" + id;
      _mySql.SqlExec(sql);

      tViewEditingItem.Header = E.Text;
      tViewEditingItem = null;
    }

    private void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter) TextBox_LostFocus(sender, e);
    }

    private void b_add_Click(object sender, RoutedEventArgs e)
    {
      //добавление нового подразделения
      if (tView.SelectedItem == null) return;
      tViewSelectedItem = (TreeViewItem)tView.SelectedItem;

      string id_parent = tViewSelectedItem.Name.Substring(1);
      string sql = "insert into IDOTree (id_parent, nodename, priority) values (" + id_parent + ", '', 10)";
      _mySql.SqlExec(sql);

      sql = "select id from IDOTree order by id";
      DataTable dt = _mySql.SqlSelect(sql).Tables[0];
      string id = dt.Rows[dt.Rows.Count - 1]["id"].ToString();
      TextBox E = new TextBox(); E.Name = "E" + id;
      E.LostFocus += new RoutedEventHandler(TextBox_LostFocus);
      E.KeyDown += new KeyEventHandler(TextBox_KeyDown);

      TreeViewItem item = new TreeViewItem();
      item.Header = E; item.Name = "I" + id;

      tViewSelectedItem.Items.Add(item);
      tViewSelectedItem.IsExpanded = true;
      tViewEditingItem = item;

      E.Focus();
    }
    private void b_change_Click(object sender, RoutedEventArgs e)
    {
      //изменение подразления
      if (tView.SelectedItem == null) return;
      if (tViewEditingItem != null) return;
      tViewSelectedItem = (TreeViewItem)tView.SelectedItem;
      tViewEditingItem = tViewSelectedItem;

      TextBox E = new TextBox(); E.Name = "E" + tViewEditingItem.Name.Substring(1); ;
      E.LostFocus += new RoutedEventHandler(TextBox_LostFocus);
      E.KeyDown += new KeyEventHandler(TextBox_KeyDown);
      E.Focus();
      E.Text = (string)tViewEditingItem.Header;
      tViewEditingItem.Header = E;
    }
    void DeleteItem(TreeViewItem root)
    {
      //удаление подразделения
      string sql;
      for (int i = 0; i < root.Items.Count; i++)
      {
        TreeViewItem item = (TreeViewItem)root.Items[i];
        DeleteItem(item);
        string id = item.Name.Substring(1);
        sql = "delete from IDOTree where id=" + id;
        _mySql.SqlExec(sql);
        root.Items.Remove(item);
      }
      sql = "delete from IDOTree where id=" + root.Name.Substring(1);
      _mySql.SqlExec(sql);
      ((TreeViewItem)root.Parent).Items.Remove(root);
    }
    private void b_del_Click(object sender, RoutedEventArgs e)
    {
      if (tView.SelectedItem == null) return;
      TreeViewItem item = (TreeViewItem)tView.SelectedItem;
      DeleteItem(item);
    }
    #region сотрудники
    private void b_ok_addS_Click(object sender, RoutedEventArgs e)
    {
      if (isAddingNewStaff)
      {

        string fio = (string)((ComboBoxItem)cb_staff.SelectedItem).Content;
        string id_form = _dt_form.Rows.Find(fio)["id"].ToString();

        string id_position = ((ComboBoxItem)cb_positions.SelectedItem).Name.Substring(1);

        string id_dep = tViewSelectedItem.Name.Substring(1);

        string sql = "insert into staff (id_dep, id_form, id_position, priority) values (" + id_dep + ", " + id_form + ", " + id_position + ", 10)";
        _mySql.SqlExec(sql);

        sql = "select * from staff where (id_dep = " + id_dep + " and id_form = " + id_form + " and id_position = " + id_position + ")";
        string id_s = _mySql.SqlSelect(sql).Tables[0].Rows[0]["id"].ToString();

        ComboBoxItem cb_item = ((ComboBoxItem)cb_positions.SelectedItem);
        ((ListBoxItem)lBox.Items[lBox.Items.Count - 1]).Content = NewLb_staffItem(id_s, id_form, fio, cb_item.Name.Substring(1), (string)cb_item.Content, "E-mail");
        isAddingNewStaff = false;
      }
      else
      {
        string fio = (string)((ComboBoxItem)cb_staff.SelectedItem).Content;
        string id_form = _dt_form.Rows.Find(fio)["id"].ToString();

        string id_position = ((ComboBoxItem)cb_positions.SelectedItem).Name.Substring(1);
        string id_dep = tViewSelectedItem.Name.Substring(1);

        string id_staff = sp_lbSelectedItem.Name.Substring(1);
        string sql = "update staff set id_dep = " + id_dep + ", id_form = " + id_form + ", id_position = " +
          id_position + " where id=" + id_staff;
        _mySql.SqlExec(sql);

        sql = "update form set email = '" + tb_email.Text + "' where id=" + id_form;
        _mySql.SqlExec(sql);

        ComboBoxItem cb_item = ((ComboBoxItem)cb_positions.SelectedItem);
        lbSelectedItem.Content = NewLb_staffItem(id_staff, id_form, fio, cb_item.Name.Substring(1), (string)cb_item.Content, tb_email.Text);

        isChangingNewStaff = false;
      }

      b_addS.IsEnabled = true;
      b_changeS.IsEnabled = true;
      b_delS.IsEnabled = true;
    }
    private void b_cancel_addS_Click(object sender, RoutedEventArgs e)
    {
      if (isAddingNewStaff)
      {
        lBox.Items.RemoveAt(lBox.Items.Count - 1);
      }
      if (isChangingNewStaff)
      {
        ((ListBoxItem)lBox.SelectedItem).Content = sp_lbSelectedItem;
      }
      if (isChangingNewStaff | isAddingNewStaff)
      {
        isAddingNewStaff = false;
        isChangingNewStaff = false;

        b_addS.IsEnabled = true;
        b_changeS.IsEnabled = true;
        b_delS.IsEnabled = true;
      }


    }
    ComboBoxItem FindComboBoxItem(ComboBox cb, string name)
    {
      ComboBoxItem res = null;
      foreach (ComboBoxItem item in cb.Items)
        if (item.Name == name)
        {
          res = item;
          break;
        }

      return res;
    }
    void ChangeStaff(bool IsNew)
    {
      if (tViewSelectedItem == null) return;

      if (IsNew)
      {
        if (isAddingNewStaff) return;
        isAddingNewStaff = true;

        ListBoxItem item = new ListBoxItem();
        item.Content = sp_staff_edit_item;
        lBox.Items.Add(item);
      }
      else
      {
        if (isChangingNewStaff) return;
        isChangingNewStaff = true;

        if (lbSelectedItem == null) return;

        StackPanel sp = (StackPanel)lbSelectedItem.Content;
        string id_fio = ((TextBlock)sp.Children[0]).Name.Substring(1);
        string id_position = ((TextBlock)sp.Children[1]).Name.Substring(1);
        cb_staff.SelectedItem = FindComboBoxItem(cb_staff, "I" + id_fio); ;
        cb_positions.SelectedItem = FindComboBoxItem(cb_positions, "I" + id_position);
        UInfo userInfo = (UInfo)((ComboBoxItem)cb_staff.SelectedItem).Tag;
        tb_email.Text = userInfo.Email;

        ((ListBoxItem)lBox.SelectedItem).Content = sp_staff_edit_item;
      }
      b_addS.IsEnabled = false;
      b_changeS.IsEnabled = false;
      b_delS.IsEnabled = false;
    }
    private void b_addS_Click(object sender, RoutedEventArgs e)
    {
      //добавить сотрудника
      ChangeStaff(true);
    }

    private void b_changeS_Click(object sender, RoutedEventArgs e)
    {
      //изменить сотрудника
      ChangeStaff(false);
    }

    private void b_delS_Click(object sender, RoutedEventArgs e)
    {
      string id_staff = sp_lbSelectedItem.Name.Substring(1);
      string sql = "delete from staff where id=" + id_staff;
      _mySql.SqlExec(sql);
      lBox.Items.Remove(lbSelectedItem);
    }

    private void lBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      lbSelectedItem = (ListBoxItem)lBox.SelectedItem;
      if (lbSelectedItem != null)
        if (!isChangingNewStaff)//запоминаем значение до начала редактирования
        {
          sp_lbSelectedItem = (StackPanel)lbSelectedItem.Content;
          string s = sp_lbSelectedItem.Name;
        }
    }
    #endregion сотрудники

    private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
    {
//      grid_staff.Height = Height;
      lBox.Height = Height - 100; //какое-то решение скроллинга
    }

  }
  class UInfo
  {
    string email;
    public string Email
    {
      get { return email; }
      set
      {
        if (value == "") email = "e-mail";
        else email = value;
      }
    }
  }
}
