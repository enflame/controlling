﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using Modules;
using CustomControls;
using ADSI;
using System.IO;
using System.Diagnostics;
using MySql;
using GeneralClasses;

namespace Controlling
{
  /// <summary>
  /// Логика взаимодействия для UserLogin.xaml
  /// </summary>
  public partial class UserLogin : Window
  {
    StaffTree.SelectedItemEventArgs staff_args;
    CMySql _mySql;

    public CMySql MySql
    {
      get { return _mySql; }
    }
    ComboBoxItem FindComboBoxItemByString(ComboBox cb, string content)
    {
      foreach (ComboBoxItem item in cb.Items)
        if (item.Content.ToString() == content) return item;
      return null;
    }
    CMySql NewConnection(bool isNewConnection)
    {
      CMySql mySql = null;
      ((ComboBoxItem)cb_connection_string.SelectedItem).Content = "db.tpu.ru:1521/TPU";
      string connect = "";
      connect = ((ComboBoxItem)cb_connection_string.SelectedItem).Content.ToString();
      try
      {
        mySql = new CMySql();
        mySql.ConnectToDB("User Id=APP_IDOIS;Password= secret;Data Source=" + connect, CMySql.DB_ORACLE);

      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
        return null;
      }

      return mySql;
    }
    public UserLogin()
    {
      InitializeComponent();

      _mySql = NewConnection(false);
      if (_mySql == null)
      {
        MessageBox.Show("Нет подключения к базе");
        return;
      }
      string sql = "select * from access_to_base where login='" + UserInfo.UserName + "'";
      DataTable dt = _mySql.SqlSelect(sql);
      if (dt.Rows.Count == 0)
      {
        ch_IsLogin.Visibility = System.Windows.Visibility.Collapsed;
        b_selectPerson.Visibility = System.Windows.Visibility.Collapsed;
      }
    }

    void Login()
    {
      string server = ((ComboBoxItem)cb_connection_string.SelectedItem).Content.ToString();
      UserInfo.server = server;

      string login = tb_login.Text.Trim();
      if (ch_IsLogin.IsChecked == true)
      {
        CADSI adsi = new CADSI("main.tpu.ru", login, tb_psw.Password);
        if (adsi.FindUser(tb_login.Text.Trim()) == null)
        {
          MessageBox.Show("Неверное имя или пароль");
          return;
        }
      }
      else if (staff_args != null) login = staff_args.staffInfo.login;

      string sql = "select U.LICHNOST_ID, U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO, U.EMAIL, " +
                 "UF.DOLGNOST, UF.PODRAZDELENIE_ID, " +
                 "P.NAZVANIE " +
                 "from app_portal.v_tpu_users U " +
                 "join SOTRUDNIK.LICHNOST_FOR_PHONE_SPR_V3 UF on UF.LICHNOST_ID=U.LICHNOST_ID " +
                 "join tpu.podrazdelenie_tpu_v P on P.ID =  UF.PODRAZDELENIE_ID " +
                 "Where U.LOGIN='" + login + "'";
      DataTable dt = _mySql.SqlSelect(sql);
      if (dt.Rows.Count == 0) { MessageBox.Show("Пользователя '" + login + "' нет в базе сотрудников"); return; }
      DataRow dr = dt.Rows[0];
      if (dt.Rows.Count > 1)
      {
        dlgSelectLoginUser dlg = new dlgSelectLoginUser(dt);
        if ((bool)dlg.ShowDialog())
        {
          dr = dlg._dr;
        }
        else return;
      }

      UserInfo.Info.login = login;
      UserInfo.Info.id_form = dr["LICHNOST_ID"].ToString();
      UserInfo.Info.fio = dr["FIO"].ToString();
      UserInfo.Info.email = dr["EMAIL"].ToString();
      UserInfo.Info.dep = dr["NAZVANIE"].ToString();
      UserInfo.Info.id_dep = dr["PODRAZDELENIE_ID"].ToString();
      //       UserInfo.id_position = dr[""].ToString(); // UserInfo.id_position - наверное не нужен
      UserInfo.Info.position = dr["DOLGNOST"].ToString(); ;

      DialogResult = true;

    }


    private void b_login_Click(object sender, RoutedEventArgs e)
    {
      Login();
    }

    void staffTree_SelectedStaff(Object sender, StaffTree.SelectedItemEventArgs args)
    {
      staff_args = args;
    }

    private void b_close_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }
    private void b_selectPerson_Click(object sender, RoutedEventArgs e)
    {
      dlgSelectStaff wnd = new dlgSelectStaff(_mySql, new dlgSelectStaff.SelectStaffEventHandler(onSelectedStaff));
      Point p = PointToScreen(Mouse.GetPosition(b_selectPerson));
      wnd.Left = p.X;
      wnd.Top = p.Y;

      wnd.Show();
    }
    void onSelectedStaff(Object sender, StaffTree.SelectedItemEventArgs args)
    {
      tb_login.Text = args.staffInfo.fio;
      staff_args = args;
    }
    private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!IsLoaded) return;
      if (e.AddedItems == cb_select_base.Items[1])
      {
        sp_connect_oracle.Visibility = System.Windows.Visibility.Visible;
      }
      else sp_connect_oracle.Visibility = System.Windows.Visibility.Collapsed;
    }
    private void cb_connection_string_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!IsLoaded) return;

      _mySql = NewConnection(true);
    }
    private void ch_IsLogin_Checked(object sender, RoutedEventArgs e)
    {
      b_selectPerson.IsEnabled = false;
    }
    private void ch_IsLogin_Unchecked(object sender, RoutedEventArgs e)
    {
      b_selectPerson.IsEnabled = true;
    }
    private void tb_login_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter) Login();
    }
    private void PasswordBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter) Login();
    }
    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      try
      {
        DateTime last_update_date;
        if (File.Exists("update_settings")) last_update_date = DateTime.Parse(File.ReadAllText("update_settings"));
        else
        {
          last_update_date = DateTime.Now - new TimeSpan(1, 0, 0, 0);
          File.WriteAllText("update_settings", DateTime.Now.ToShortDateString());
        }
        DataTable _dt = _mySql.SqlSelect("select * from programm_update where change_date > to_date('" + _mySql.ToDateTime(last_update_date.ToShortDateString()) + "', 'dd.MM.yyyy') order by id");
        if (_dt.Rows.Count > 0)
        {
          ProcessStartInfo info = new ProcessStartInfo("StartControlling.exe");
          Process.Start(info);
          Close();
        }
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
        return;
      }


    }

  }
}
