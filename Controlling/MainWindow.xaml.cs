﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using System.Windows.Documents;
using System.IO;

using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Runtime.Serialization.Formatters.Binary;

using CustomControls;
using Modules;
using System.Threading;
using System.Diagnostics;
using MySql;
using Net;
using GeneralClasses;

namespace Controlling
{
  /// <summary>
  /// Логика взаимодействия для MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    const string VERSION = "1.0.0";
    bool _reallyCloseWindow = false;
    CMySql _mySql;
    DataTable _dt_messages;

    IndWrapPanel _mainIWP;
    ScorecardElement CurrentIndicator;

    BSCtree.SelectedItemEventArgs BSCtreeSelectedItem = new BSCtree.SelectedItemEventArgs();//криво, для ...


    Thread th_InitComponents;
    public MainWindow()
    {
      try
      {
        string s = UserInfo.TempDir;
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
        return;
      }
      //очистка временных файлов
      Functions.DeleteAllFiles(UserInfo.TempDir);
      //
      {
        UserLogin dlg = new UserLogin();
        Nullable<bool> res = dlg.ShowDialog();
        if (!(bool)res)
        {
          this.Close();
          return;
        }
        _mySql = dlg.MySql;

        DataTable dt_UserParams = _mySql.SqlSelect("select * from user_params where id_staff = " + UserInfo.Info.id_form);
        if (dt_UserParams.Rows.Count > 0)
        {
          if (!ChangeTargetYear(dt_UserParams.Rows[0]["TargetYear"].ToString()))
          {
            DataTable dt_params = _mySql.SqlSelect("select * from params where TargetYear =(select MAX(TargetYear) from params)");
            if (dt_params.Rows.Count == 0) Close();
            ChangeTargetYear(dt_params.Rows[0]["TargetYear"].ToString());
          }
        }
        else
        {
          //          string targetYear = DateTime.Now.Year.ToString();
          string targetYear = DateTime.Now.Year.ToString();
          _mySql.SqlExec("insert into user_params (id_staff, targetyear) values(" + UserInfo.Info.id_form + ", " + targetYear + ")");
          if (!ChangeTargetYear(targetYear)) return;
        }

      }
      InitializeComponent();

      th_InitComponents = new Thread(InitComponents);
      th_InitComponents.Start();
      _reallyCloseWindow = false;

    }
    bool ChangeTargetYear(string TargetYear)
    {
      bool res = false;
      DataTable dt_params = _mySql.SqlSelect("select * from params where TargetYear = " + TargetYear);
      if (dt_params.Rows.Count == 0)
      {
        if (MessageBox.Show("Начать работу с новым отчётным периодом имеет право директор института. \r\n Если вы директор нажмите продолжить", "Новый отчётный период", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
        {
          _mySql.SqlExec("insert into params (id_head_all_department, targetyear) values(" + UserInfo.Info.id_form + ", " + TargetYear + ")");
          UserInfo.id_head_all_department = UserInfo.Info.id_form;
          UserInfo.TargetYear = TargetYear;
          res = true;
        }
      }
      else
      {
        UserInfo.id_head_all_department = dt_params.Rows[0]["id_head_all_department"].ToString();
        UserInfo.TargetYear = TargetYear;

        res = true;
      }
      _mySql.SqlExec("update user_params set targetyear=" + UserInfo.TargetYear + " where id_staff=" + UserInfo.Info.id_form);
      return res;
    }
    void InitComponents()
    {
      this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new System.Windows.Threading.DispatcherOperationCallback(delegate
      {


        DataRow dr = _mySql.SqlSelect("select sysdate from dual").Rows[0];
        UserInfo.ServerDate = (DateTime)dr["sysdate"];

        kpiProperties.Visibility = System.Windows.Visibility.Hidden;

        kpiProperties.SCHandler += new Modules.KPIProperties.SCEventHandler(OnSCEvent);
        kpiReports.Set(_mySql);
        navigator.SelectedItem += new Navigator.SelectedItemEventHandler(OnNavigatorEvent);
        navigator.bSCtree_SelectedItem += new BSCtree.SelectedItemEventHandler(OnBSCTreeItem_Selected);
        navigator.staffTree_SelectedStaff += new StaffTree.SelectedItemEventHandler(OnStaffTreeItem_Selected);

        _mainIWP = new IndWrapPanel();
        _mainIWP.Name = "mainIWP";
        _mainIWP.Setup(_mySql, -1, -1, "Показатели предыдущего уровня", wpChild, monitoring);
        navigator.Set(_mySql, TypeHeader.All, _mainIWP);
        FillScorecardTree();

        pkmReport.Set(_mySql, "-1");

        StaffTree.SelectedItemEventArgs args = new StaffTree.SelectedItemEventArgs();
        args.staffInfo = UserInfo.Info;
        staffProperties.Set(args, _mySql);

        tb_FIO.Text = UserInfo.Info.fio + ", " + UserInfo.Info.dep;// +" - Дата: " + UserInfo.ServerDate.ToShortDateString();

        cb_targetyear.Items.Clear();
        DataTable dt = _mySql.SqlSelect("select TargetYear from params order by TargetYear desc");
        foreach (DataRow dr1 in dt.Rows)
        {
          ComboBoxItem cb = new ComboBoxItem(); cb.Content = dr1["TargetYear"].ToString();
          cb_targetyear.Items.Add(cb);
        }
        cb_targetyear.SelectedItem = Functions.FindComboBoxItemByString(cb_targetyear, UserInfo.TargetYear);

        int CountFactValues = FactValues.CountFactValues(_mySql);
        if (CountFactValues > 0) tm_factValues.Show(tm_factValues.Text + "(" + CountFactValues.ToString() + ")", true);
        return null;
      }), null);

      LoadSetting();

    }
    void UpdateMessages()
    {

      string sql = "select * from messages where id_form_for=" + UserInfo.Info.id_form + " and isEnded < 1 and targetyear = " + UserInfo.TargetYear.ToString();
      _dt_messages = _mySql.SqlSelect(sql);

      tb_Messages.Text = "Сообщения и уведомления (" + _dt_messages.Rows.Count.ToString() + ")";
      if (_dt_messages.Rows.Count > 0)
      {
        //dlgMessages dlg = new dlgMessages(_mySql);
        //dlg.ShowDialog();
        //b_Messages.BorderBrush = Brushes.YellowGreen;

        //NotifyBalloon nb = (NotifyBalloon)NotifyIcon.TrayPopup;
      }
      else
      {
        b_Messages.BorderBrush = Brushes.Transparent;
      }

    }

    #region Заполнение деревьев
    Brush TreeViewItemBrush(string id)
    {//расчёт расцеки показателя в зависимости от степени выполнения
      Brush res = Brushes.GreenYellow;
      return res;
    }
    void FillScorecardTree()
    {
      TC_perspective.Items.Clear();

      string sql = "select * from SCTree where id_parent=0  and targetyear = " + UserInfo.TargetYear.ToString() + " order by id";
      DataTable dt = _mySql.SqlSelect(sql);//перспективы
      foreach (DataRow dr_persp in dt.Rows)
      {
        sql = "select * from SC where id=" + dr_persp["id_SC"].ToString() + " and targetyear = " + UserInfo.TargetYear.ToString();
        DataTable dt_SC = _mySql.SqlSelect(sql);
        DataRow dr_SC = dt_SC.Rows[0];//одна строка


        TabItem tb = new TabItem(); tb.Name = "t" + dr_persp["id_SC"].ToString();
        TreeViewItemHeader h = new TreeViewItemHeader("/Controlling;component/Ico/stock_mail_flag_for_followup_32.png", dr_SC["name1"].ToString(), -1, null);
        tb.Header = h; tb.Style = (Style)this.Resources["TabItemStyle"];

        tb.Content = null;
        TC_perspective.Items.Add(tb);
      }
      if (TC_perspective.Items.Count > 0)
      {
        TC_perspective.SelectedIndex = 0;
        ((TabItem)TC_perspective.Items[0]).Content = _mainIWP;
        navigator.ChangePerspective(((TabItem)TC_perspective.Items[0]).Name.Substring(1));
      }
    }
    #endregion Заполнение деревьев

    #region События от элементов
    private void OnBSCTreeItem_Selected(object sender, BSCtree.SelectedItemEventArgs args)
    {
      if (BSCtreeSelectedItem.IsEqual(args)) 
        return;

      BSCtreeSelectedItem = args;
      if (args.isEdit)
      {
        tab_control.SelectedIndex = 1;

        kpiProperties.Visibility = System.Windows.Visibility.Hidden;
        tab_control.SelectedIndex = 1;

        string sql = "select * from SC where id=" + args.id_SC + " and targetyear = " + UserInfo.TargetYear.ToString();
        DataTable dt_SC = _mySql.SqlSelect(sql);
        if (dt_SC.Rows.Count == 0) return; //ещё нет показателей
        DataRow dr_SC = dt_SC.Rows[0];//одна строка

        //Кривой код
        int level1 = _mySql.ToInt32(dr_SC["level1"]);
        if (level1 != TypeHeader.Perspective)
        {
          kpiProperties.isKPISetting = false;
          kpiProperties.Visibility = System.Windows.Visibility.Visible;
          kpiProperties.Set(args.id_SC, args.id_SC_root, _mySql, monitoring);
          //return;
        }
        // else wpChild.SelectScorecard(-1);
      }
      else
      {
        tab_control.SelectedIndex = 0;
        if (TC_perspective.Items.Count == 0) return;

        string idPerspective = args.sce.idPerspective.ToString();
        if (((TabItem)TC_perspective.SelectedItem).Name.Substring(1) != idPerspective)
          foreach (TabItem tab in TC_perspective.Items)
          {
            if (tab.Name.Substring(1) == idPerspective)
            {
              // выделили стратегическую цель
              TC_perspective.SelectedItem = tab;
              navigator.ChangePerspective(idPerspective);

              break;
            }
          }

        _mainIWP.SelectScorecardInAllIWP(args.sce);
        //if (wpChild.SelectScorecard(Convert.ToInt32(args.id_SC)) == -1)
        //{
        //  _mainIWP.SelectScorecard(Convert.ToInt32(args.id_SC));
        //  _mainIWP.SelectScorecardInAllIWP(args.sce);
        //}
        //else 
        //{
        //  wpChild.SelectScorecard(Convert.ToInt32(args.id_SC));
        //}
      }
    }
    void OnIndWrapPanelEvent(Object sender, IndWrapPanel.IndWrapPanelEventArgs args)
    {
      if (args.Event == IndWrapPanel.IsShowSmartEvent)
      {
        CurrentIndicator = args.Element;

        kpiProperties.isKPISetting = false;
        kpiProperties.Visibility = System.Windows.Visibility.Visible;
        kpiProperties.Set(args.Element.idSC.ToString(), args.Element.idSCParent.ToString(), _mySql, monitoring);
      }
    }
    private void OnStaffTreeItem_Selected(object sender, Modules.StaffTree.SelectedItemEventArgs args)
    {
      tab_control.SelectedIndex = 2;
      staffProperties.Set(args, _mySql);
    }
    private void OnSCEvent(object sender, Modules.KPIProperties.SCEventArgs args)
    {
      //события от панели свойств показателя
      if (args.TypeEvent == KPIProperties.IsApprovedConst)
      {
        navigator.AproveSC((bool)args.obj, args.idSC);
      }
      if (args.TypeEvent == KPIProperties.IsSaved)
      {
        navigator.ChangeNameSC((string)args.obj, args.idSC);
      }
    }
    private void OnNavigatorEvent(object sender, Modules.Navigator.SelectedItemEventArgs args)
    {
      //события от панели свойств показателя
      if (args.typeItem == Navigator.SelectedItemEventArgs.isStrategicSelected)
      {
        tab_control.SelectedIndex = 0;
      }
      if (args.typeItem == Navigator.SelectedItemEventArgs.isBSCPropertiesSelected)
      {
        tab_control.SelectedIndex = 1;
      }
      if (args.typeItem == Navigator.SelectedItemEventArgs.isSelectSCForUser)
      {
        bool isOnlyUseSC = (bool)args.obj;

      }
    }

    #endregion

    #region Добавление показателей
    void AddPerspective(string name)
    {
    }

    #endregion

    void LoadSetting()
    {
      string sql = "select * from programm_settings where s_name='SendMail'";
      DataTable dt = _mySql.SqlSelect(sql);
      if (dt.Rows.Count == 0) return;

      byte[] blob = (byte[])dt.Rows[0]["s_obj"];
      MemoryStream ms = new MemoryStream(blob);
      BinaryFormatter bf = new BinaryFormatter();
      UserInfo.SendMail = (SendMail)bf.Deserialize(ms);

    }
    void SaveSetting()
    {
      BinaryFormatter bf = new BinaryFormatter();
      MemoryStream ms = new MemoryStream();
      bf.Serialize(ms, UserInfo.SendMail);
      string sql = "update programm_settings set s_obj=:BlobParameter where s_name='SendMail'";
      _mySql.InsertBlobOracle(sql, ms.GetBuffer());

    }
    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      if (UserInfo.UserName == "eugene") b_upload.Visibility = System.Windows.Visibility.Visible;
      NotifyIcon.TrayPopup = new NotifyBalloon();
      ((NotifyBalloon)NotifyIcon.TrayPopup).Set(_mySql);

      wpChild.Caption = "";
      UpdateMessages();

      //this.WindowState = System.Windows.WindowState.Maximized;
    }
  
      private void b_Messages_Click(object sender, RoutedEventArgs e)
    {
      dlgMessages dlg = new dlgMessages(_mySql);
      dlg.ShowDialog();
    }
    private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (tab_control.SelectedItem == tab_Staff) border_Monitoring.Visibility = System.Windows.Visibility.Collapsed;
      else border_Monitoring.Visibility = System.Windows.Visibility.Visible;
    }

    private void b_Fact_Click(object sender, RoutedEventArgs e)
    {
      dlgInsertFactValues dlg = new dlgInsertFactValues(_mySql);

      dlg.ShowDialog();
    }
    private void NotifyIconOpen_Click(object sender, RoutedEventArgs e)
    {

    }
    private void NotifyIconExit_Click(object sender, RoutedEventArgs e)
    {
      _reallyCloseWindow = true;
      Close();
    }
    private void NotifyIconConfigure_Click(object sender, RoutedEventArgs e)
    {

    }

    private void NotifyIcon_TrayMouseDoubleClick(object sender, RoutedEventArgs e)
    {
      this.Show();
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      //if (!this._reallyCloseWindow)
      //{
      //  e.Cancel = true;
      //  // Trying to hide
      //  Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, (DispatcherOperationCallback)delegate(object o)
      //  {
      //    this.Hide();
      //    return null;
      //  }, null);
      //}
    }

 
    private void TC_perspective_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (e.RemovedItems.Count == 0) return;
      if (e.AddedItems.Count == 0) return;

      ((TabItem)e.RemovedItems[0]).Content = null;
      ((TabItem)e.AddedItems[0]).Content = _mainIWP;
      navigator.ChangePerspective(((TabItem)e.AddedItems[0]).Name.Substring(1));
    }
    private void cb_targetyear_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!IsLoaded) return;
      if (e.AddedItems.Count == 0) return;
      if (((ComboBoxItem)e.AddedItems[0]).Content.ToString() == UserInfo.TargetYear) return;

      string TargetYear = ((ComboBoxItem)e.AddedItems[0]).Content.ToString();
      if (TargetYear == UserInfo.TargetYear) return;
      if (!ChangeTargetYear(TargetYear))
      {
        cb_targetyear.SelectedItem = e.RemovedItems[0];
        return;
      }
      _mainIWP.RemoveAllIndicators();

      InitComponents();

    }
    private void b_addPeriod_Click(object sender, RoutedEventArgs e)
    {
      dlgAddValue dlg = new dlgAddValue("Введите год (вы должны быть руководителем всего подразделения)");
      dlg.Title = "Добавить новый период";
      if ((bool)dlg.ShowDialog())
      {
        try
        {
          int TargetYear = Convert.ToInt32(dlg.Value);
   //       ChangeTargetYear(TargetYear.ToString());
          
          ComboBoxItem cb = new ComboBoxItem();
          cb.Content = TargetYear.ToString();
          cb_targetyear.Items.Add(cb);
          cb_targetyear.SelectedIndex = cb_targetyear.Items.Count - 1;

 //         cb_targetyear_SelectionChanged(object sender, SelectionChangedEventArgs e)
        }
        catch(Exception exc) 
        {
          MessageBox.Show(exc.Message);
        }
      }
    }
    private void b_upload_Click(object sender, RoutedEventArgs e)
    {
      dlgUpdateProgram dlg = new dlgUpdateProgram(_mySql);
      dlg.ShowDialog();
    }
    private void b_UserManual_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        string f = Directory.GetCurrentDirectory() + "\\Руководство пользователя.docx";
        ProcessStartInfo info = new ProcessStartInfo(f);
        Process.Start(info);
      }
      catch (Exception exc) { MessageBox.Show(exc.Message); }
    }

    private void b_Settings_Click(object sender, RoutedEventArgs e)
    {
      wndSettings wnd = new wndSettings();
      wnd.ShowDialog();
      SaveSetting();
    }

  }
}
