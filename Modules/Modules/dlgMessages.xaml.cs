﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Modules;
using System.Data;
using MySql;
using GeneralClasses;
using KIP;
namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для dlgMessages.xaml
  /// </summary>
  public partial class dlgMessages : Window
  {
    CMySql _mySql;
    bool _isOwnMess;
    bool _isAllMess;
    public dlgMessages(CMySql mySql)
    {
      InitializeComponent();

      _mySql = mySql;
      _isAllMess = false;
      _isOwnMess = false;
      GetMessages();
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }

    void GetMessages()
    {
      if (_isOwnMess) dg_messages.Columns[1].Header = "Получатель";
      else dg_messages.Columns[1].Header = "Адресант";
      dg_messages.ItemsSource = TypeMessages.GetMessages(_isOwnMess, _isAllMess, _mySql).DefaultView;
    }
    private void cb_OwnMess_Checked(object sender, RoutedEventArgs e)
    {
      _isOwnMess = true;
      GetMessages();
    }

    private void cb_OwnMess_Unchecked(object sender, RoutedEventArgs e)
    {
      _isOwnMess = false;
      GetMessages();
    }
    private void cb_AllMess_Checked(object sender, RoutedEventArgs e)
    {
      _isAllMess = true;
      GetMessages();
    }
    private void cb_AllMess_Unchecked(object sender, RoutedEventArgs e)
    {
      _isAllMess = false;
      GetMessages();
    }

    private void b_Accept_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        //убрать оставить только для информирования
        DataRowView dr = (DataRowView)dg_messages.SelectedItem;
        string sql;
        int TypeMess = Convert.ToInt32(dr["TypeMess"]);
        if (TypeMess == TypeMessages.SendPKMForApproved)
        {
          dlgCreatePKM dlg = new dlgCreatePKM(_mySql, true, dr["IDSC"].ToString()); //в IDSC лежит id PKM
          dlg.ShowDialog();
          sql = "select IsApproved from sc_pkm where id = " + dr["IDSC"].ToString();
          if (_mySql.SqlSelect(sql).Rows[0]["IsApproved"].ToString() != "1") return;
        }
        if (TypeMess == TypeMessages.KIP)
        {
          int TypeKIP = Convert.ToInt32(dr["typeID"]);
          int KIPID = Convert.ToInt32(dr["idSC"]);

          DataTable dt = _mySql.SqlSelect("select * from staff_kip where id = " + KIPID);
          CKIP KIP = new CKIP(dt.Rows[0]);
          KIP.SetEvent(TypeKIP, _mySql);

        }
        dr["isEnded"] = true;
        sql = "update messages set IsEnded = 1 where id = " + dr["id"].ToString();
        _mySql.SqlExec(sql);
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }
    }

  }
}
