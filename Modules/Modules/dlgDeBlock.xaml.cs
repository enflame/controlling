﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Modules;
using Microsoft.Win32;
using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для dlgDeBlock.xaml
  /// </summary>
  public partial class dlgDeBlock : Window
  {
    public string strReason
    {
      get { return tb_reason.Text; }
    }
    public dlgDeBlock(int typeHistory)
    {
      InitializeComponent();
      if (typeHistory == TypeHistory.SC_DeBlock)
      {
        Title = "Разблокировка показателя";
        b_UnBlock_Text.Text = "Разблокировать";
      }
      if (typeHistory == TypeHistory.SC_DeDelegation)
      {
        Title = "Снять делегирование";
        b_UnBlock_Text.Text = "Снять делегирование";
      }
    }

    private void b_UnBlock_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = true;
    }

    private void b_ready_attachment_Click(object sender, RoutedEventArgs e)
    {
      OpenFileDialog dlg = new OpenFileDialog();
      if ((bool)dlg.ShowDialog())
      {
        //       _dr["b_ready_attachment"] = dlg.FileName;
      }
    }
  }
  public class TypeHistory
  {
    static public int SC = 1;
    //    static public int SC_period = 2;
    //    static public int SC_fact = 3;
    static public int SC_DeDelegation = 4;
    static public int SC_Delegation = 5;
    static public int SC_DeBlock = 6;
    static public int SC_Approved = 7;

    static public void Insert(CMySql mySql, string idSC, string reason_history, string id_form_created_history, int type_action)
    {
      string caption = "";
      if (type_action == TypeHistory.SC_DeBlock) caption = "Разблокировка";
      if (type_action == TypeHistory.SC_DeDelegation) caption = "Снятие делегирования";
      if (type_action == TypeHistory.SC_Delegation) caption = "Делегирование";
      if (type_action == TypeHistory.SC_Approved) caption = "Утверждение";

      string sql = "insert into history (idSC, date_history, caption, reason_history, id_form_created_history, type_action, TargetYear) values (" +
        idSC + ", '" + DateTime.Now.ToShortDateString() + "', '" + caption + "', '" + reason_history + "', " + id_form_created_history + ", " + type_action.ToString() + ", " + UserInfo.TargetYear + ")";
      mySql.SqlExec(sql);
    }
  }
}
