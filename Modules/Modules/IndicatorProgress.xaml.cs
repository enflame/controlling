﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для IndicatorProgress.xaml
  /// </summary>
  public partial class IndicatorProgress : UserControl
  {
 
    LinearGradientBrush good_brush;
    LinearGradientBrush bad_brush;

    public static DependencyProperty Fact;
    public static DependencyProperty Plan;
    public static DependencyProperty Weight;
    public object plan
    {
      get { return GetValue(Plan); }
      set { SetValue(Plan, value); }

    }
    public object fact
    {
      get { return GetValue(Fact); }
      set { SetValue(Fact, value); }
    }
    public object weight
    {
      get { return GetValue(Weight); }
      set { SetValue(Weight, value); }
    }

    public static DependencyProperty LowCriticalLevel;
    public static DependencyProperty HiCriticalLevel;
    public object lowCriticalLevel
    {
      get { return GetValue(LowCriticalLevel); }
      set { SetValue(LowCriticalLevel, value); }

    }
    public object hiCriticalLevel
    {
      get { return GetValue(HiCriticalLevel); }
      set { SetValue(HiCriticalLevel, value); }
    }

    public IndicatorProgress()
    {
      InitializeComponent();

      {
        good_brush = new LinearGradientBrush();
        good_brush.StartPoint = new Point(0, 0.5);
        good_brush.EndPoint = new Point(1, 0.5);
        GradientStop gradientStop1 = new GradientStop();
        gradientStop1.Offset = 0.6;
        gradientStop1.Color = Colors.White;
        good_brush.GradientStops.Add(gradientStop1);
        GradientStop gradientStop2 = new GradientStop();
        gradientStop2.Offset = 0;
        gradientStop2.Color = Colors.DarkGreen;
        good_brush.GradientStops.Add(gradientStop2);
      }
      {
        bad_brush = new LinearGradientBrush();
        bad_brush.StartPoint = new Point(0, 0.5);
        bad_brush.EndPoint = new Point(1, 0.5);
        GradientStop gradientStop1 = new GradientStop();
        gradientStop1.Offset = 0.6;
        gradientStop1.Color = Colors.White;
        bad_brush.GradientStops.Add(gradientStop1);
        GradientStop gradientStop2 = new GradientStop();
        gradientStop2.Offset = 0;
        gradientStop2.Color = Colors.Red;
        bad_brush.GradientStops.Add(gradientStop2);
      }

      pb.Style = (Style)this.Resources["GoodProgressStyle"];
      if (pb.Value != 0)
        if (pb.Value < 100) pb.Style = (Style)this.Resources["BadProgressStyle"];
    }
    static IndicatorProgress()
    {
      Fact = DependencyProperty.Register("fact", typeof(object), typeof(IndicatorProgress), new FrameworkPropertyMetadata(0, new PropertyChangedCallback(OnValueChanged)));
      Plan = DependencyProperty.Register("plan", typeof(object), typeof(IndicatorProgress), new FrameworkPropertyMetadata(0, new PropertyChangedCallback(OnValueChanged)));
      Weight = DependencyProperty.Register("weight", typeof(object), typeof(IndicatorProgress), new FrameworkPropertyMetadata(0, new PropertyChangedCallback(OnValueChanged)));
      LowCriticalLevel = DependencyProperty.Register("lowCriticalLevel", typeof(object), typeof(IndicatorProgress), new FrameworkPropertyMetadata(0, new PropertyChangedCallback(OnValueChanged)));
      HiCriticalLevel = DependencyProperty.Register("hiCriticalLevel", typeof(object), typeof(IndicatorProgress), new FrameworkPropertyMetadata(0, new PropertyChangedCallback(OnValueChanged)));
    }

    private static void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      IndicatorProgress ind = (IndicatorProgress)sender;

      double p = 0;
      double f = 0;
      double w = 0;
      p = Convert.ToDouble(ind.plan);
      f = Convert.ToDouble(ind.fact);
      w = Convert.ToDouble(ind.weight);

      if (e.Property == Fact)
      {
        p = Convert.ToDouble(ind.plan);
        f = Convert.ToDouble(e.NewValue);
      }
      if (e.Property == Plan)
      {
        p = Convert.ToDouble(e.NewValue);
        f = Convert.ToDouble(ind.fact);
      }

      if (p == 0) return;

      double v = f / p * 100;
      if (w != 0) v = f / p / w * 100;
      ind.pb.Value = v; ind.l_progress.Content = v.ToString("f0") + "%";
      if (ind.pb.Value != 0)
        if (ind.pb.Value < 100) ind.pb.Background = Brushes.Red;
      
    }
    private static void OnFactChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      IndicatorProgress ind = (IndicatorProgress)sender;
      double p = Convert.ToDouble(ind.plan);
      double f = Convert.ToDouble(e.NewValue);
      double w = Convert.ToDouble(ind.weight);
      if (p == 0) return;

      double v = f / p * 100;
      if (w != 0) v = f / p / w * 100;

      ind.pb.Value = v; ind.l_progress.Content = v.ToString("f0") + "%";
    }
    private static void OnPlanChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      IndicatorProgress ind = (IndicatorProgress)sender;

      double p = Convert.ToDouble(e.NewValue);
      double f = Convert.ToDouble(ind.fact);
      double w = Convert.ToDouble(ind.weight);
      if (p == 0) return;
      double v = f / p * 100;
      if (w != 0) v = f / p / w * 100;

      ind.pb.Value = v; ind.l_progress.Content = v.ToString("f0") + "%";
    }

    public void Set()
    {
      pb.Style = (Style)this.Resources["GoodProgressStyle"];
      if (pb.Value != 0)
        if (pb.Value < 100) pb.Style = (Style)this.Resources["BadProgressStyle"];
    }
  }
}
