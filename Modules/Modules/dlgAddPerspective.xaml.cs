﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для dlgAddPerspective.xaml
  /// </summary>
  public partial class dlgAddPerspective : Window
  {
    public string NameSC
    {
      get { return tb_name.Text; }
      set { tb_name.Text = value; }
    }
    public dlgAddPerspective(string caption)
    {
      InitializeComponent();
      tb_name.Focus();
      this.Title = caption;
    }

    void ok()
    {
      DialogResult = true;
      Close();
    }
    private void b_ok_Click(object sender, RoutedEventArgs e)
    {
      ok();
    }

    private void b_cancel_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = false;
      Close();
    }

    private void Window_Deactivated(object sender, EventArgs e)
    {
      //     DialogResult = false;
      //     Close();
    }

    private void tb_name_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter) ok();
    }
  }
}
