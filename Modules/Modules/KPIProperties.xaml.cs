﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

using System.Windows.Controls.DataVisualization.Charting;
using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для KPIProperties.xaml
  /// </summary>
  public partial class KPIProperties : UserControl
  {
    public const int IsApprovedConst = 1;
    public const int IsNotApprovedConst = 2;
    public const int IsSaved = 3;

    string _idSC;
    CMySql _mySql;
    Monitoring _monitoring;
    double _sum_period_plan;
    double _sum_period_fact;

//    DataTable dt_SC_Period;
    //  DataTable dt_KPI_delegation;
    List<SCPeriod> SCPeriods;

    DataTable dt_History;

    DataTable _dt_SC;
    DataRow _dr_SC;
    DataTable _dt_SC_old;

    DataRow _dr_parentSC;
    string _idSC_root;

    bool isIDcontrolPerson;
    public bool isKPISetting;
    bool handleSelectionPeriod;
    List<DelegateUser> _delegUser = new List<DelegateUser>();

    bool IsEqualTables(DataTable tb1, DataTable tb2)
    {
      if (tb1.Rows.Count != tb2.Rows.Count) return false;
      if (tb1.Rows[0].ItemArray.Length != tb2.Rows[0].ItemArray.Length) return false;

      for (int i = 0; i < tb1.Rows.Count; i++)
        for (int j = 0; j < tb1.Rows[0].ItemArray.Length; j++)
          if (tb1.Rows[i].ItemArray[j].ToString() != tb2.Rows[i].ItemArray[j].ToString()) return false;

      return true;
    }
    public KPIProperties()
    {
      InitializeComponent();

      cb_period.Items.Clear();
      cb_period.Items.Add(Period.No);
      cb_period.Items.Add(Period.Year);
      cb_period.Items.Add(Period.HalfYear);
      cb_period.Items.Add(Period.Quarter);
      cb_period.Items.Add(Period.Month);

      cb_period_input_data.Items.Clear();
      cb_period_input_data.Items.Add(Period.No);
      cb_period_input_data.Items.Add(Period.Year);
      cb_period_input_data.Items.Add(Period.HalfYear);
      cb_period_input_data.Items.Add(Period.Quarter);
      cb_period_input_data.Items.Add(Period.Month);
      cb_period_input_data.SelectedItem = 4;
      isKPISetting = false;
      handleSelectionPeriod = true;

    }

    void ReloadDelegate(bool isControlPerson)
    {
      //делегирование переделать запрос

      //string sql = "select STAFF_KPI.ID, STAFF_KPI.ID_FORM, STAFF_KPI.ID_KPI, STAFF_KPI.TARGETVALUE, STAFF_KPI.WEIGHT, STAFF_KPI.ID_POSITION, " +
      //             "U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO, " +
      //             "UF.DOLGNOST, UF.PODRAZDELENIE_ID as id_dep " +
      //             "from staff_KPI " +
      //             "inner join app_portal.v_tpu_users U on U.LICHNOST_ID=staff_KPI.id_form " +
      //             "inner join SOTRUDNIK.LICHNOST_FOR_PHONE_SPR_V3 UF on UF.LICHNOST_ID=staff_KPI.id_form  and  UF.DOLGNOST_ID =  STAFF_KPI.ID_POSITION " +
      //             "where staff_KPI.id_KPI = " + _idSC + " and staff_KPI.targetyear = " + UserInfo.TargetYear.ToString() + " order by staff_KPI.id";

      string sql = "select * from VIEW_DELEGATE " +
        "where id_KPI = " + _idSC + " and targetyear = " + UserInfo.TargetYear.ToString();

      DataTable dt_KPI_delegation = _mySql.SqlSelect(sql);
      dt_KPI_delegation.Columns.Add(new DataColumn("EnabledOnlyForControlPerson", Type.GetType("System.Boolean")));
      foreach (DataRow dr in dt_KPI_delegation.Rows)
      {
        dr["EnabledOnlyForControlPerson"] = isControlPerson;
      }

      _delegUser = new List<DelegateUser>();
      foreach (DataRow dr in dt_KPI_delegation.Rows)
      {
        DelegateUser user = new DelegateUser();
        user.ID_FORM = dr["ID_FORM"].ToString();
        user.ID = dr["ID"].ToString();
        user.FIO = dr["FIO"].ToString();
        user.DOLGNOST = dr["DOLGNOST"].ToString();
        user.TARGETVALUE = Convert.ToDouble(dr["TARGETVALUE"]);
        user.EnabledOnlyForControlPerson = isControlPerson;


        user.id_dep = dr["id_dep"].ToString();
        user.id_position = dr["id_position"].ToString();
        user.id_KPI = dr["id_KPI"].ToString();
        user.weight = Convert.ToDouble( dr["weight"]);

        _delegUser.Add(user);
      }
      dg_KPI_delegation.ItemsSource = _delegUser;
      //   dg_KPI_delegation.ItemsSource = dt_KPI_delegation.DefaultView;
    }
    void RenewPeriod()
    {
      string sql = "select * from SC_Period where idSC = " + _idSC + " and targetyear = " + UserInfo.TargetYear.ToString() + " order by id";
      DataTable dt_SC_Period = _mySql.SqlSelect(sql);
      if (_mySql.ToInt32(_dr_SC["level1"]) > 20)
        if ((dt_SC_Period.Rows.Count == 0) & (Convert.ToInt32(_dr_parentSC["level1"]) != TypeHeader.Perspective))
        {
          _dr_SC["targetBeginDate"] = _dr_parentSC["targetBeginDate"].ToString();
          _dr_SC["targetEndDate"] = _dr_parentSC["targetEndDate"].ToString();
          _dr_SC["period"] = _dr_parentSC["period"].ToString();

          SetPeriod(_dr_parentSC["period"].ToString());
          //          Save();

          sql = "select * from SC_Period where idSC = " + _idSC + " and targetyear = " + UserInfo.TargetYear.ToString();
          dt_SC_Period = _mySql.SqlSelect(sql);
        }
      SCPeriods = new List<SCPeriod>();

  
      _sum_period_plan = 0;
      bool isFormula = true;
      if (tb_Formula.Text == "") isFormula = false;
      if (nUpDown_precision.Value == null) nUpDown_precision.Value = 0; //не нужно

      foreach (DataRow dr in dt_SC_Period.Rows)
      {
        SCPeriod p = new SCPeriod();
        p.id = Convert.ToInt32(dr["id"]);
        p.Plan = Convert.ToDouble(dr["plan"]);
        p.Fact = Convert.ToDouble(dr["fact"]);
        p.lowCriticalLevel = Convert.ToDouble(dr["lowCriticalLevel"]);
        p.hiCriticalLevel = Convert.ToDouble(dr["hiCriticalLevel"]);
        p.DateBegin = (DateTime)dr["DateBegin"];
        p.DateEnd = (DateTime)dr["DateEnd"];
        p.Period = dr["Period"].ToString();

        _sum_period_plan += p.Plan;
        _sum_period_fact += p.Fact;

        p.isFormula = isFormula;
        p.StrFormat = "f" + nUpDown_precision.Value;
        p.isApproved =  Convert.ToBoolean(_dr_SC["isApproved"]);
        SCPeriods.Add(p);
      }
//      UpdatePeriodPrecision();
      dg_Period.ItemsSource = SCPeriods;
      ShowPeriodErrors();
      _monitoring.Set(_mySql, _dr_SC["id"].ToString(), _dr_SC["name1"].ToString());
    }
    void RenewActions()
    {
      Activities.Set(_dr_SC, _mySql, false);
    }
    void RenewKPI()
    {
      string sql = "select SC.*from SC " +
                   "where id = " + _idSC_root + " and targetyear = " + UserInfo.TargetYear.ToString();
      _dr_parentSC = _mySql.SqlSelect(sql).Rows[0];


      //      sql = "select * from SC where id = " + _idSC + " and targetyear = " + UserInfo.TargetYear.ToString();
      sql = "select SC.*, U1.FAMILIYA||' '||U1.IMYA||' '||U1.OTCHESTVO as FIO_CONTROL, U2.FAMILIYA||' '||U2.IMYA||' '||U2.OTCHESTVO as FIO_RESP " +
            "from SC " +
            "left join app_portal.v_tpu_users U1 on U1.LICHNOST_ID = SC.IDCONTROLPERSON " +
            "left join app_portal.v_tpu_users U2 on U2.LICHNOST_ID = SC.IDRESPONSIBLEPERSON " +
            "where SC.id = " + _idSC + " and SC.targetyear = " + UserInfo.TargetYear.ToString();
      _dt_SC = _mySql.SqlSelect(sql);

      _dt_SC.Columns.Add(new DataColumn("ReadOnlyForNotControlPerson", Type.GetType("System.Boolean")));
      _dr_SC = _dt_SC.Rows[0];
      if (_dr_SC["IDcontrolPerson"].ToString() != UserInfo.Info.id_form) _dr_SC["ReadOnlyForNotControlPerson"] = true;
      else _dr_SC["ReadOnlyForNotControlPerson"] = false;

      _dt_SC.Columns.Add(new DataColumn("EnabledOnlyForControlPerson", Type.GetType("System.Boolean")));
      _dr_SC = _dt_SC.Rows[0];
      if (_dr_SC["IDcontrolPerson"].ToString() != UserInfo.Info.id_form) _dr_SC["EnabledOnlyForControlPerson"] = false;
      else _dr_SC["EnabledOnlyForControlPerson"] = true;

      tb_Formula.Text = _dt_SC.Rows[0]["Formula"].ToString();
      string formula = _dt_SC.Rows[0]["Formula"].ToString();
      if (formula != "") KPIParser.FromIDToNames(_mySql, _dt_SC.Rows[0]["Formula"].ToString(), ref formula);
      tb_Formula.Text = formula;
      tbl_Formula.Text = formula;

      if (_dr_SC["IDresponsiblePerson"].ToString() == "") _dr_SC["IDresponsiblePerson"] = -1;
      if (_dr_SC["IDcontrolPerson"].ToString() == "") _dr_SC["IDcontrolPerson"] = -1;

      int level1 = _mySql.ToInt32(_dr_SC["level1"]);
      if ((level1 > TypeHeader.Goal) | (UserInfo.Info.id_form != _dr_SC["IDcontrolPerson"].ToString()))
      {
        cb_period.IsEnabled = false;
        //cb_unit.IsEnabled = false;
        dt_targetBeginDate.IsReadOnly = true;
        dt_targetEndDate.IsReadOnly = true;
      }
      else
      {
        cb_period.IsEnabled = true;
        cb_unit.IsEnabled = true;
        dt_targetBeginDate.IsReadOnly = false;
        dt_targetEndDate.IsReadOnly = false;
      }
      if (Convert.ToInt32(_dr_SC["level1"]) == TypeHeader.IndependScore)
      {
        cb_period.IsEnabled = true;
        cb_unit.IsEnabled = true;
        dt_targetBeginDate.IsReadOnly = false;
        dt_targetEndDate.IsReadOnly = false;
      }

      //Период
      RenewPeriod();
      if (UserInfo.Info.id_form != _dr_SC["IDcontrolPerson"].ToString())
      {
        dg_Period.IsEnabled = false;
      }
      else
      {
        dg_Period.IsEnabled = true;
      }


      //делегирование
      ReloadDelegate(UserInfo.Info.id_form == _dr_SC["IDcontrolPerson"].ToString() ? true : false);

      //Мероприятия
      RenewActions();

      RenewHistory();
       
      if (cb_period.Text == Period.Year) dg_Period.Columns[1].Visibility = System.Windows.Visibility.Collapsed;

//      grid_KPI.DataContext = _dt_SC;
      sp_KPI.DataContext = _dt_SC;
      sp_topPanel.DataContext = _dt_SC;
      b_NewDelegation.DataContext = _dt_SC;

      if (Convert.ToInt32(_dr_SC["isApproved"]) == 1)
      {//убираем колонку удалить
        dg_KPI_delegation.Columns[3].Visibility = System.Windows.Visibility.Collapsed;
      }
    }
    void RenewHistory()
    {
      string sql = "SELECT history.id, history.idSC, history.date_history, history.caption, history.reason_history, history.id_form_created_history, history.type_action, " +
                   "U.LICHNOST_ID, U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO " +
                   "FROM history " +
                   "inner join app_portal.v_tpu_users U on U.LICHNOST_ID=history.id_form_created_history " +
                   "where idSC=" + _idSC + " and history.targetyear = " + UserInfo.TargetYear.ToString();
      dt_History = _mySql.SqlSelect(sql);
      dg_History.ItemsSource = dt_History.DefaultView;
    }
    public void Set(string idSC, string idSC_root, CMySql mySql, Monitoring monitoring)
    {
      //if (_dt_SC_old != null)
      //if (!IsEqualTables(_dt_SC_old, _dt_SC))
      //  if (MessageBox.Show("Сохранить изменения!", "Сохранение", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      //  {
      //    Save();
      //  }

      _idSC = idSC;
      _idSC_root = idSC_root;
      _mySql = mySql;
      _monitoring = monitoring;
      RenewKPI();

      SetFormula();
      isKPISetting = true;

      UpdatePeriodPrecision();

      _dt_SC_old = _dt_SC.Copy();
    }

    private void DecimalUpDown_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      if (e.OldValue == null) return;
      DelegateUser user = (DelegateUser)dg_KPI_delegation.SelectedItem;
      //double oldV = Convert.ToDouble(e.OldValue);
     double newV = Convert.ToDouble(e.NewValue);
      //_sum_period_plan = _sum_period_plan - oldV + newV;
      user.TARGETVALUE = newV;
    }

    string Validate()
    {
      string res = "";
      //делегирование
      if (_delegUser.Count > 0)
      {
        double targetvalue = 0;
        foreach (DelegateUser dr in _delegUser)
          targetvalue += Convert.ToDouble(dr.TARGETVALUE);
        if (targetvalue != Convert.ToDouble(_dr_SC["targetvalue"]))
        {
          res = res + "Сумма делегирований должна быть равна целевому значению \r\n";
          return res;
        }

        string sc = "";
        if (_dr_SC["formula"].ToString() != "")

          if (Convert.ToDouble(_dr_SC["targetvalue"]) > 0)
            foreach (DelegateUser dr_KPI_delegation in _delegUser)
            {
              string sql = "select SC.id, SC.name1, SC.targetvalue from SCTree  INNER JOIN SC ON SCTree.id_SC = SC.id where SCTree.id_parent = " + _dr_SC["id"].ToString() +
                " and SC.IDControlPerson = " + dr_KPI_delegation.ID_FORM;
              DataTable dt = _mySql.SqlSelect(sql);
              targetvalue = 0;
              foreach (DataRow dr1 in dt.Rows)
              {
                targetvalue += Convert.ToDouble(dr1["targetvalue"]);
                sc += dr1["name1"].ToString() + " - " + dr1["targetvalue"].ToString() + "\r\n";
              }

              if (dt.Rows.Count > 0)
                if (targetvalue != Convert.ToDouble(dr_KPI_delegation.TARGETVALUE))
                  return "Делегированное целевое значение " + dr_KPI_delegation.TARGETVALUE.ToString() + " должно быть равно сумме целевых значений нижележащих показателей: \r\n\r\n " + sc;
            }
      }
      //if (nUpDown_lowCriticalLevel.Value == null) return "Неверно заполнены значения";
      //if (nUpDown_hiCriticalLevel.Value == null) return "Неверно заполнены значения";
      //if (_mySql.ToInt32(_dr_parentSC["level1"]) > 0)
      //{
      //  DateTime dt_root = Convert.ToDateTime(_dr_parentSC["targetBeginDate"]);
      //  DateTime dt = (DateTime)dt_targetBeginDate.Value;
      //  DateTime dt_root1 = Convert.ToDateTime(_dr_parentSC["targetEndDate"]);
      //  DateTime dt1 = (DateTime)dt_targetEndDate.Value;

      //  if ((dt < dt_root) | (dt1 > dt_root1)) return "Целевой период текущего не включается в целевой период корневого " + dt_root.ToShortDateString() + " - " + dt_root1.ToShortDateString();
      //}
      ShowPeriodErrors();
      return "";
    }

    void CopyToHistory(string reason)
    {
      string sql = "insert into SC_history select * from SC where  id = " + _idSC + " and targetyear = " + UserInfo.TargetYear.ToString();
      _mySql.SqlExec(sql);
    }
    void SaveDelegate()
    {
      string sql;
      foreach (DelegateUser dr in _delegUser)
      {
        sql = "update staff_KPI set targetValue = " + Functions.ToString(dr.TARGETVALUE) + " where id = " + dr.ID;

        _mySql.SqlExec(sql);
      }
    }
    void Save()
    {
      SetFormula();

      string formula = tb_Formula.Text;
      if (formula != "") KPIParser.FromNamesToId(_mySql, tb_Formula.Text, ref formula);

      string nameSC = tb_name1.Text.Trim();
      string sql = "update SC set name1 = '" + nameSC + "', period = '" + cb_period.Text + "', period_input_data = '" + cb_period_input_data.Text +
      "', unit = '" + cb_unit.Text + "', precision = " + nUpDown_precision.Value.ToString() +
      ", targetValue = " + Functions.ToString(nUpDown_targetValue.Value) +
      ", targetBeginDate = " + _mySql.DateToString((DateTime)dt_targetBeginDate.Value) + ", targetEndDate = " + _mySql.DateToString((DateTime)dt_targetEndDate.Value) +
      ", lowCriticalLevel = " + Functions.ToString(nUpDown_lowCriticalLevel.Value) + ", hiCriticalLevel = " + Functions.ToString(nUpDown_hiCriticalLevel.Value) +
      ", IDcontrolPerson = " + _dr_SC["IDcontrolPerson"].ToString() +
      ", Formula = '" + formula + "', IDresponsiblePerson = " + _dr_SC["IDresponsiblePerson"] +
      ", plan=" + _sum_period_plan.ToString("f" + nUpDown_precision.Value.Value.ToString()).Replace(',', '.') +
      ", fact=" + _sum_period_fact.ToString("f" + nUpDown_precision.Value.Value.ToString()).Replace(',', '.') +
      ", isApproved=" + _dr_SC["isApproved"].ToString() + " where id = " + _idSC;
      _mySql.SqlExec(sql);

      foreach (SCPeriod dr in SCPeriods)
      {
        sql = "update SC_Period set period = '" + dr.Period + "', DateBegin = " + _mySql.DateToString(dr.DateBegin) + ", DateEnd = " + _mySql.DateToString(dr.DateEnd) +
          ", plan = " + dr.Plan + ", hiCriticalLevel = " + dr.hiCriticalLevel + ", lowCriticalLevel = " +dr.lowCriticalLevel +
          ", fact = " + dr.Fact + " where id = " + dr.id;

        _mySql.SqlExec(sql);
      }
      Activities.Save();
      SaveDelegate();

      TimeMessage_menu_KPISave.Show("Сохранено", false);
      OnSCEvent(new SCEventArgs() { TypeEvent = IsSaved, obj = nameSC, idSC = _idSC });

      _dt_SC_old = _dt_SC.Copy();
      _monitoring.Set(_mySql, _dr_SC["id"].ToString(), _dr_SC["name1"].ToString());
    }
    private void menu_KPISave_Click(object sender, RoutedEventArgs e)
    {
      string err = Validate();
      if (err != "") MessageBox.Show(err);
      Save();
    }

    private void nUpDown_precision_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      UpdatePeriodPrecision();
    }
    private void b_selectResponsiblePerson_Click(object sender, RoutedEventArgs e)
    {
      if (((Button)sender).Name == "b_removeResponsiblePerson")
      {
        tb_IDresponsiblePerson.Text = "";
        _dr_SC["IDresponsiblePerson"] = -1;
      }
      else
      {
        isIDcontrolPerson = false;
        dlgSelectStaff wnd = new dlgSelectStaff(_mySql, new dlgSelectStaff.SelectStaffEventHandler(onSelectedStaff));
        Point p = PointToScreen(Mouse.GetPosition(b_selectResponsiblePerson));
        wnd.Left = p.X;
        wnd.Top = p.Y;

        wnd.Show();
      }
    }
    void onSelectedStaff(Object sender, StaffTree.SelectedItemEventArgs args)
    {
      if (isIDcontrolPerson)
      {
        tb_IDcontrolPerson.Text = args.staffInfo.fio;
        _dr_SC["IDcontrolPerson"] = args.staffInfo.id_form;
      }
      else
      {
        tb_IDresponsiblePerson.Text = args.staffInfo.fio;
        _dr_SC["IDresponsiblePerson"] = args.staffInfo.id_form;
      }
    }

    #region Period

    void UpdatePeriodPrecision()
    {
      //if (!isKPISetting) return;
      //if (dt_SC_Period == null) return;

      //foreach (DataRow dr in dt_SC_Period.Rows)
      //{
      //  if (nUpDown_precision.Value == 0) dr["precision"] = 1;
      //  else
      //  {
      //    if (nUpDown_precision.Value > 0) dr["precision"] = 1;
      //    for (int i = 0; i < nUpDown_precision.Value; i++) dr["precision"] = (Convert.ToDouble(dr["precision"])) / 10.0;
      //  }
      //}
    }
    void UpdateFactPeriodIsFormula(bool isFormula)
    {
      //foreach (DataRow dr in dt_SC_Period.Rows)
      //{
      //  for (int i = 0; i < nUpDown_precision.Value; i++) dr["isFormula"] = isFormula;
      //}
    }
    private void b_CreateKPIPeriod_Click(object sender, RoutedEventArgs e)
    {
      //dlgPeriod wnd = new dlgPeriod(cb_period.Text, ((DateTime)dt_targetEndDate.Value).Year);
      //Point p = PointToScreen(Mouse.GetPosition((Button)sender));
      //wnd.Left = p.X;
      //wnd.Top = p.Y;

      //Nullable<bool> res = wnd.ShowDialog();
      //if ((bool)res)
      //{
      //  if (wnd.period != null)
      //  {

      //    string sql = "delete from SC_Period where idSC = " + _idSC;
      //    _mySql.SqlExec(sql);
      //    foreach (string m in wnd.period)
      //    {
      //      sql = "insert into SC_period (idSC, year1, lowCriticalLevel, hiCriticalLevel, period, TaegetYear) values(" + _idSC + ", " + wnd.yearFrom.ToString() +
      //        ", " + nUpDown_lowCriticalLevel.Text + ", " + nUpDown_hiCriticalLevel.Text + ", '" + m + "', " + UserInfo.TargetYear + ")";
      //      _mySql.SqlExec(sql);
      //    }
      //    sql = "select * from SC_Period where idSC = " + _idSC + " and targetyear = " + UserInfo.TargetYear.ToString();
      //    dt_SC_Period = _mySql.SqlSelect(sql);
      //    dg_Period.ItemsSource = dt_SC_Period.DefaultView;

      //  }
      //}
    }
    private void cb_period_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!isKPISetting) return;
      try
      {
        if (handleSelectionPeriod)
          if (e.AddedItems.Count > 0)
          {
            if (SetPeriod((string)e.AddedItems[0]) == -1)
            {
              handleSelectionPeriod = false;
              cb_period.SelectedItem = e.RemovedItems[0];
              return;
            }
            UpdatePeriodPrecision();

            if ((string)e.AddedItems[0] == Period.Year)
              dg_Period.Columns[1].Visibility = System.Windows.Visibility.Collapsed;
            else dg_Period.Columns[1].Visibility = System.Windows.Visibility.Visible;
          }
        handleSelectionPeriod = true;
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message, "Ошибка разбиения целевого промежутка");
      }
    }
    int SetPeriod(string period)
    {
      //string err = Validate();
      //if (err != "")
      //{
      //  MessageBox.Show(err);
      //  return -1;
      //}
      if (SCPeriods.Count > 0)
        if (MessageBox.Show("Текущее распределение целевого периода будет недоступно. \r\n Продолжить?", "Задание периода", MessageBoxButton.YesNo) == MessageBoxResult.No) return -1;
      _sum_period_plan = 0;
      if ((_dr_SC["targetBeginDate"].ToString() == "") | (_dr_SC["targetEndDate"].ToString() == "")) return -1;
      string sql = "delete from SC_Period where idSC = " + _idSC;
      _mySql.SqlExec(sql);

      DateTime y1 = DateTime.Parse(_dr_SC["targetBeginDate"].ToString());
      DateTime y2 = DateTime.Parse(_dr_SC["targetEndDate"].ToString());
      dg_Period.Columns[1].Visibility = System.Windows.Visibility.Visible;
      string lowCriticalLevel = (nUpDown_lowCriticalLevel.Value.ToString()).Replace(",", ".");
      string hiCriticalLevel = (nUpDown_hiCriticalLevel.Value.ToString()).Replace(",", ".");

      TimeSpan ts_day = new TimeSpan(1, 0, 0, 0);
      if (period == Period.Year)
      {
        while (y1 < y2)
        {
          DateTime ye = new DateTime(y1.Year + 1, y1.Month, y1.Day);
          if (ye > y2) ye = y2 + ts_day;
          sql = "insert into SC_period (idSC, DateBegin, DateEnd, lowCriticalLevel, hiCriticalLevel, period, targetYear) values(" + _idSC + ", " +
            _mySql.DateToString(y1) + ", " + _mySql.DateToString(ye - ts_day) +
        ", " + lowCriticalLevel + ", " + hiCriticalLevel + ", '" + y1.ToString() + "', " + UserInfo.TargetYear + ")";
          _mySql.SqlExec(sql);

          y1 = ye;
        }
        //sql = "insert into SC_period (idSC, DateBegin, DateEnd, lowCriticalLevel, hiCriticalLevel, period) values(" + _idSC + ", '" + y1.ToString() + "', '" + (y1 + ts).ToString() +
        //      "', " + lowCriticalLevel + ", " + hiCriticalLevel + ", '" + y1.ToString() + "')";
        //_mySql.SqlExec(sql);

        if (cb_period.Text == Period.Year)
          dg_Period.Columns[1].Visibility = System.Windows.Visibility.Collapsed;
      }
      if (period == Period.HalfYear)
      {
        while (y1 < y2)
        {
          foreach (string s in Period.H)
          {
            if (y1 >= y2)
            {
              y1 = new DateTime(y1.Year + 1, y1.Month, y1.Day);
              break;
            }

            int month = y1.Month + 6;
            int year = y1.Year;
            if (month > 12)
            {
              month = month - 12;
              year++;
            }
            DateTime ye = new DateTime(year, month, y1.Day);
            if (ye > y2) ye = y2 + ts_day;
            sql = "insert into SC_period (idSC, DateBegin, DateEnd, lowCriticalLevel, hiCriticalLevel, period, targetYear) values(" + _idSC + ", " +
              _mySql.DateToString(y1) + ", " + _mySql.DateToString(ye - ts_day) +
            ", " + lowCriticalLevel + ", " + hiCriticalLevel + ", '" + s + "', " + UserInfo.TargetYear + ")";
            _mySql.SqlExec(sql);

            y1 = ye;
          }
        }
      }
      if (period == Period.Quarter)
      {

        while (y1 < y2)
        {
            DateTime dt1 = new DateTime();
            DateTime dt2 = new DateTime();
            string name_period = Period.GetQuarter(y1, ref dt1, ref dt2);

            sql = "insert into SC_period (idSC, DateBegin, DateEnd, lowCriticalLevel, hiCriticalLevel, period, targetYear) values(" + _idSC + ", " +
            _mySql.DateToString(y1) + ", " + _mySql.DateToString(dt2) +
          ", " + lowCriticalLevel + ", " + hiCriticalLevel + ", '" + name_period + "', " + UserInfo.TargetYear + ")";
            _mySql.SqlExec(sql);

            y1 = dt2 + new TimeSpan(1, 0, 0, 0, 0);

            if (dt2 > y2)
            {
              name_period = Period.GetQuarter(y1, ref dt1, ref dt2);

              sql = "insert into SC_period (idSC, DateBegin, DateEnd, lowCriticalLevel, hiCriticalLevel, period, targetYear) values(" + _idSC + ", " +
              _mySql.DateToString(y1) + ", " + _mySql.DateToString(y2) +
            ", " + lowCriticalLevel + ", " + hiCriticalLevel + ", '" + name_period + "', " + UserInfo.TargetYear + ")";
              _mySql.SqlExec(sql);
            }

        }

      }
      if (period == Period.Month)
      {
        while (y1 < y2)
        {
          //         _dr_SC["targetBeginDate"]
          List<YM> ym = Period.GetMonthes((DateTime)_dr_SC["targetBeginDate"], (DateTime)_dr_SC["targetEndDate"]);
          //          List<YM> ym = Period.GetMonthes((DateTime)dt_targetBeginDate.Value, (DateTime)dt_targetEndDate.Value);
          foreach (YM s in ym)
          {
            if (y1 >= y2)
            {
              y1 = new DateTime(y1.Year + 1, y1.Month, y1.Day);
              break;
            }
            int month = y1.Month + 1;
            int year = y1.Year;
            if (month > 12)
            {
              month = month - 12;
              year++;
            }
            DateTime ye = new DateTime(year, month, y1.Day);
            if (ye > y2) ye = y2 + ts_day;

            sql = "insert into SC_period (idSC, DateBegin, DateEnd, lowCriticalLevel, hiCriticalLevel, period, TargetYear) values(" + _idSC + ", " +
              _mySql.DateToString(y1) + ", " + _mySql.DateToString(ye - ts_day) + ", " + lowCriticalLevel + ", " + hiCriticalLevel + ", '" + s.M + "', " + UserInfo.TargetYear + ")";
            _mySql.SqlExec(sql);
            y1 = ye;
          }

        }
      }

      sql = "update SC_period set plan=(select TARGETVALUE from SC where ID=" + _idSC + ") where idSC=" + _idSC + " and id=(select max(id) from SC_period where idSC=" + _idSC + " and targetyear = " + UserInfo.TargetYear.ToString() + ")";
      _mySql.SqlExec(sql);

      RenewPeriod();

      //sql = "select * from SC_Period where idSC = " + _idSC + " and targetyear = " + UserInfo.TargetYear.ToString();
      //dt_SC_Period = _mySql.SqlSelect(sql);
      //dt_SC_Period.Columns.Add("precision", Type.GetType("System.Double"));
      //dt_SC_Period.Columns.Add("isFormula", Type.GetType("System.Boolean"));

      //dg_Period.ItemsSource = dt_SC_Period.DefaultView;

  
      return 1;
    }
  
    void ShowPeriodErrors()
    {
      tb_period_error.Text = ""; tb_period_error.Visibility = System.Windows.Visibility.Collapsed;
      _sum_period_plan = Convert.ToDouble(_sum_period_plan.ToString("f" + Convert.ToInt16(nUpDown_precision.Value).ToString()));
      if (_sum_period_plan > Convert.ToDouble(nUpDown_targetValue.Value))
      {
        tb_period_error.Text = String.Format("Сумма плановых значений ({0}) больше целевого", _sum_period_plan.ToString("f" + Convert.ToInt16(nUpDown_precision.Value).ToString()));
      }
      if (_sum_period_plan < Convert.ToDouble(nUpDown_targetValue.Value))
      {
        tb_period_error.Text = String.Format("Сумма плановых значений ({0}) меньше целевого", _sum_period_plan.ToString("f" + Convert.ToInt16(nUpDown_precision.Value).ToString()));
      }
      //if (tb_period_error.Text != "") tb_period_error.Visibility = System.Windows.Visibility.Visible;
    }
    private void DecimalUpDown_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      if (e.OldValue == null) return;
      double oldV = Convert.ToDouble(e.OldValue);
      double newV = Convert.ToDouble(e.NewValue);
      _sum_period_plan = _sum_period_plan - oldV + newV;
      ShowPeriodErrors();
    }
    #endregion Period

    private void b_matrix_Click(object sender, RoutedEventArgs e)
    {

      //Button b = (Button)sender;
      //DataRow dr = ((DataRowView)dg_KPI_delegation.SelectedItems[0]).Row;
      //string id_form = dr["id_form"].ToString();
      //StaffTree.SelectedItemEventArgs args = new StaffTree.SelectedItemEventArgs();
      //args.staffInfo.dep = dr["dep"].ToString();
      //args.staffInfo.fio = dr["fio"].ToString();
      //args.staffInfo.id_dep = dr["id_dep"].ToString();
      //args.staffInfo.id_position = dr["id_position"].ToString();
      //args.staffInfo.id_form = dr["id_form"].ToString();
      //args.staffInfo.position = dr["position"].ToString();

      //dlgStaff dlg = new dlgStaff(args, _mySql);
      //dlg.ShowControl(false);
      //dlg.ShowDialog();
      //ReloadDelegate(UserInfo.Info.id_form == _dr_SC["IDcontrolPerson"].ToString() ? true : false);
    }

    private void b_NewDelegation_Click(object sender, RoutedEventArgs e)
    {
      int level1 = _mySql.ToInt32(_dr_SC["level1"]);
      //if (level1 > TypeHeader.Goal)
      //{
      //  MessageBox.Show("Делегирование на втором уровне заблокировано");
      //  return;
      //}

      isIDcontrolPerson = true;
      dlgSelectStaff wnd = new dlgSelectStaff(_mySql, new dlgSelectStaff.SelectStaffEventHandler(onUpdateDelegationKPI));
      Point p = PointToScreen(Mouse.GetPosition((Button)sender));
      wnd.Left = p.X;
      wnd.Top = p.Y;

      wnd.Show();
    }
    void onUpdateDelegationKPI(Object sender, StaffTree.SelectedItemEventArgs args)
    {
      SaveDelegate();
      DelegateUser dr = new DelegateUser();

      dr.FIO = args.staffInfo.fio;
      dr.DOLGNOST = args.staffInfo.position;
      dr.id_dep = args.staffInfo.id_dep;
      dr.id_position = args.staffInfo.id_position;
      dr.ID_FORM = args.staffInfo.id_form;
      dr.id_KPI = _idSC;
      dr.weight = 0;


      DelegationTree.InsertDelegateKPItoStaff(_mySql, dr);

      ReloadDelegate(UserInfo.Info.id_form == _dr_SC["IDcontrolPerson"].ToString() ? true : false);

     }

    #region Formula
    private void menu_KPIRenew_Click(object sender, RoutedEventArgs e)
    {
      RenewKPI();
    }
    private void tb_Formula_PreviewDragEnter(object sender, DragEventArgs e)
    {
      TreeViewItem item = (TreeViewItem)e.Data.GetData(typeof(TreeViewItem));
      if (item != null)
      {
        TreeViewItemHeader h = (TreeViewItemHeader)((TreeViewItem)item).Header;
        e.Data.SetData("[" + h.Text + "]");
      }

    }
    private void tb_Formula_PreviewDrop(object sender, DragEventArgs e)
    {
      string s = (string)e.Data.GetData(typeof(string));
      //      MessageBox.Show(s);
      //		tb_Formula.Text = tb_Formula.Text + s;
    }

    private void tb_Formula_KeyDown(object sender, KeyEventArgs e)
    {

    }
    string ValidatePeriodsForFormula(string id)
    {
      string res = "";
      //проверка на период
      string sql = "select * from SC where id = " + id + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataRow dr1 = _mySql.SqlSelect(sql).Rows[0];

      DateTime dt = (DateTime)dt_targetBeginDate.Value;
      DateTime dt1 = Convert.ToDateTime(dr1["targetBeginDate"]);
      if (dt != dt1) return "Целевая дата начала показателя \"" + dr1["name1"].ToString() + "\" не совпадает с датой текущего показателя";

      dt = (DateTime)dt_targetEndDate.Value;
      dt1 = Convert.ToDateTime(dr1["targetEndDate"]);
      if (dt != dt1) return "Целевая дата окончания показателя \"" + dr1["name1"].ToString() + "\" не совпадает с датой текущего показателя";
      if (cb_period.Text != dr1["period"].ToString()) return "Период разбиения показателя \"" + dr1["name1"].ToString() + "\" не совпадает с периодом текущего показателя";

      return res;
    }

    void EvaluateFormula(string idSC)
    {
      string sql = "select * from SC where id =  " + idSC + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataRow drSC = _mySql.SqlSelect(sql).Rows[0];


      string formula = drSC["formula"].ToString();
      List<string> idKPI = KPIParser.GetIDs(formula);
      if (idKPI == null) return;

      //?? formulasP formulasF = formula
      sql = "select * from SC_Period where idSC = " + idSC + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dtSCPeriod = _mySql.SqlSelect(sql);
      string[] formulasP = new string[dtSCPeriod.Rows.Count];
      string[] formulasF = new string[dtSCPeriod.Rows.Count];
      for (int i = 0; i < dtSCPeriod.Rows.Count; i++)
      {
        formulasP[i] = formula;
        formulasF[i] = formula;
      }

      foreach (string id in idKPI)
      {
        if (id == idSC)
        {
          MessageBox.Show("В формуле не должно быть ссылки на сам показатель");
          return; //разобраться !
        }
        sql = "select * from SC where id =  " + id + " and targetyear = " + UserInfo.TargetYear.ToString();
        DataRow dr1 = _mySql.SqlSelect(sql).Rows[0];
        if (dr1["formula"].ToString() != "") EvaluateFormula(id);

        sql = "select * from SC_period where idSC = " + id + " and targetyear = " + UserInfo.TargetYear.ToString() + " order by id";
        DataTable dtSCPeriod1 = _mySql.SqlSelect(sql);
        try
        {
          for (int i = 0; i < dtSCPeriod.Rows.Count; i++)
          {
            string str = "[" + id + "]";
            formulasP[i] = formulasP[i].Replace(str, dtSCPeriod1.Rows[i]["plan"].ToString());
            formulasF[i] = formulasF[i].Replace(str, dtSCPeriod1.Rows[i]["fact"].ToString());
          }
        }
        catch { }
      }

      MathParser.Parser parser = new MathParser.Parser();
      for (int i = 0; i < dtSCPeriod.Rows.Count; i++)
      {
        parser.Evaluate(formulasP[i]);
        dtSCPeriod.Rows[i]["plan"] = Convert.ToDouble(parser.Result.ToString("f" + Convert.ToInt16(nUpDown_precision.Value).ToString()));

        parser.Evaluate(formulasF[i]);
        dtSCPeriod.Rows[i]["fact"] = Convert.ToDouble(parser.Result.ToString("f" + Convert.ToInt16(nUpDown_precision.Value).ToString()));

        DataRow dr = dtSCPeriod.Rows[i];
        double plan = Convert.ToDouble(dr["plan"]);
        double fact = Convert.ToDouble(dr["fact"]);
        if ((double.IsInfinity(fact)) | (double.IsNaN(fact))) dr["fact"] = 0;
        if ((double.IsInfinity(plan)) | (double.IsNaN(plan))) dr["plan"] = 0;
        sql = "update SC_Period set plan = " + Functions.ToString(dr["plan"]) + ", fact = " + Functions.ToString(dr["fact"]) + " where id = " + dr["id"].ToString();
        StringBuilder str = new StringBuilder(sql);
        str.Replace("NaN", "0");
        _mySql.SqlExec(str.ToString());
      }

    }
    void SetFormula()
    {
      if (tb_Formula.Text != "")
      {
        //        b_selectResponsiblePerson.Visibility = Visibility.Hidden;
        cb_period_input_data.IsEnabled = false;
        UpdateFactPeriodIsFormula(true);

        EvaluateFormula(_idSC);
        RenewPeriod();


        string formula = "";
        List<string> idKPI = KPIParser.FromNamesToId(_mySql, tb_Formula.Text, ref formula);
        if (idKPI == null) return;

        //string sql = "select * from SC where formula LIKE '%" + _idSC + "%'";
        string sql;
        foreach (SCPeriod dr in SCPeriods)
        {
          foreach (string id in idKPI)
          {
            string err = ValidatePeriodsForFormula(id);
            if (err != "")
            {
              MessageBox.Show(err);
              return;
            }
            ////            sql = "select * from SC_period where DateEnd <= Format('" + dr["DateEnd"].ToString() + "', 'dd/mm/yyyy')";
            //sql = "select * from SC_period where DateEnd <= " + _mySql.to_date((DateTime)dr["DateEnd"]) + " and targetyear = " + UserInfo.TargetYear.ToString();
            //DataTable dt = _mySql.SqlSelect(sql);
            //DataRow dr1 = dt.Rows[0];
          }
        }
      }
      else
      {
        //       b_selectResponsiblePerson.Visibility = Visibility.Visible;
        //       if (_mySql.ToInt32(_dr_SC["Level1"]) == TypeHeader.Goal) b_selectResponsiblePerson.IsEnabled = false;

        UpdateFactPeriodIsFormula(false);
        cb_period_input_data.IsEnabled = true;
      }
    }
    private void tb_Formula_TextChanged(object sender, TextChangedEventArgs e)
    {
      if (!this.IsLoaded) return;
      if (SCPeriods == null) return;
      //			SetFormula();
    }
    #endregion Formula

    private void UserControl_Loaded(object sender, RoutedEventArgs e)
    {
    }
    private void dt_targetEndDate_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      if (!isKPISetting) return;
      //cb_period.SelectedItem = Period.No;
    }
    bool CanApproveBranch(string id_root, ref string message)
    {
      bool res = true;
      string sql = "select * from SCTree where id_parent=" + id_root + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt = _mySql.SqlSelect(sql);

      message = "Утверждение запрещается: /r/n";
      foreach (DataRow dr in dt.Rows)
      {
        sql = "select * from SC where id=" + dr["id_SC"].ToString() + " and targetyear = " + UserInfo.TargetYear.ToString();
        DataTable dt_SC = _mySql.SqlSelect(sql);

        if (dt_SC.Rows.Count > 0)
        {
          DataRow dr_SC = dt_SC.Rows[0];//одна строка
          if (!_mySql.ToBool(dr_SC["IsApproved"]))
          {
            message = "Показатель \"" + dr_SC["name1"].ToString() + "\" не утверждён";
            return false;
          }
          if (!CanApproveBranch(dr["id_SC"].ToString(), ref message))
          {
            message = "Показатель \"" + dr_SC["name1"].ToString() + "\" не утверждён";
            return false;
          }
        }
      }
      return res;
    }
    private void menu_KPIApprove_Click(object sender, RoutedEventArgs e)
    {
      string mess = "";
      if (!CanApproveBranch(_dr_SC["id"].ToString(), ref mess))
      {
        MessageBox.Show(mess, "Утверждение", MessageBoxButton.OK);
        return;
      }
      string err = Validate();
      if (err != "")
      {
        MessageBox.Show(err);
        return;
      }

      OnSCEvent(new SCEventArgs() { TypeEvent = IsApprovedConst, obj = true, idSC = _dr_SC["id"].ToString() });

      _dr_SC["isApproved"] = true;
      Save();

      //формирование сообщений
      TypeMessages.Insert(_dr_parentSC["IDControlPerson"].ToString(), UserInfo.Info.id_form, _idSC, TypeMessages.FromSC, _idSC, "-1", "Закончена работа с показателем " +
        _dr_SC["name1"].ToString(), _mySql);
      if (_dr_SC["IDresponsiblePerson"].ToString() != "-1") TypeMessages.Insert(_dr_SC["IDresponsiblePerson"].ToString(), UserInfo.Info.id_form, _idSC, TypeMessages.FromSC,
        _idSC, "-1", "Вы назначены ответственным за ввод фактических значений показателя: " + _dr_SC["name1"].ToString(), _mySql);
      TypeHistory.Insert(_mySql, _dr_SC["id"].ToString(), "Показатель утверждён", _dr_SC["IDControlPerson"].ToString(), TypeHistory.SC_Approved);

      RenewHistory();
      Activities.Set(_dr_SC, _mySql, false);

      //menu_KPIApprove.Visibility = System.Windows.Visibility.Collapsed;
      //menu_KPISave.Visibility = System.Windows.Visibility.Collapsed;
      //menu_KPIUnBlock.Visibility = System.Windows.Visibility.Visible;
    }
    private void menu_KPIUnBlock_Click(object sender, RoutedEventArgs e)
    {
      dlgDeBlock dlg = new dlgDeBlock(TypeHistory.SC_DeBlock);
      if ((bool)dlg.ShowDialog())
      {
        RenewHistory();
        _dr_SC["isApproved"] = false;
        Save();

        TypeHistory.Insert(_mySql, _dr_SC["id"].ToString(), dlg.strReason, _dr_SC["IDControlPerson"].ToString(), TypeHistory.SC_DeBlock);
        TypeMessages.Insert(_dr_parentSC["IDControlPerson"].ToString(), UserInfo.Info.id_form, _idSC, TypeMessages.FromSC, _idSC, "-1", "Разблокирован показатель", _mySql);

        Activities.Set(_dr_SC, _mySql, false);

        //menu_KPIApprove.Visibility = System.Windows.Visibility.Visible;
        //menu_KPISave.Visibility = System.Windows.Visibility.Visible;
        //menu_KPIUnBlock.Visibility = System.Windows.Visibility.Collapsed;

        OnSCEvent(new SCEventArgs() { TypeEvent = IsApprovedConst, obj = false });
      }
    }

    #region Events
    public class SCEventArgs : EventArgs
    {
      public int TypeEvent;
      public string idSC;
      public object obj;
    }
    public delegate void SCEventHandler(Object sender, SCEventArgs args);
    public event SCEventHandler SCHandler;
    protected virtual void OnSCEvent(SCEventArgs args)
    {
      if (SCHandler != null)
      {
        SCHandler(this, args);
      }
    }
    #endregion Events

  
    private void b_DeleteAction_Click(object sender, RoutedEventArgs e)
    {
      //DataRow dr = ((DataRowView)dg_KPI_delegation.SelectedItem).Row;
      DelegateUser dr = (DelegateUser)dg_KPI_delegation.SelectedItem;
      if (MessageBox.Show("Убрать делегирование для \r\n" + dr.FIO + "? \r\n Созданные нижележащие показатели также будут удалены", "Удаление делегирования", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {

        string sql = "delete from staff_KPI where id = " + dr.ID;
        _mySql.SqlExec(sql);
       _delegUser.RemoveAt(dg_KPI_delegation.SelectedIndex);
      }

    }

    private void nUpDown_targetValue_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      if (!IsLoaded) return;
      if (e.NewValue == null) return;
   //   if (dt_SC_Period.Rows.Count > 0) dt_SC_Period.Rows[dt_SC_Period.Rows.Count - 1]["plan"] = e.NewValue;
    }

    private void Activities_Loaded(object sender, RoutedEventArgs e)
    {
        
    }

  

  }

  public class DelegateUser
  {
    public string ID { get; set; }
    public string ID_FORM { get; set; }

    public string FIO { get; set; }
    public string DOLGNOST { get; set; }
    public double TARGETVALUE { get; set; }
    public bool EnabledOnlyForControlPerson { get; set; }

    public string id_dep { get; set; }
    public string id_position { get; set; }
    public string id_KPI { get; set; }
    public double weight { get; set; }

  }
}
