﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для FactValues.xaml
  /// </summary>
  public partial class FactValues : UserControl
  {
    DataTable _dt_Responsible_KPI;
    CMySql _mySql;

    double SC_fact;
    public FactValues()
    {
      InitializeComponent();
    }

    public void Set(CMySql mySql)
    {
      _mySql = mySql;
    }
    public static int CountFactValues(CMySql mySql)
    {
      string sql = "SELECT SC.name1, SC.isApproved, SC.id AS idSC, SC.fact AS SC_fact, SC_Period.id, SC_Period.DateBegin, SC_Period.DateEnd, SC_Period.plan, SC_Period.fact, SC_Period.isInserted, SC_Period.DateInserted " +
          "FROM SC_Period INNER JOIN SC ON SC_Period.idSC = SC.id where SC.IDResponsiblePerson = " + UserInfo.Info.id_form + " and SC_Period.DateEnd < " + mySql.DateToString(UserInfo.DateWarn) + " and SC.targetyear = " + UserInfo.TargetYear.ToString();
      sql += " and isInserted = 0"; 
     DataTable dt = mySql.SqlSelect(sql);
     return dt.Rows.Count;
    }
    void UpdateStaffResponsible()
    {
      if (!IsLoaded) return;
      string sql = "SELECT SC.name1, SC.isApproved, SC.id AS idSC, SC.fact AS SC_fact, SC_Period.id, SC_Period.DateBegin, SC_Period.DateEnd, SC_Period.plan, SC_Period.fact, SC_Period.isInserted, SC_Period.DateInserted " +
            "FROM SC_Period INNER JOIN SC ON SC_Period.idSC = SC.id where SC.IDResponsiblePerson = " + UserInfo.Info.id_form + " and SC_Period.DateEnd < " + _mySql.DateToString(UserInfo.DateWarn) + " and SC.targetyear = " + UserInfo.TargetYear.ToString();
      if ((bool)rb_current.IsChecked) { sql += " and isInserted = 0"; }
      if ((bool)rb_approved.IsChecked) { sql += " and isInserted = 1"; }
      if ((bool)rb_all.IsChecked) 
        sql = "SELECT SC.name1, SC.isApproved, SC.id AS idSC, SC.fact AS SC_fact, SC_Period.id, SC_Period.DateBegin, SC_Period.DateEnd, SC_Period.plan, SC_Period.fact, SC_Period.isInserted, SC_Period.DateInserted " +
            "FROM SC_Period INNER JOIN SC ON SC_Period.idSC = SC.id where SC.IDResponsiblePerson = " + UserInfo.Info.id_form + " and SC.targetyear = " + UserInfo.TargetYear.ToString();
      _dt_Responsible_KPI = _mySql.SqlSelect(sql);

      dg_responsible_kpi.ItemsSource = _dt_Responsible_KPI.DefaultView;

    }
    private void b_insertFact_Click(object sender, RoutedEventArgs e)
    {

      DataRowView dr = (DataRowView)dg_responsible_kpi.SelectedItem;
      dr.Row["isInserted"] = true;
      dr.Row["DateInserted"] = DateTime.Now.ToShortDateString();
      dg_responsible_kpi.CommitEdit();


      string sql = "update SC_Period set fact=" + Functions.ToString(dr["fact"]) + ", isInserted=" + dr["isInserted"].ToString() +
        ", DateInserted=" + _mySql.DateToString((DateTime)dr["DateInserted"]) + " where id=" + dr["id"].ToString();
      _mySql.SqlExec(sql);

     SC_fact = Functions.ToDouble(dr["SC_fact"].ToString());
     if (SC_fact < Functions.ToDouble(dr["fact"].ToString())) 
      {
        sql = "update SC set fact=" + Functions.ToString(dr["fact"]) +  ", plan=" + Functions.ToString(dr["plan"]) + " where id=" + dr["idSC"].ToString();
        _mySql.SqlExec(sql);
        SC_fact = Functions.ToDouble(dr["fact"].ToString());
      }
      dg_responsible_kpi.Items.Refresh();
    }
    private void rb_current_Checked(object sender, RoutedEventArgs e)
    {
      UpdateStaffResponsible();
    }
    private void rb_approved_Checked(object sender, RoutedEventArgs e)
    {
      UpdateStaffResponsible();
    }
    private void rb_all_Checked(object sender, RoutedEventArgs e)
    {
      UpdateStaffResponsible();
    }

    private void UserControl_Loaded(object sender, RoutedEventArgs e)
    {
      UpdateStaffResponsible();
    }

    private void b_close_Click(object sender, RoutedEventArgs e)
    {
      
    }

    private void b_CancelFact_Click(object sender, RoutedEventArgs e)
    {
      DataRowView dr = (DataRowView)dg_responsible_kpi.SelectedItem;
      dr.Row["isInserted"] = false;
      dr.Row["DateInserted"] = DateTime.Now.ToShortDateString();
      dg_responsible_kpi.CommitEdit();


      string sql = "update SC_Period set fact=" + Functions.ToString(dr["fact"]) + ", isInserted=0" +
        ", DateInserted=" + _mySql.DateToString((DateTime)dr["DateInserted"]) + " where id=" + dr["id"].ToString();
      _mySql.SqlExec(sql);

      dg_responsible_kpi.Items.Refresh();

    }
  }
}
