﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
using Modules;
using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для ScorecardElement.xaml
  /// </summary>
  public partial class ScorecardElement : UserControl
  {


    public static int IsCheckedEvent = 1;
    public static int IsUnCheckedEvent = 2;
    public static int IsNameEditEvent = 3;

    string _IndName;
    public string IndName
    {
      get { return _IndName; }
    }

    bool _isChecked;
    public bool isChecked
    {
      set { SelectIndicator(value); }
      get { return _isChecked; }
    }

    int _idSC; //id в SC
    public int idSC
    {
      get { return _idSC; }
    }
    int _idSCParent; //рдительский id в SCTree
    public int idSCParent
    {
      get { return _idSCParent; }
    }
    public int idPerspective { get; set; }
    CMySql _mySql;
    DataRow _dr_SC;

    public string IdControlPerson;
    public string IdResponsiblePerson;
    public List<ScorecardElement> childrenSC;

    public ScorecardElement()
    {
      InitializeComponent();
      ScorecardElementdEvent = null;

      childrenSC = new List<ScorecardElement>();
      //r1.Style = (Style)this.Resources["IndicatorStyle"]; 
    }
    void SetName(string IndName)
    {
      _IndName = IndName;
      TB_Name.Text = _IndName;
    }
    public void EditName()
    {
      L_Name.Visibility = System.Windows.Visibility.Hidden;
      TB_Name.Visibility = System.Windows.Visibility.Visible;
      TB_Name.Focus();
    }
    public void SetIndicator(string IndName, int idSC, int idParent, int idPespective, CMySql mySql, ScorecardElementdEventHandler ElementdEvent)
    {
      SetName(IndName);
      _idSC = idSC;
      _idSCParent = idParent;
      idPerspective = idPespective;
      ScorecardElementdEvent += ElementdEvent;

      string sql = "select * from SC where id = " + idSC + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dtSC = mySql.SqlSelect(sql);

      {
        //расчёт индикации
        sql = "select MAX(SC_Period.plan) as PLAN, MAX(SC_Period.fact) as FACT from SC_Period where idSC = " + idSC +
          " and DateEnd < " + mySql.DateToString(UserInfo.DateWarn) + " and targetyear = " + UserInfo.TargetYear.ToString();
        DataTable dt_SC_Period = mySql.SqlSelect(sql);
        tb_plan.Text = dt_SC_Period.Rows[0][0].ToString();
        this.DataContext = dt_SC_Period;
      }

      //sql = "select TARGETVALUE from staff_kpi where id_kpi = " + idSC + " and ID_FORM = " + UserInfo.id_form;
      //DataTable dt_staff_kpi = mySql.SqlSelect(sql);
      //if (dt_staff_kpi.Rows.Count == 0)
      //{
      //  tb_plan.Text = dtSC.Rows[0]["TARGETVALUE"].ToString();
      //}
      //else tb_plan.Text = dt_staff_kpi.Rows[0][0].ToString(); 

      DataTable dt_user = SQL.GetUserInfo(mySql, dtSC.Rows[0]["IDCONTROLPERSON"].ToString());
      if (dt_user.Rows.Count > 0) L_ResponsiblePerson.Text = dt_user.Rows[0]["fio"].ToString();

      tb_unit.Text = dtSC.Rows[0]["UNIT"].ToString();
      indProgress.Set();
    }
    public void SetIndicator(DataRow dr_SC, int idPespective, CMySql mySql, ScorecardElementdEventHandler ElementdEvent)
    {
      SetName(dr_SC["name1"].ToString());
      _idSC = Convert.ToInt32(dr_SC["id_SC"]);
      _idSCParent = Convert.ToInt32(dr_SC["id_parent"]);
      idPerspective = idPespective;
      ScorecardElementdEvent += ElementdEvent;
      _mySql = mySql;
      _dr_SC = dr_SC;

      UpdateIndication();
    }
    void UpdateIndication()
    {
      //расчёт индикации 
      //переделять!!
      //string sql = "select MIN(SC_Period.plan) as PLAN, MIN(SC_Period.fact) as FACT from SC_Period where idSC = " + idSC +
      //  " and DateEnd >= " + mySql.to_date(UserInfo.ServerDate) + " and targetyear = " + UserInfo.TargetYear + " order by DateEnd ASC";
      //string sql = "select MAX(SC_Period.plan) as PLAN from SC_Period where idSC = " + idSC +
      //              " and targetyear = " + UserInfo.TargetYear + " order by DateEnd ASC";
      //DataTable dt_SC_Period = _mySql.SqlSelect(sql);
      //tb_plan.Text = dt_SC_Period.Rows[0][0].ToString();


      string sql = "select MAX(SC_Period.plan) as PLAN, MAX(SC_Period.fact) as FACT from SC_Period where idSC = " + idSC +
              " and  DateEnd<= " + _mySql.DateToString(UserInfo.ServerDate) + " and targetyear = " + UserInfo.TargetYear + " order by DateEnd ASC";

      sql = "select MAX(SC_Period.plan) as PLAN, MAX(SC_Period.fact) as FACT from SC_Period where idSC = " + idSC +
                    " and IsInserted = 1 and targetyear = " + UserInfo.TargetYear + " order by DateEnd ASC";

      if (UserInfo.ServerDate > new DateTime(Convert.ToInt32(UserInfo.TargetYear), 12, 31))
        sql = "select MAX(SC_Period.plan) as PLAN, MAX(SC_Period.fact) as FACT from SC_Period where idSC = " + idSC +
       " and IsInserted = 1 order by DateEnd ASC";

      DataTable dt_SC_Period = _mySql.SqlSelect(sql);
      tb_plan_curr.Text = dt_SC_Period.Rows[0][0].ToString();
      this.DataContext = dt_SC_Period;

      //DataTable dt_user = SQL.GetUserInfo(mySql, dr_SC["IDCONTROLPERSON"].ToString());
      //if (dt_user.Rows.Count > 0) 
      L_ResponsiblePerson.Text = _dr_SC["fio"].ToString();
      tb_unit.Text = tb_unit_curr.Text = _dr_SC["UNIT"].ToString();
      tb_plan.Text = _dr_SC["TARGETVALUE"].ToString();
      indProgress.Set();
    }

    public void SelectIndicator(bool isSelected)
    {
      _isChecked = isSelected;
      if (isChecked)
      {
        ScorecardElementEventArgs see = new ScorecardElementEventArgs();
        see.Element = this; see.Event = ScorecardElement.IsCheckedEvent;

        OnScorecardElementdEvent(see);

        border.Style = (Style)this.Resources["SelectedStyle"];

        UpdateIndication();
      }
      else
      {
        ScorecardElementEventArgs see = new ScorecardElementEventArgs();
        see.Element = this; see.Event = ScorecardElement.IsUnCheckedEvent;

        OnScorecardElementdEvent(see);

        border.Style = (Style)this.Resources["UnSelectedStyle"];
      }
    }
    public void AddChildrenSC(ScorecardElement sce)
    {
      childrenSC.Add(sce);
    }
    private void b_editName_Click(object sender, RoutedEventArgs e)
    {
      EditName();
    }

    void OnEditLostFocus()
    {
      L_Name.Visibility = System.Windows.Visibility.Visible;
      TB_Name.Visibility = System.Windows.Visibility.Hidden;
      _IndName = TB_Name.Text;

      ScorecardElementEventArgs see = new ScorecardElementEventArgs();
      see.Element = this; see.Event = ScorecardElement.IsNameEditEvent;
      OnScorecardElementdEvent(see);
    }
    private void TB_Name_LostFocus(object sender, RoutedEventArgs e)
    {
      OnEditLostFocus();
    }
    private void TB_Name_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
    {
      //			OnEditLostFocus();
    }

    private void button_Checked(object sender, RoutedEventArgs e)
    {
      ScorecardElementEventArgs see = new ScorecardElementEventArgs();
      see.Element = this; see.Event = ScorecardElement.IsCheckedEvent;

      OnScorecardElementdEvent(see);

    }
    private void button_Unchecked(object sender, RoutedEventArgs e)
    {
      ScorecardElementEventArgs see = new ScorecardElementEventArgs();
      see.Element = this; see.Event = ScorecardElement.IsUnCheckedEvent;

      OnScorecardElementdEvent(see);
    }

    #region События
    //машрутизируемое событие - не используется
    public static readonly RoutedEvent CheckedChangedEvent = EventManager.RegisterRoutedEvent("CheckedChanged", RoutingStrategy.Bubble,
    typeof(RoutedPropertyChangedEventHandler<bool>), typeof(ScorecardElement));
    public event RoutedPropertyChangedEventHandler<bool> CheckedChanged
    {
      add { AddHandler(CheckedChangedEvent, value); }
      remove { RemoveHandler(CheckedChangedEvent, value); }
    }
    private void OnCheckedChanged(bool oldValue, bool newValue)
    {
      RoutedPropertyChangedEventArgs<bool> args = new RoutedPropertyChangedEventArgs<bool>(oldValue, newValue);
      args.RoutedEvent = ScorecardElement.CheckedChangedEvent;
      RaiseEvent(args);
    }

    //обычное
    public class ScorecardElementEventArgs : EventArgs
    {
      int _event;
      ScorecardElement _se;

      public int Event
      {
        get { return _event; }
        set { _event = value; }
      }
      public ScorecardElement Element
      {
        get { return _se; }
        set { _se = value; }
      }
      public ScorecardElementEventArgs()
      {
        _event = -1;
        _se = null;
      }
    }
    public delegate void ScorecardElementdEventHandler(Object sender, ScorecardElementEventArgs args);
    public event ScorecardElementdEventHandler ScorecardElementdEvent;
    protected virtual void OnScorecardElementdEvent(ScorecardElementEventArgs e)
    {
      if (ScorecardElementdEvent != null)
      {
        ScorecardElementdEvent(this, e);
      }
    }

    #endregion События

    private void TB_Name_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter) OnEditLostFocus();
    }


    private void border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      SelectIndicator(!isChecked);
    }


  }
}
