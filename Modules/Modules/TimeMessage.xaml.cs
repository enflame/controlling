﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Threading;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для TimeMessage.xaml
  /// </summary>
  public partial class TimeMessage : UserControl
  {
    DispatcherTimer dispatcherTimer;
    string _text;
    public string Text
    {
      get { return tB.Text; }
      set
      {
        tB.Text = value;
        _text = value;
      }
    }
    bool _isRepeat;
    bool _isUsualStyle;

    public TimeMessage()
    {
      InitializeComponent();

      dispatcherTimer = new DispatcherTimer();
      dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
      dispatcherTimer.Interval = new TimeSpan(0, 0, 2);

      _isUsualStyle = true;
    }

    public void Show(string mess, bool isRepeat)
    {
      _isRepeat = isRepeat;

      tB.Text = mess;
      ChangeStyle();
      dispatcherTimer.Start();
    }

    void Action()
    {
      while (_isRepeat)
      {
        this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new System.Windows.Threading.DispatcherOperationCallback(delegate
        {

          return null;
        }), null);
      }
    }
    void dispatcherTimer_Tick(object sender, EventArgs e)
    {
      tB.Text = _text;
      dispatcherTimer.Stop();

      if (_isRepeat) Show(_text, _isRepeat);
      else ChangeStyle();

    }

    void ChangeStyle()
    {
      if (_isUsualStyle) tB.Style = (Style)this.Resources["ActionStyle"];
      else tB.Style = (Style)this.Resources["UsualStyle"];
      _isUsualStyle = !_isUsualStyle;
    }
  }
}
