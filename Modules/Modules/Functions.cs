﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Globalization;

using MathParser;
using System.Data;
using System.IO;
using System.Windows.Controls;
using MySql;
using GeneralClasses;

namespace Modules
{

  public class YM
  {
    public int Y;
    public string M;

    public YM(int y, string m)
    {
      Y = y;
      M = m;
    }
  }
  public class Period
  {
    //public static int Year = 0;
    //public static int HalfYear = 1;
    //public static int quarter = 2;
    //public static int Month = 3;

    public static string No = "Не выбран";
    public static string Year = "Год";
    public static string HalfYear = "Полугодие";
    public static string Quarter = "Квартал";
    public static string Month = "Месяц";

    public static string[] H = new string[2] { "Первое полугодие", "Второе полугодие" };
    public static string[] Q = new string[4] { "1 кв.", "2 кв.", "3 кв.", "4 кв." };
    public static string[] M = new string[12] { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };


    public static string GetQuarter(DateTime dt, ref DateTime dt1, ref DateTime dt2)
    {
      if (dt.Month <= 3)
      {
        dt1 = new DateTime(dt.Year, 1, 1);
        dt2 = new DateTime(dt.Year, 3, DateTime.DaysInMonth(dt.Year, 3));
        return Q[0];
      }
      if (dt.Month > 3 & dt.Month <= 6)
      {
        dt1 = new DateTime(dt.Year, 4, 1);
        dt2 = new DateTime(dt.Year, 6, DateTime.DaysInMonth(dt.Year, 6));
        return Q[1];
      }
      if (dt.Month > 6 & dt.Month <= 9)
      {
        dt1 = new DateTime(dt.Year, 7, 1);
        dt2 = new DateTime(dt.Year, 9, DateTime.DaysInMonth(dt.Year, 9));
        return Q[2];
      }
      if (dt.Month >= 10)
      {
        dt1 = new DateTime(dt.Year, 10, 1);
        dt2 = new DateTime(dt.Year, 12, DateTime.DaysInMonth(dt.Year, 12));
        return Q[3];
      }
      return "";
    }

    public static List<YM> GetMonthes(DateTime dt1, DateTime dt2)
    {
      int y1 = dt1.Year;
      int y2 = dt2.Year;
      if (y1 > y2) return null;

      List<YM> res = new List<YM>();

      int m1 = dt1.Month;
      int m2 = dt2.Month;

      if (y1 == y2)
      {
        for (int i = m1 - 1; i < m2; i++) res.Add(new YM(y1, M[i]));
        return res;
      }
      for (int i = m1 - 1; i < 12; i++) res.Add(new YM(y1, M[i]));
      while (y1 + 1 <= y2 - 1)
      {
        for (int i = 0; i < 12; i++) res.Add(new YM(y1, M[i]));
        y1++;
      }
      for (int i = 0; i < m2; i++) res.Add(new YM(y2, M[i]));



      return res;
    }
    public static string GetReportPeriod(DateTime dt, string typePeriod)
    {
      string res = "";


      if (typePeriod == Period.HalfYear)
      {
        res = "первое полугодие";
      }

      if (typePeriod == Period.Quarter)
      {
        DateTime dt_p = new DateTime(dt.Year, 03, 01);
        if (dt <= dt_p) return Period.Q[0];

        dt_p = new DateTime(dt.Year, 06, 01);
        if (dt <= dt_p) return Period.Q[1];

        dt_p = new DateTime(dt.Year, 09, 01);
        if (dt <= dt_p) return Period.Q[2];

        dt_p = new DateTime(dt.Year, 12, 01);
        if (dt <= dt_p) return Period.Q[3];
      }

      if (typePeriod == Period.Month)
      {
      }

      return res;
    }
  }
  public class Functions
  {
    static CultureInfo culture;
    static Functions()
    {
      culture = new CultureInfo("en-Us");
    }


    public static ImageSource ToImageSource(string source)
    {

      BitmapImage bi3 = new BitmapImage();
      bi3.BeginInit();
      bi3.UriSource = new Uri(source, UriKind.Relative);
      bi3.EndInit();
      return bi3;
    }
    public static double ToDouble(string s)
    {
      return Convert.ToDouble(s.Replace(",", "."), culture);
    }
    public static string ToString(string s)
    {
      return s.Replace(",", ".");
    }
    public static string ToString(object d)
    {
      try
      {
        return (d.ToString()).Replace(",", ".");
      }
      catch { return ""; }
    }
    public static int ToInt32(object v)
    {
      return Convert.ToInt32(v);
    }

    public static void DeleteAllFiles(string dir)
    {
      string[] files = Directory.GetFiles(dir);
      foreach (string file in files)
        DeleteFile(file);
    }
    public static bool DeleteFile(string full_f_name)
    {
      if (File.Exists(full_f_name))
        try
        {
          File.Delete(full_f_name);
        }
        catch (Exception exc)
        {
          System.Windows.MessageBox.Show(exc.Message);
          return false;
        }
      return true;
    }

    public static ComboBoxItem FindComboBoxItemByString(ComboBox cb, string content)
    {
      foreach (ComboBoxItem item in cb.Items)
        if (item.Content.ToString() == content) return item;
      return null;
    }

  }
  public class TaskAttrib
  {
    public string id_form;
    public string id_dep;
    public string id_form_setup;
    public string id_form_approved;
    public string id_form_checked;
    public string id_form_delegated;
    public string id_smart_task;

    public TaskAttrib()
    {
      id_form = "-1";
      id_dep = "-1";
      id_form_setup = "-1";
      id_form_approved = "-1";
      id_form_checked = "-1";
      id_form_delegated = "-1";
      id_smart_task = "-1";
    }
  }

  public class KPIParser : Parser
  {
    public static List<string> GetIDs(string formula)
    {
      List<string> res = new List<string>();

      int ind = 0;
      int ind1 = formula.IndexOf("[", ind);
      int ind2 = formula.IndexOf("]", ind1 + 1);
      try
      {
        while (ind1 != -1)
        {
          string id = formula.Substring(ind1 + 1, ind2 - ind1 - 1);

          ind = ind2;
          ind1 = formula.IndexOf("[", ind);
          if (ind1 != -1) ind2 = formula.IndexOf("]", ind1 + 1);

          res.Add(id);
        }
      }
      catch
      {
        res = null;
      }

      return res;
    }
    public static List<string> FromNamesToId(CMySql mySql, string formula, ref string outFormula)
    {
      List<string> res = new List<string>();
      outFormula = "";

      int ind = 0;
      int ind1 = formula.IndexOf("[", ind);
      int ind2 = formula.IndexOf("]", ind1 + 1);
      try
      {
        while (ind1 != -1)
        {
          outFormula += formula.Substring(ind, ind1 - ind + 1);
          string name = formula.Substring(ind1 + 1, ind2 - ind1 - 1);
          string sql = "select id from SC where name1 = '" + name + "'" + " and targetyear = " + UserInfo.TargetYear.ToString();

          DataTable dt = mySql.SqlSelect(sql);
          string id = dt.Rows[0]["id"].ToString();
          outFormula += id;

          ind = ind2;
          ind1 = formula.IndexOf("[", ind);
          if (ind1 != -1) ind2 = formula.IndexOf("]", ind1 + 1);

          res.Add(id);
        }
        outFormula += formula.Substring(ind2, formula.Length - ind2);
      }
      catch
      {
        res = null;
        outFormula = "Ошибка вблизи \"" + outFormula + "\"";
      }

      return res;
    }

    public static int FromIDToNames(CMySql mySql, string formula, ref string outFormula)
    {
      int res = 1;
      outFormula = "";

      int ind = 0;
      int ind1 = formula.IndexOf("[", ind);
      int ind2 = formula.IndexOf("]", ind1 + 1);
      try
      {
        while (ind1 != -1)
        {
          outFormula += formula.Substring(ind, ind1 - ind + 1);
          string sql = "select name1 from SC where id = " + formula.Substring(ind1 + 1, ind2 - ind1 - 1) + " and targetyear = " + UserInfo.TargetYear.ToString();

          DataTable dt = mySql.SqlSelect(sql);
          outFormula += dt.Rows[0]["name1"].ToString();

          ind = ind2;
          ind1 = formula.IndexOf("[", ind);
          if (ind1 != -1) ind2 = formula.IndexOf("]", ind1 + 1);
        }
        outFormula += formula.Substring(ind2, formula.Length - ind2);
      }
      catch
      {
        res = -1;
        outFormula = "Ошибка вблизи \"" + outFormula + "\"";
      }

      return res;
    }
  }
  public class KPI_ID
  {
    public string Name;
    public string Id;

    public KPI_ID(string name, string id)
    {
      Name = name;
      Id = id;
    }
  }

  public class SQL
  {
    public static DataTable GetUserInfo(CMySql mysql, string lichost_id)
    {
      string sql = "select P.PARENT_ID AS id_parent_dep, UF.PODRAZDELENIE_ID AS id_dep, UF.DOLGNOST_ID AS id_position, P.NAZVANIE AS dep_name, UF.DOLGNOST, " +
               "U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO " +
               "from SOTRUDNIK.LICHNOST_FOR_PHONE_SPR_V3 UF " +
               "inner join tpu.podrazdelenie_tpu_v P on P.ID =UF.PODRAZDELENIE_ID " +
               "inner join app_portal.v_tpu_users U on U.LICHNOST_ID=UF.LICHNOST_ID " +
               "where U.LICHNOST_ID = " + lichost_id;
      return mysql.SqlSelect(sql);
    }
  }
}
