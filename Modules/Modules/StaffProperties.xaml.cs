﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.Data;
using KIP;
using MySql;
using GeneralClasses;
using System.IO;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для StaffProperties.xaml
  /// </summary>
  public partial class StaffProperties : UserControl
  {

    StaffInfo _staffInfo;

    CMySql _mySql;

    CStaffKIP _staffKIP;
    DataTable _dt_KPI;
    public StaffProperties()
    {
      InitializeComponent();
    }

    public void Set(Modules.StaffTree.SelectedItemEventArgs args, CMySql mySql)
    {
      _mySql = mySql;
      _staffInfo = args.staffInfo;

      tb_fio.Text = _staffInfo.fio;
      tb_position.Text = _staffInfo.dep + "; " + _staffInfo.position;
      //string sql = "select * from smart_task where id_KPI=" + idSC;
      //_dt_smart = _mySql.SqlSelect(sql);
      _staffKIP = new CStaffKIP(_staffInfo, mySql);

      UpdateStaffKpi();
      UpdateStaffKIP();

    }
    void UpdateStaffKIP()
    {
      if (rb_AllKIPS.IsChecked == true) dg_smart_task.ItemsSource = _staffKIP.GetKIPs(CStaffKIP.IsAll);
      if (rb_CurrKIPS.IsChecked == true) dg_smart_task.ItemsSource = _staffKIP.GetKIPs(CStaffKIP.IsCurrent);
      if (rb_FinishedKIPS.IsChecked == true) dg_smart_task.ItemsSource = _staffKIP.GetKIPs(CStaffKIP.IsFinished);
    }
    void UpdateStaffKpi()
    {
      string sql = "SELECT SC.name1 AS kpiName, SC.IDcontrolPerson, SC.plan, SC.fact, SC.targetBeginDate, SC.targetEndDate, " +
                  "staff_KPI.id, staff_KPI.weight, staff_KPI.id_dep, staff_KPI.id_position, staff_KPI.id_form, staff_KPI.id_KPI, staff_KPI.criterion, staff_KPI.targetValue " +
                  "FROM SC " +
                  "inner join staff_KPI on  STAFF_KPI.ID_KPI=SC.ID " +
                  "WHERE staff_KPI.id_form = " + _staffInfo.id_form + " and staff_KPI.id_dep =" + _staffInfo.id_dep +// " and SC.IDcontrolPerson=" + UserInfo.Info.id_form +
                  " and SC.targetyear = " + UserInfo.TargetYear.ToString();

      _dt_KPI = _mySql.SqlSelect(sql);


      double weight = 0;
      foreach (DataRow r in _dt_KPI.Rows)
      {

        weight += Convert.ToInt32(r["weight"].ToString());

        sql = "select MAX(SC_Period.plan), MAX(SC_Period.fact) from SC_Period where idSC = " + r["id_KPI"].ToString() + " and DateEnd <" + _mySql.DateToString(DateTime.Now) + " and targetyear = " + UserInfo.TargetYear.ToString();
        DataTable dt_SC_Period = _mySql.SqlSelect(sql);
        if (dt_SC_Period.Rows.Count > 0)
        {//plan и fact добавить в виде колонок?
          r["plan"] = dt_SC_Period.Rows[0][0];
          r["fact"] = dt_SC_Period.Rows[0][1];
        }
      }

      DataRow dr = _dt_KPI.NewRow();
      dr["id_dep"] = -1;
      dr["id_form"] = -1;
      dr["id_position"] = -1;
      dr["id_KPI"] = -1;
      dr["kpiName"] = "Сверхзадачи %";
      dr["weight"] = -1;
      _dt_KPI.Rows.Add(dr);

      dr = _dt_KPI.NewRow();
      dr["id_dep"] = -1;
      dr["id_form"] = -1;
      dr["id_position"] = -1;
      dr["id_KPI"] = -1;
      dr["kpiName"] = "Сумма";
      dr["weight"] = weight;

      _dt_KPI.Rows.Add(dr);


      DataColumn dc = new DataColumn("NumberRow");
      _dt_KPI.Columns.Add(dc);
      for (int i = 0; i < _dt_KPI.Rows.Count - 1; i++)
      {//номер строки
        _dt_KPI.Rows[i]["NumberRow"] = i + 1;
      }

      dg_KPI.ItemsSource = _dt_KPI.DefaultView;

    }
    public void ShowControl(bool isShow)
    {
      if (!isShow) exp_control.Visibility = System.Windows.Visibility.Collapsed;
      else exp_control.Visibility = System.Windows.Visibility.Visible;
    }

    void RemoveKPI()
    {
      DataRow dr = ((DataRowView)dg_KPI.SelectedItem).Row;

      dlgDeBlock dlg = new dlgDeBlock(TypeHistory.SC_DeDelegation);
      if ((bool)dlg.ShowDialog())
      {

        //Удаление KPI, создание соответстующей документации
        //DelegationTree.DelegateKPIStaffToHistory(_mySql, drv.Row, "Делегирование снято");

        string sql = "delete from staff_kpi where id=" + dr["id"];
        _mySql.SqlExec(sql);

        UpdateStaffKpi();

        TypeHistory.Insert(_mySql, dr["id_KPI"].ToString(), dlg.strReason, dr["IDControlPerson"].ToString(), TypeHistory.SC_DeDelegation);
        string mess = "Снято делегирование показателя " + dr["kpiName"].ToString();
        TypeMessages.Insert(dr["id_form"].ToString(), UserInfo.Info.id_form, dr["id_KPI"].ToString(), TypeMessages.FromSC, dr["id_KPI"].ToString(), "-1", mess, _mySql);
      }
    }

    private void b_addSmart_Click(object sender, RoutedEventArgs e)
    {
      //CustomControls.dlgSmartTaskEdit dlg = new CustomControls.dlgSmartTaskEdit(_mySql, _staffInfo, "");
      //Nullable<bool> res = dlg.ShowDialog();
      //if ((bool)res)
      //{
      //}
    }

    private void UpDown_Weight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      if (e.OldValue == null) return;

      double weight = 0;
      for (int i = 0; i < _dt_KPI.Rows.Count - 2; i++)
      {
        weight += Convert.ToInt32(_dt_KPI.Rows[i]["weight"]);
      }
      _dt_KPI.Rows[_dt_KPI.Rows.Count - 1]["weight"] = weight;
      {
      }

    }

    //private void b_Approved_Click(object sender, RoutedEventArgs e)
    //{
    //  DataRowView drv = (DataRowView)dg_KPI.SelectedItem;

    //  drv.Row["isChanged"] = false;
    //  drv.Row["isApproved"] = true;
    //  drv.Row["IsReadonly"] = true;
    //  drv.Row["date_approved"] = DateTime.Now.ToShortDateString();

    //  DelegationTree.UpdateDelegateKPItoStaff(_mySql, drv.Row);


    //  dg_KPI.CommitEdit();
    //  dg_KPI.Items.Refresh();


    //}

    private void DatePicker_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      DataRowView drv = (DataRowView)dg_KPI.SelectedItem;
      drv.Row["isChanged"] = true;
      //drv.Row["isApproved"] = false;
    }
    private void indProgress_Loaded(object sender, RoutedEventArgs e)
    {
      IndicatorProgress ind = (IndicatorProgress)sender;
      int id = Convert.ToInt32(ind.Tag);
      if (id == -1) ind.Visibility = System.Windows.Visibility.Collapsed;
    }

    private void b_ApproveMatrix_Click(object sender, RoutedEventArgs e)
    {
      if (MessageBox.Show("После утверждения \r\n изменения в матрице будут запрещены", "Матрица результативности", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel) return;

      for (int i = 0; i < dg_KPI.Items.Count - 2; i++)
      {
        DataRowView drv = (DataRowView)dg_KPI.Items[i];
        DelegationTree.UpdateDelegateKPItoStaff(_mySql, drv.Row);
      }
      string sql = "update form set DATEAPPROVEDMATRIX=" + _mySql.DateToString(DateTime.Now) + ", ISAPPROVEDMATRIX=1 where id=" + _staffInfo.id_form;
      _mySql.SqlExec(sql);
    }
    private void UserControl_Loaded(object sender, RoutedEventArgs e)
    {

    }

    #region KIP
    private void Button_Click(object sender, RoutedEventArgs e)
    {
      KIP.wndNewKIP wnd = new KIP.wndNewKIP(_staffInfo,_mySql);
      wnd.ShowDialog();
    }
    private void b_open_base_doc_Click(object sender, RoutedEventArgs e)
    {
      CKIP kip = (CKIP)dg_smart_task.SelectedItem;
      string sql = "select doc_base from staff_kip where id = " + kip.ID;
      string o = _mySql.SqlSelect(sql).Rows[0][0].ToString();
      if (o != "") CMSWord.OpenFromBlob(UserInfo.TempDir + "\\" + System.IO.Path.GetRandomFileName(), (byte[])_mySql.SqlSelect(sql).Rows[0][0]);
    }
    private void b_open_end_doc_Click(object sender, RoutedEventArgs e)
    {
      CKIP kip = (CKIP)dg_smart_task.SelectedItem;
      string sql = "select doc_end from staff_kip where id = " + kip.ID;
      object o = _mySql.SqlSelect(sql).Rows[0][0];
    }
    private void b_AcceptTask_Click(object sender, RoutedEventArgs e)
    {
      CKIP kip = (CKIP)dg_smart_task.SelectedItem;
      if (MessageBox.Show("Принять  задачу: \r\n" + kip.Content, "Новая задача", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        kip.SetEvent(CKIP.IsAccepted, _mySql);
      }

    }
    private void b_finish_kip_Click(object sender, RoutedEventArgs e)
    {
      CKIP kip = (CKIP)dg_smart_task.SelectedItem;
      if (MessageBox.Show("Завершить задачу: \r\n" + kip.Content, "Завершение", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        kip.SetEvent(CKIP.IsOnAcceptedRes, _mySql);
      }

    }
    private void b_cancel_kip_Click(object sender, RoutedEventArgs e)
    {
      CKIP kip = (CKIP)dg_smart_task.SelectedItem;
      if (MessageBox.Show("Отменить задачу: \r\n" + kip.Content, "Отмена задания", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        kip.SetEvent(CKIP.IsCanceled, _mySql);
      }
    }
    private void rb_AllKIPS_Checked(object sender, RoutedEventArgs e)
    {
      if (IsLoaded) UpdateStaffKIP();
    }
    private void rb_CurrKIPS_Checked(object sender, RoutedEventArgs e)
    {
      if (IsLoaded) UpdateStaffKIP();
    }
    private void rb_FinishedKIPS_Checked(object sender, RoutedEventArgs e)
    {
      if (IsLoaded) UpdateStaffKIP();
    }
    private void b_kip_history_show_Click(object sender, RoutedEventArgs e)
    {
      CKIP kip = (CKIP)dg_smart_task.SelectedItem;
      wndKIPHistory wnd = new wndKIPHistory(_mySql, kip.ID, kip.Content);
      wnd.ShowDialog();
    }
    private void b_change_end_date_kip_Click(object sender, RoutedEventArgs e)
    {

    }
    private void b_AcceptRes_kip_Click(object sender, RoutedEventArgs e)
    {
      CKIP kip = (CKIP)dg_smart_task.SelectedItem;
      if (MessageBox.Show("Принять результаты: \r\n" + kip.Content, "Завершение", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        kip.SetEvent(CKIP.IsAcceptedRes, _mySql);
      }

    }
    private void b_NotAcceptRes_kip_Click(object sender, RoutedEventArgs e)
    {
      CKIP kip = (CKIP)dg_smart_task.SelectedItem;
      if (MessageBox.Show("Не принимать результаты: \r\n" + kip.Content, "Завершение", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        kip.SetEvent(CKIP.IsNotAcceptedRes, _mySql);
      }

    }
    #endregion KIP

 
  }

  public class VisibilityConverterInt : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      //      if(typeof(value) == Type.GetType(bool))
      bool visibility = true;
      if ((int)value == 0) visibility = false;
      return visibility ? Visibility.Visible : Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      Visibility visibility = (Visibility)value;
      return (visibility == Visibility.Visible);
    }
  }
  public class EnabledConverterInt : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      //      if(typeof(value) == Type.GetType(bool))
      bool enabled = true;
      if (value == (object)0) enabled = false;
      return enabled;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      bool enabled = (bool)value;
      return enabled ? 1 : 0;
    }
  }

  public class EnabledConverterIntNot : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      //      if(typeof(value) == Type.GetType(bool))
      bool enabled = false;
      if (Functions.ToInt32(value) == 0) enabled = true;
      return enabled;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      bool enabled = (bool)value;
      return enabled ? 0 : 1;
    }
  }

  public class VisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {

      //bool visibility = (bool)value;
      bool visibility = true;
      try
      {
        visibility = (bool)value;
      }
      catch
      {
        if ((decimal)value == 0) visibility = false;
      }
      return visibility ? Visibility.Visible : Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      Visibility visibility = (Visibility)value;
      return (visibility == Visibility.Visible);
    }
  }
  public class VisibilityConverterNot : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      bool visibility = true;
      try
      {
        visibility = (bool)value;
      }
      catch
      {
        if ((decimal)value == 0) visibility = false;
      }
      visibility = !visibility;
      return visibility ? Visibility.Visible : Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      Visibility visibility = (Visibility)value;
      return (visibility == Visibility.Visible);
    }
  }
  public class IsReadOnlyNot : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      bool visibility = true;
      try
      {
        visibility = (bool)value;
      }
      catch
      {
        if ((decimal)value == 0) visibility = false;
      }
      return !visibility;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      bool visibility = (bool)value;
      return !visibility;
    }
  }
  public class RowNumberConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value != null && value.GetType() == typeof(DataRowView))
        return "№" + ((DataRowView)value).Row.Table.Rows.IndexOf(((DataRowView)value).Row);
      return "ха";
    }
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }


  [ValueConversion(typeof(ItemsPresenter), typeof(Orientation))]
  public class ItemsPanelOrientationConverter : IValueConverter
  {
    // Returns 'Horizontal' for root TreeViewItems 
    // and 'Vertical' for all other items.
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      // The 'value' argument should reference 
      // an ItemsPresenter.
      ItemsPresenter itemsPresenter = value as ItemsPresenter;
      if (itemsPresenter == null)
        return Binding.DoNothing;

      // The ItemsPresenter's templated parent 
      // should be a TreeViewItem.
      TreeViewItem item = itemsPresenter.TemplatedParent as TreeViewItem;
      if (item == null)
        return Binding.DoNothing;

      // If the item is contained in a TreeView then it is
      // a root item.  Otherwise it is contained in another
      // TreeViewItem, in which case it is not a root.
      bool isRoot =
       ItemsControl.ItemsControlFromItemContainer(item) is TreeView;

      // The children of root items are layed out
      // in a horizontal row.  The grandchild items 
      // (i.e. cities) are layed out vertically.
      return
       isRoot ?
       Orientation.Horizontal :
       Orientation.Vertical;
    }

    public object ConvertBack(
     object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotSupportedException("Cannot convert back.");
    }
  }
}
