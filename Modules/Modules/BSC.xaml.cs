﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
using MySql;
using CustomControls;
namespace Modules
{
	/// <summary>
	/// Логика взаимодействия для UserControl1.xaml
	/// </summary>
	public partial class BSCtree : UserControl
	{
		CMySql _mySql;
		List<string> _id_SC_list;
		public List<string> id_SC_list
		{
			get
			{
				return _id_SC_list;
			}
		}

		public BSCtree()
		{
			InitializeComponent();
			_id_SC_list = new List<string>();
		}
		public void Set(CMySql mySql)
		{
			_mySql = mySql;
			FillScorecardTree();
		}

		void FillScorecardBranch(TreeViewItem root, string id_root)
		{
			string sql = "select * from SCTree where id_parent=" + id_root;
			DataTable dt = _mySql.SqlSelect(sql).Tables[0];

			foreach (DataRow dr in dt.Rows)
			{
				sql = "select * from SC where id=" + dr["id_SC"].ToString();
				DataTable dt_SC = _mySql.SqlSelect(sql).Tables[0];
				DataRow dr_SC = dt_SC.Rows[0];//одна строка

				TreeViewItem node = new TreeViewItem();
				TreeViewItemHeader h = null;
				if (((int)dr_SC["Level1"]) == 20)
					h = new TreeViewItemHeader("/Controlling;component/Ico/target_32.png", dr_SC["name1"].ToString());
				if (((int)dr_SC["Level1"]) == 40)
					h = new TreeViewItemHeader("/Controlling;component/Ico/target_22.png", dr_SC["name1"].ToString());
				node.Header = h; node.Name = "n" + dr["id_SC"].ToString();
				node.Tag = root;
				root.Items.Add(node);


				FillScorecardBranch(node, dr["id_SC"].ToString());
			}
		}
		public void FillScorecardTree()
		{
			tView.Items.Clear();

			string sql = "select * from SCTree where id_parent=0";
			DataTable dt = _mySql.SqlSelect(sql).Tables[0];
			foreach (DataRow dr in dt.Rows)
			{
				sql = "select * from SC where id=" + dr["id_SC"].ToString();
				DataTable dt_SC = _mySql.SqlSelect(sql).Tables[0];
				DataRow dr_SC = dt_SC.Rows[0];//одна строка

				TreeViewItem root = new TreeViewItem(); root.Tag = null;
				TreeViewItemHeader h = new TreeViewItemHeader("/Controlling;component/Ico/stock_mail_flag_for_followup_32.png", dr_SC["name1"].ToString());
				root.Header = h; root.Name = "n" + dr["id_SC"].ToString();
				tView.Items.Add(root);

				FillScorecardBranch(root, dr["id_SC"].ToString());

			}
		}

		private void tView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			//реакция на выделение показателя
			TreeViewItem item = (TreeViewItem)e.NewValue;
			TreeViewItemHeader h = (TreeViewItemHeader)((item).Header);
			_id_SC_list.Clear();
			_id_SC_list.Add(item.Name.Substring(1));
			while(item.Tag != null)
			{
				item = (TreeViewItem)item.Tag;
				_id_SC_list.Add(item.Name.Substring(1));
			}

			OnSelectedItem();
		}

		#region Events
		public class SelectedItemEventArgs : EventArgs
		{
			public string id_SC;
		}
		public delegate void SelectedItemEventHandler(Object sender);
		public event SelectedItemEventHandler SelectedItem;
		protected virtual void OnSelectedItem()
		{
			if (SelectedItem != null)
			{
				SelectedItem(this);
			}
		}
		#endregion Events

	}
}
