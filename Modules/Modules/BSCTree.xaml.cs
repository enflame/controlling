﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
using System.IO;
using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для UserControl1.xaml
  /// полагаем level1 = 0 - Перспектива
  /// полагаем level1 = 10 - Цель
  /// полагаем level1 = 20 - Показатель
  /// </summary>
  public partial class BSCtree : UserControl
  {
    public string Caption
    {
      set
      {
        if (value == "") TB_tViewName.Visibility = System.Windows.Visibility.Collapsed;
        else
        {
          TB_tViewName.Text = value;
          TB_tViewName.Visibility = System.Windows.Visibility.Visible;
        }
      }
    }

    Point _lastMouseDown;
    string draggedItem;

    CMySql _mySql;
    TreeViewItem _curr_item;

    bool _isEdit;

    bool isOnlyDelegation;
    bool isOnlyOwn;
    IndWrapPanel _mainIWP;

    public ScorecardElement SelectedBSC
    {
      get
      {
        if (_curr_item == null) return null;
        NodeTag nt = (NodeTag)_curr_item.Tag;
        return nt.sce;
      }
    }
    public BSCtree()
    {
      InitializeComponent();
    }
    public void Set(CMySql mySql, IndWrapPanel mainIWP)
    {
      _mySql = mySql;
      _mainIWP = mainIWP;
      FillScorecardTree();
    }
    public void ChangePerspective(string idSCPersp)
    {

      foreach (object o in tView.Items)
      {
        TreeViewItem item = (TreeViewItem)o;
        if (item.Name == "n" + idSCPersp)
        {
          _mainIWP.RemoveAllIndicators();
          NodeTag nt = (NodeTag)item.Tag;
          foreach (ScorecardElement sce in nt.sce.childrenSC)
            _mainIWP.AddIndicator(sce);

          return;
        }
      }
    }
    public static DataTable SelectFromTreeAndSC(CMySql mySql, string id_root, int id_resp_person)
    {
      string sql = "select SCTree.id_SC, SC.Name1, SC.Level1, SC.IDControlperson, SC.targetvalue, sc.unit from SCTree " +
                   "left join SC on SCTree.id_SC = SC.ID " +
                   "where SCTree.id_parent=" + id_root + " and SCTree.targetyear = " + UserInfo.TargetYear + " order by SCTree.id";
      if (id_resp_person != -1)
        sql = "select SCTree.id_SC, SC.Name1, SC.Level1, SC.IDControlperson, SC.targetvalue, sc.unit from SCTree " +
                      "left join SC on SCTree.id_SC = SC.ID " +
                      "left join staff_kpi sk on sk.id_kpi = SC.ID " +
                      "where SCTree.id_parent=" + id_root + " and SC.IDControlperson = " + id_resp_person + " and SCTree.targetyear = " + UserInfo.TargetYear + " order by SCTree.id";

      return mySql.SqlSelect(sql);
    }
    public static void SelectSCDelegated(CMySql mySql, string id_root, string IDCONTROLPERSON, ref DataTable resTable)
    {
      if (resTable == null)
      {
        resTable = new DataTable();
        resTable.Columns.Add("id_SC", Type.GetType("System.Int32"));
        resTable.Columns.Add("Name1", Type.GetType("System.String"));
        resTable.Columns.Add("IDControlperson", Type.GetType("System.Int32"));
      }
      DataTable dt = BSCtree.SelectFromTreeAndSC(mySql, id_root,-1);

      foreach (DataRow dr in dt.Rows)
      {
        string sql = "select SCTree.id_SC, SC.Name1, SC.IDControlperson from SCTree " +
                          "left join SC on SCTree.id_SC = SC.ID " +
                          "where SCTree.id_parent=" + dr["id_SC"].ToString() +
                          " and SC.IDControlperson = " + IDCONTROLPERSON +
                          " and SCTree.targetyear = " + UserInfo.TargetYear.ToString() + " order by SCTree.id";

        DataTable dt_sk = mySql.SqlSelect(sql);
        foreach (DataRow dr_sk in dt_sk.Rows)
        {
          //   DataRow row = dt_sk.Rows.
          resTable.Rows.Add(dr_sk.ItemArray);
        }

        BSCtree.SelectSCDelegated(mySql, dr["id_SC"].ToString(), IDCONTROLPERSON, ref resTable);
      }


      //string sql = "select SK.ID_KPI as id_SC, SC.Name1, SC.Level1, SC.targetvalue from SC " +
      //            "inner join staff_kpi sk on SK.ID_KPI = SC.ID " +
      //            "where SC.IDCONTROLPERSON = " + IDCONTROLPERSON + "and SC.TARGETYEAR=" + UserInfo.TargetYear +
      //            "";
    }

    void AddContextMenuToNode(TreeViewItem root)
    {
      TreeViewItemHeader h = (TreeViewItemHeader)root.Header;
      NewNodeMenuContext(root);
      foreach (TreeViewItem node in root.Items)
        AddContextMenuToNode(node);
    }
    public void ChangeMode(bool isEdit)
    {
      _isEdit = isEdit;

      if (_isEdit)
      {
        ContextMenu menu = new ContextMenu();
        MenuItem item = new MenuItem();
        item.Header = "Добавить новую стратегическую цель"; item.Name = "i" + MenuConsts.AddNewPerspective.ToString();
        item.Click += new RoutedEventHandler(MenuItem_Click);
        menu.Items.Add(item);

        item = new MenuItem();
        item.Header = "Добавить существующую стратегическую цель"; item.Name = "i" + MenuConsts.AddExistingPerspective.ToString();
        item.Click += new RoutedEventHandler(MenuItem_Click);
        menu.Items.Add(item);

        TB_tViewName.ContextMenu = menu;
        tView.ContextMenu = menu;
      }
      else
      {
        TB_tViewName.ContextMenu = null;
        tView.ContextMenu = null;
      }
      foreach (TreeViewItem root in tView.Items)
      {
        AddContextMenuToNode(root);
      }
    }

    void AddNewPerspective(string nameSC, int typeHeader, TreeViewItem root)
    {
      string sql = "";
      DataRow dr_SCParent = null;
      string idSCParent = "0";
      if (root != null)
      {
        double TARGETVALUE_Delegated = 0;
        if (typeHeader == TypeHeader.Goal)
        {
          //проверка
          idSCParent = root.Name.Substring(1);
          sql = "select SC.* from SC " +
                 "where (SC.id = " + idSCParent + " and SC.targetyear = " + UserInfo.TargetYear + ") and (SC.IDCONTROLPERSON =" + UserInfo.Info.id_form + ")";
          DataTable dt = _mySql.SqlSelect(sql);
          if (dt.Rows.Count == 0)
          {
            MessageBox.Show("Вы не создавали стратегическую цель");
            return;
          }
          dr_SCParent = dt.Rows[0];
          dr_SCParent["targetBeginDate"] = new DateTime(Convert.ToInt32(UserInfo.TargetYear), 1, 1);
          dr_SCParent["targetEndDate"] = new DateTime(Convert.ToInt32(UserInfo.TargetYear), 12, 31);
        }

        double targetValue = 0;
        if (typeHeader != TypeHeader.Goal)
        {
          //проверка
          idSCParent = root.Name.Substring(1);
          sql = "select SC.*, SK.TARGETVALUE as TARGETVALUE_Delegated from SC " +
                "left join staff_kpi SK on SK.ID_KPI=SC.ID " +
                "where (SC.id = " + idSCParent + " and SC.targetyear = " + UserInfo.TargetYear + ") and (SK.ID_KPI=" + idSCParent + " and SK.ID_FORM=" + UserInfo.Info.id_form + ")";
          DataTable dt = _mySql.SqlSelect(sql);
          if (dt.Rows.Count == 0)
          {
            MessageBox.Show("Вам этот показатель не делегирован");
            return;
          }
          dr_SCParent = dt.Rows[0];
          TARGETVALUE_Delegated = Convert.ToDouble(dr_SCParent["TARGETVALUE_Delegated"]);


          //подсчёт остатка от целевого значения
          targetValue = 0;
          sql = "select SCTree.id_SC, SC.Name1, SC.Level1, SC.IDControlperson, SC.targetvalue from SCTree " +
                      "left join SC on SCTree.id_SC = SC.ID " +
                      "where SCTree.id_parent=" + idSCParent + " and SC.IDControlperson = " + UserInfo.Info.id_form + "  and SCTree.targetyear = " + UserInfo.TargetYear.ToString() + " order by SCTree.id";
          dt = _mySql.SqlSelect(sql);
          foreach (DataRow dr in dt.Rows)
          {
            targetValue += Math.Abs(Convert.ToDouble(dr["targetvalue"]));//одна строка
          }
        }

        sql = "insert into SC (level1, name1, period, period_input_data, targetBeginDate, targetEndDate, IDcontrolPerson, targetyear, targetvalue, unit) values (" +
            typeHeader.ToString() + ", '" + nameSC + "', '" + dr_SCParent["period"].ToString() + "', '" + dr_SCParent["period_input_data"].ToString() +
            "', " + _mySql.DateToString((DateTime)dr_SCParent["targetBeginDate"]) + ", " + _mySql.DateToString((DateTime)dr_SCParent["targetEndDate"]) +
            ", " + UserInfo.Info.id_form + ", " + UserInfo.TargetYear + ", " + Functions.ToString(TARGETVALUE_Delegated - targetValue) + ",'" + dr_SCParent["unit"].ToString() + "')";
        _mySql.SqlExec(sql);

      }
      else
      {
        sql = "insert into SC (level1, name1, IDcontrolPerson, targetyear) values (0, '" + nameSC + "', " + UserInfo.Info.id_form + ", " + UserInfo.TargetYear + ")";
        _mySql.SqlExec(sql);
      }

      //выбираем  добавленный показатель и добавляем его в дерево
      //sql = "select * from SC where name1 = '" + nameSC + "'" + " and targetyear = " + UserInfo.TargetYear.ToString() + " and id = max(id)";      
      sql = "select max(id) as id from SC where name1 = '" + nameSC + "'" + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt_SC = _mySql.SqlSelect(sql);
      DataRow dr_SC = dt_SC.Rows[0];
      sql = "insert into SCTree (id_parent, id_SC, TargetYear) values (" + idSCParent + ", " + dr_SC["id"].ToString() + ", " + UserInfo.TargetYear + ")";
      _mySql.SqlExec(sql);

      //обновляем дерево
      TreeViewItem node = new TreeViewItem();
      TreeViewItemHeader h = null;
      if (root == null)
      {
        node.Tag = new NodeTag() { root = node, IdControlPerson = UserInfo.Info.id_form, IdResponsiblePerson = "-1" };
        h = new TreeViewItemHeader("/Controlling;component/Ico/stock_mail_flag_for_followup_32.png", nameSC, TypeHeader.Perspective, null);
      }
      else
      {
        node.Tag = new NodeTag() { root = root, IdControlPerson = UserInfo.Info.id_form, IdResponsiblePerson = "-1" };
        h = new TreeViewItemHeader("/Controlling;component/Ico/SCNotApproved.png", nameSC, typeHeader, root);
        if (typeHeader == TypeHeader.IndependScore) h = new TreeViewItemHeader("/Controlling;component/Ico/free_for_job.png", nameSC, typeHeader, root);

      }
      node.Header = h; node.Name = "n" + dr_SC["id"].ToString();
      NewNodeMenuContext(node);
      if (root == null) tView.Items.Add(node);
      else
      {
        root.Items.Add(node);
        root.IsExpanded = true;
        AddContextMenuToNode(root);
      }
      node.MouseLeftButtonUp += new MouseButtonEventHandler(Item_MouseLeftButtonUp);

      node.IsSelected = true;
      SelectItem(node);
    }

    void DeleteKPI(string idSC)
    {
      string sql = "select * from SCTree where id_parent = " + idSC + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt = _mySql.SqlSelect(sql);
      foreach (DataRow dr in dt.Rows)
      {
        DeleteKPI(dr["id_SC"].ToString());
        sql = "delete from SCTree where id = " + dr["id"].ToString();
        _mySql.SqlExec(sql);
        sql = "Update SC set isInTree = 0 where id = " + idSC;
        _mySql.SqlExec(sql);
      }
      sql = "delete from SCTree where id_SC = " + idSC;
      _mySql.SqlExec(sql);

      string date = (new DateTime(DateTime.Today.Year, 1, 1)).ToShortDateString();
      sql = "delete from SC where id = " + idSC;
      //sql = "Update SC set isInTree = 0, period = '" + Period.No + "', period_input_data = '" + Period.No + "', precision = 0, minValue = 0, maxValue = 0, " +
      //    "targetValue = 0, targetBeginDate = '" + date + "', targetEndDate = '" + date + "', lowCriticalLevel= 0, hiCriticalLevel=0, IDresponsiblePerson = -1, " +
      //    " IDcontrolPerson = -1, Formula = '', descr = '', plan = 0, fact = 0, isApproved = 0  where id = " + idSC;
      _mySql.SqlExec(sql);

      sql = "delete from SC_Period where idSC = " + idSC;
      _mySql.SqlExec(sql);

      sql = "delete from staff_KPI_history where id_KPI = " + idSC;
      _mySql.SqlExec(sql);

      sql = "delete from SC_Actions where idSC = " + idSC;
      _mySql.SqlExec(sql);

      sql = "delete from SC_history where idSC = " + idSC;
      _mySql.SqlExec(sql);

      sql = "delete from history where idSC = " + idSC;
      _mySql.SqlExec(sql);

      sql = "delete from messages where idSC = " + idSC;
      _mySql.SqlExec(sql);
    }
    void MenuItem_Click(object sender, RoutedEventArgs e)
    {
      //добавляется только корень
      MenuItem menu_item = (MenuItem)sender;
      MenuActions(menu_item);
    }
    public void MenuActions(MenuItem menu_item)
    {
      string sql = "";
      if (menu_item.Tag != null)
      {
        //  if (((NodeTag)((TreeViewItem)menu_item.Tag).Tag).IsApproved)
        //  {
        //    MessageBox.Show("Необходимо разблокировать показатель");
        //    return;
        //  }
        //  sql = "select * from staff_kpi where id_form = " + UserInfo.id_form + " and id_kpi = " + ((TreeViewItem)menu_item.Tag).Name.Substring(1);
        //  DataTable dt = _mySql.SqlSelect(sql);
        //  if (dt.Rows.Count == 0)
        //  {
        //    MessageBox.Show("Вам не делегирован данный показатель");
        //    return;
        //  }
      }

      int type = Convert.ToInt32(menu_item.Name.Substring(1));

      if (type == MenuConsts.AddNewPerspective)
      {
        dlgAddPerspective dlg = new dlgAddPerspective("Добавить новую стратегическую цель");
        Nullable<bool> res = dlg.ShowDialog();
        if ((bool)res)
        {
          AddNewPerspective(dlg.NameSC, TypeHeader.Perspective, null);
        }
      }
      if (type == MenuConsts.AddNewGoal)
      {
        dlgAddPerspective dlg = new dlgAddPerspective("Добавить новую задачу");
        Nullable<bool> res = dlg.ShowDialog();
        if ((bool)res)
        {
          AddNewPerspective(dlg.NameSC, TypeHeader.Goal, (TreeViewItem)menu_item.Tag);
        }
      }
      if (type == MenuConsts.AddNewSC)
      {
        dlgAddPerspective dlg = new dlgAddPerspective("Добавить показатель");
        Nullable<bool> res = dlg.ShowDialog();
        if ((bool)res)
        {
          AddNewPerspective(dlg.NameSC, TypeHeader.Score, (TreeViewItem)menu_item.Tag);
        }
      }
      if (type == MenuConsts.AddNewIndependSC)
      {
        dlgAddPerspective dlg = new dlgAddPerspective("Добавить независимый показатель");
        Nullable<bool> res = dlg.ShowDialog();
        if ((bool)res)
        {
          AddNewPerspective(dlg.NameSC, TypeHeader.IndependScore, (TreeViewItem)menu_item.Tag);
        }
      }

      if (type == MenuConsts.AddExistingPerspective)
      {
        dlgSelectFromNavigator dlgSelectFromNavigator = new dlgSelectFromNavigator(_mySql, TypeHeader.Perspective, false);
        Nullable<bool> res = dlgSelectFromNavigator.ShowDialog();
        if ((bool)res)
          if (dlgSelectFromNavigator.nav_args != null)
            AddNewPerspective(dlgSelectFromNavigator.nav_args.name_SC, TypeHeader.Perspective, (TreeViewItem)menu_item.Tag);
      }
      if (type == MenuConsts.AddExistingGoal)
      {
        dlgSelectFromNavigator dlgSelectFromNavigator = new dlgSelectFromNavigator(_mySql, TypeHeader.Perspective, false);
        Nullable<bool> res = dlgSelectFromNavigator.ShowDialog();
        if ((bool)res)
          if (dlgSelectFromNavigator.nav_args != null)
            AddNewPerspective(dlgSelectFromNavigator.nav_args.name_SC, TypeHeader.Goal, (TreeViewItem)menu_item.Tag);
      }
      if (type == MenuConsts.AddExistingSC)
      {
        //        dlgSelectFromNavigator dlg = new dlgSelectFromNavigator(_mySql, TypeHeader.Score, false);
        dlgSelectFromNavigator dlgSelectFromNavigator = new dlgSelectFromNavigator(_mySql, TypeHeader.Perspective, false);
        Nullable<bool> res = dlgSelectFromNavigator.ShowDialog();
        if ((bool)res)
          if (dlgSelectFromNavigator.nav_args != null)
            AddNewPerspective(dlgSelectFromNavigator.nav_args.name_SC, TypeHeader.Score, (TreeViewItem)menu_item.Tag);
      }

      if (type == MenuConsts.Delete)
      {
        TreeViewItemHeader h = (TreeViewItemHeader)((TreeViewItem)menu_item.Tag).Header;
        TreeViewItem item = (TreeViewItem)menu_item.Tag;
        string idSC = item.Name.Substring(1);

        sql = "SELECT SC.IDcontrolPerson FROM SC where id = " + idSC;
        DataTable dt_SC = _mySql.SqlSelect(sql);
        if (dt_SC.Rows.Count > 0)
        {
          DataRow dr = dt_SC.Rows[0];
          if (dr["IDcontrolPerson"].ToString() != UserInfo.Info.id_form)
          {
            sql = "select U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO from app_portal.v_tpu_users U Where U.LICHNOST_ID=" + dr["IDcontrolPerson"].ToString();
            DataTable dt = _mySql.SqlSelect(sql);


            MessageBox.Show("Удалить имеет право владелец: \r\n " + dt.Rows[0]["fio"], "Удаление", MessageBoxButton.OK);
            return;
          }
        }
        else
        {
          MessageBox.Show("неизвестный владелец", "Удаление", MessageBoxButton.OK);
          return;
        }

        sql = "select id_form, id_dep from staff_KPI where id_KPI = " + idSC + " and targetyear = " + UserInfo.TargetYear.ToString();
        DataTable dt_staff_KPI = _mySql.SqlSelect(sql);
        string deleg = "";
        foreach (DataRow dr1 in dt_staff_KPI.Rows)
        {
          DataRow dr_form = SQL.GetUserInfo(_mySql, dr1["id_form"].ToString()).Rows[0];

          deleg += dr_form["fio"].ToString() + " - " + dr_form["dep_name"].ToString() + "\r\n";
        }
        string mess = "";


        if (deleg == "") mess = "Удалить \r\n" + h.Text;
        else
        {
          //mess = "Показатель \"" + h.Text + "\" \r\n\r\nДелегирован сотрудникам: " + "\r\n\r\n" + deleg + "\r\n Удаление невозможно";
          //MessageBox.Show(mess, "Удаление", MessageBoxButton.OK);
          //return;
          mess = "Показатель \"" + h.Text + "\" \r\n\r\nДелегирован сотрудникам: " + "\r\n\r\n" + deleg + "\r\n Удалить?";
          if (MessageBox.Show(mess, "Удаление", MessageBoxButton.YesNo) == MessageBoxResult.No) return;
        }
        if (MessageBox.Show(mess, "Удаление", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
        {


          TreeViewItem root = h.ParentNode;
          DeleteKPI(idSC);
          try
          {
            ((TreeViewItem)item.Parent).Items.Remove(item);
          }
          catch
          {
            tView.Items.Remove(item);
          }
          //        FillScorecardTree();
          if (root != null)
          {
            root.IsSelected = true;
            SelectItem(root);
            //          root.IsExpanded = true;

          }
        }
      }
    }

    void NewNodeMenuContext(TreeViewItem node)
    {
      if (!_isEdit)
      {
        node.ContextMenu = null;
        return;
      }
      ContextMenu menu = new ContextMenu();
      MenuItem item = null;

      TreeViewItemHeader h = (TreeViewItemHeader)node.Header;
      int typeHeader = h.TypeItem;

      if ((typeHeader != TypeHeader.Goal) & (typeHeader != TypeHeader.Score))
      {
        item = new MenuItem(); item.Tag = node;
        item = new MenuItem(); item.Tag = node;
        item.Header = "Добавить новую стратегическую задачу"; item.Name = "i" + MenuConsts.AddNewGoal.ToString();
        item.Click += new RoutedEventHandler(MenuItem_Click);
        menu.Items.Add(item);

        item = new MenuItem(); item.Tag = node;
        item.Header = "Добавить существующую стратегическую задачу"; item.Name = "i" + MenuConsts.AddExistingGoal.ToString();
        item.Click += new RoutedEventHandler(MenuItem_Click);
        menu.Items.Add(item);
      }
      else
      {
        item = new MenuItem(); item.Tag = node;
        item.Header = "Добавить новый показатель";
        item.Click += new RoutedEventHandler(MenuItem_Click);
        item.Name = "i" + MenuConsts.AddNewSC.ToString();
        menu.Items.Add(item);

        item = new MenuItem(); item.Tag = node;
        item.Header = "Добавить существующий показатель";
        item.Click += new RoutedEventHandler(MenuItem_Click);
        item.Name = "i" + MenuConsts.AddExistingSC.ToString();
        menu.Items.Add(item);

        item = new MenuItem(); item.Tag = node;
        item.Header = "Добавить независимый показатель";
        item.Click += new RoutedEventHandler(MenuItem_Click);
        item.Name = "i" + MenuConsts.AddNewIndependSC.ToString();
        menu.Items.Add(item);
      }
      item = new MenuItem(); item.Tag = node;
      item.Header = "Удалить";
      item.Click += new RoutedEventHandler(MenuItem_Click);
      item.Name = "i" + MenuConsts.Delete.ToString();
      menu.Items.Add(item);

      node.ContextMenu = menu;
    }
    void SetVisibleParentNodes(TreeViewItem node)
    {
      NodeTag nTag = (NodeTag)node.Tag;
      while (nTag.sce.idSCParent != 0)
      {
        nTag.root.Visibility = System.Windows.Visibility.Visible;
        nTag.sce.Visibility = System.Windows.Visibility.Visible;
        node = nTag.root;
        nTag = (NodeTag)node.Tag;
      }
    }

    public void SetOnlyUserSC()
    {
      if (tView.Items.Count < 1) return;

      foreach (TreeViewItem item in tView.Items)
        OnlyUserSC(item);
    }
    void OnlyUserSC(TreeViewItem root)
    {
      try
      {
        bool isOnlyUserSC = UserInfo.isOnlyUserSC;
        foreach (TreeViewItem objTreeviewItem in root.Items)
        {
          if ((objTreeviewItem.Tag != null) & isOnlyUserSC)
          {
            NodeTag nTag = (NodeTag)objTreeviewItem.Tag;
            if ((nTag.IdControlPerson == UserInfo.Info.id_form) | (nTag.IdResponsiblePerson == UserInfo.Info.id_form))
            {
              objTreeviewItem.Visibility = System.Windows.Visibility.Visible;
              SetVisibleParentNodes(objTreeviewItem);
            }
            else
            {
              objTreeviewItem.Visibility = System.Windows.Visibility.Collapsed;
              nTag.sce.Visibility = System.Windows.Visibility.Collapsed;
            }
          }
          else
          {
            objTreeviewItem.Visibility = System.Windows.Visibility.Visible;
            ((NodeTag)objTreeviewItem.Tag).sce.Visibility = System.Windows.Visibility.Visible;
          }

          OnlyUserSC(objTreeviewItem);
        }
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }
    }

    void FillScorecardBranch(TreeViewItem root, string id_root, string id_perspective)
    {
      string sql = "select SCTree.id_parent, SCTree.id_SC, SC.name1, SC.level1, SC.IsApproved, SC.UNIT, SC.TargetValue, SC.IDCONTROLPERSON, SC.IDRESPONSIBLEPERSON, " +
                  "U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO " +
                  "from SCTree " +
                  "left join SC on SC.ID = SCTree.id_SC " +
                  "left join app_portal.v_tpu_users U on U.LICHNOST_ID = SC.IDCONTROLPERSON " +
                   "where SCTree.id_parent=" + id_root + " and SCTree.targetyear = " + UserInfo.TargetYear.ToString() + " order by SCTree.id";
      DataTable dt = _mySql.SqlSelect(sql);

      foreach (DataRow dr_SCTree in dt.Rows)
      {
        TreeViewItem node = new TreeViewItem();
        TreeViewItemHeader h = null;
        int level1 = _mySql.ToInt32(dr_SCTree["Level1"]);


        h = new TreeViewItemHeader("/Controlling;component/Ico/SCNotApproved.png", dr_SCTree["name1"].ToString(), level1, root);
        if (_mySql.ToBool(dr_SCTree["IsApproved"]))
        {
          h.ImgSource = "/Controlling;component/Ico/SCApproved.png";
          if (node.ContextMenu != null) node.ContextMenu.Visibility = System.Windows.Visibility.Collapsed;
        }
        if (level1 == TypeHeader.IndependScore)
          h.ImgSource = "/Controlling;component/Ico/free_for_job.png";


        ScorecardElement se = new ScorecardElement();
        se.SetIndicator(dr_SCTree, Convert.ToInt32(id_perspective), _mySql, new ScorecardElement.ScorecardElementdEventHandler(OnScorecardElementEvent));

        node.Header = h; node.Name = "n" + dr_SCTree["id_SC"].ToString();
        node.Tag = new NodeTag() { root = root, IdControlPerson = dr_SCTree["IDCONTROLPERSON"].ToString(), IdResponsiblePerson = dr_SCTree["IDRESPONSIBLEPERSON"].ToString(), IsApproved = _mySql.ToBool(dr_SCTree["IsApproved"]), sce = se };
        node.MouseLeftButtonUp += new MouseButtonEventHandler(Item_MouseLeftButtonUp);
        NewNodeMenuContext(node);
        root.Items.Add(node);
        node.Visibility = System.Windows.Visibility.Visible;

        ((NodeTag)root.Tag).sce.AddChildrenSC(se);

        FillScorecardBranch(node, dr_SCTree["id_SC"].ToString(), id_perspective);
      }
    }
    public void FillScorecardTree()
    {
      tView.Items.Clear();

      //     string sql = "select * from SCTree where id_parent=0  and targetyear = " + UserInfo.TargetYear.ToString() + " order by id";
      string sql = "select SCTree.id_parent, SCTree.id_SC, SC.name1, SC.level1, SC.IsApproved, SC.UNIT, SC.TargetValue, SC.IDCONTROLPERSON, SC.IDRESPONSIBLEPERSON, " +
                  "U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO " +
                  "from SCTree " +
                  "left join SC on SC.ID = SCTree.id_SC " +
                  "left join app_portal.v_tpu_users U on U.LICHNOST_ID = SC.IDCONTROLPERSON " +
                  "where SCTree.id_parent=0 and SCTree.targetyear = " + UserInfo.TargetYear.ToString() + " order by SCTree.id";
      DataTable dt = _mySql.SqlSelect(sql);
      foreach (DataRow dr in dt.Rows)
      {
        TreeViewItem root = new TreeViewItem(); root.Tag = null;
        TreeViewItemHeader h = new TreeViewItemHeader("/Controlling;component/Ico/stock_mail_flag_for_followup_32.png", dr["name1"].ToString(), TypeHeader.Perspective, null);
        root.Header = h; root.Name = "n" + dr["id_SC"].ToString();
        tView.Items.Add(root);

        root.MouseLeftButtonUp += new MouseButtonEventHandler(Item_MouseLeftButtonUp);
        //root.MouseDoubleClick += new MouseButtonEventHandler(TreeViewItem_MouseDoubleClick);
        NewNodeMenuContext(root);

        ScorecardElement se = new ScorecardElement(); se.SetIndicator(dr, Convert.ToInt32(dr["id_parent"]), _mySql, null);
        root.Tag = new NodeTag() { root = root, IdControlPerson = dr["IDCONTROLPERSON"].ToString(), IdResponsiblePerson = dr["IDRESPONSIBLEPERSON"].ToString(), IsApproved = _mySql.ToBool(dr["IsApproved"]), sce = se };

        FillScorecardBranch(root, dr["id_SC"].ToString(), dr["id_SC"].ToString());
        root.IsExpanded = true;
      }
      if (tView.Items.Count > 0) ((TreeViewItem)tView.Items[0]).IsExpanded = true;
      IsApprovedAll();
      //TreeViewItem item = FindItem("n69", (TreeViewItem)tView.Items[0]);
      //OnlyUserSC((TreeViewItem)tView.Items[0]);
    }

    private void tView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      //реакция на выделение показателя
      TreeViewItem NewItem = (TreeViewItem)e.NewValue;
      if (NewItem == null) return;
      if (e.OldValue != null)
      {
        TreeViewItem OldItem = (TreeViewItem)e.OldValue;
        string Newid_SC_root = ((NodeTag)NewItem.Tag).root.Name.Substring(1);
        string Oldid_SC_root = ((NodeTag)OldItem.Tag).root.Name.Substring(1);
        if (Newid_SC_root != Oldid_SC_root)
        {
          TreeViewItem item = null;
          foreach (object o in tView.Items)
          {
            item = FindTreeViewItem(((NodeTag)NewItem.Tag).root.Name, (TreeViewItem)o);
            if (item != null) break;
          }
          if (item != null) SelectItem(item);
          SelectItem(NewItem);
        }
        else SelectItem(NewItem);
      }
      else SelectItem(NewItem);




      if (_curr_item == null) return;
      //TreeViewItemHeader h = (TreeViewItemHeader)((item).Header);
      //TreeViewItemHeader ch = (TreeViewItemHeader)(_curr_item.Header);
      //  if (h.Text != ch.Text)
      //  {
      //  _curr_item.IsSelected = true;
      //  }

    }
    void SelectItem(TreeViewItem item)
    {
      if (item == null) return;
      if (((NodeTag)item.Tag).root == null)
      {
        return;
      }

      TreeViewItemHeader h = (TreeViewItemHeader)((item).Header);

      SelectedItemEventArgs args = new SelectedItemEventArgs();
      args.id_SC_root = "-1";
      if (item.Tag != null)
      {
        args.id_SC_root = ((NodeTag)item.Tag).root.Name.Substring(1);
        args.sce = ((NodeTag)item.Tag).sce;
      }
      args.id_SC = item.Name.Substring(1);
      args.name_SC = h.Text;
      args.isEdit = _isEdit;

      //args.curr_item = _curr_item;

      if (_curr_item != null)
      {
        TreeViewItemHeader ch = (TreeViewItemHeader)(_curr_item.Header);
        if (h.Text != ch.Text)
        {
          _curr_item = item;
          OnSelectedItem(args);
        }
      }
      else
      {
        _curr_item = item;
        OnSelectedItem(args);
      }
    }
    private void Item_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {

      //TreeViewItem item = (TreeViewItem)tView.SelectedItem;
      //SelectItem(item);
    }

    #region Events

    public class SelectedItemEventArgs : EventArgs
    {
      public string id_SC_root;
      public string id_SC;
      public string name_SC;

      public bool isEdit;

      public ScorecardElement sce;

      //public object curr_item;

      public bool IsEqual(SelectedItemEventArgs args)
      {
        if (id_SC_root != args.id_SC_root) return false;
        if (id_SC != args.id_SC) return false;
        if (name_SC != args.name_SC) return false;
        if (isEdit != args.isEdit) return false;

        return true;
      }
    }
    public delegate void SelectedItemEventHandler(Object sender, SelectedItemEventArgs args);
    public event SelectedItemEventHandler SelectedSCInTree;
    protected virtual void OnSelectedItem(SelectedItemEventArgs args)
    {
      if (SelectedSCInTree != null)
      {
        SelectedSCInTree(this, args);
      }
    }
    #endregion Events
    #region Обработка событий от элементов ScorecardElement
    void SelectIndicator(ScorecardElement se)
    {
      if (se.childrenSC.Count > 0)
      {
      }
      else
      {

      }
      //if (CurrentIndicator != null) if (CurrentIndicator.idSC != se.idSC) CurrentIndicator.SelectIndicator(false);
      //CurrentIndicator = se;
      //if (_wpChild != null)
      //{
      //  _wpChild.Setup(_mySql, Scorecard.KPI, _id_SC, "Показатели следующего уровня", null);//level = Scorecard.KPI
      //  _wpChild.UpdateChildElements(se);
      //  se.Visibility = OnlyUserSCInChild();

      //  IndWrapPanelEventArgs args = new IndWrapPanelEventArgs();
      //  args.Element = CurrentIndicator; args.Event = IsShowSmartEvent;
      //  OnIndWrapPanelEvent(args);
      //}
    }
    void OnScorecardElementEvent(Object sender, ScorecardElement.ScorecardElementEventArgs e)
    {
      //возникает при действии с индикатором

      //SelectedItemEventArgs args = new SelectedItemEventArgs() { isEdit = false, sce = se };
      //OnSelectedItem(args);
      if (e.Event == ScorecardElement.IsCheckedEvent)
      {
        ScorecardElement se = e.Element;
        SelectedItemEventArgs args = new SelectedItemEventArgs();
        args.id_SC_root = se.idSCParent.ToString();
        args.id_SC = se.idSC.ToString();
        args.isEdit = false;
        args.name_SC = se.IndName;
        args.sce = se;
        OnSelectedItem(args);


      }
      if (e.Event == ScorecardElement.IsUnCheckedEvent)
      {
        //       if (_wpChild != null) _wpChild.UpdateChildElements(null);
      }
      //if (e.Event == ScorecardElement.IsNameEditEvent)
      //{
      //  if (ParentIndicator != null)
      //  {
      //    if (MessageBox.Show("Изменение будет применено ко всем показателям. \r\n Продолжить?", "Предупреждение", MessageBoxButton.YesNo) == MessageBoxResult.No)
      //      return;
      //  }
      //  string sql = "Update SC set name1 = '" + se.IndName + "' where id=" + se.idSC.ToString();
      //  _mySql.SqlExec(sql);
      //}
    }
    #endregion

    private void tView_PreviewMouseMove(object sender, MouseEventArgs e)
    {
      //if (e.LeftButton == MouseButtonState.Pressed ||
      // e.RightButton == MouseButtonState.Pressed && !_IsDragging)
      //{
      //  Point position = e.GetPosition(null);
      //  if (Math.Abs(position.X - _startPoint.X) >
      //          SystemParameters.MinimumHorizontalDragDistance ||
      //      Math.Abs(position.Y - _startPoint.Y) >
      //          SystemParameters.MinimumVerticalDragDistance)
      //  {
      //    StartDrag(e);
      //  }
      //}           
    }
    private void tView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      //_startPoint = e.GetPosition(null);

    }
    private void StartDrag(MouseEventArgs e)
    {
      //_IsDragging = true;
      ////object temp = this.tView.SelectedItem;

      //TreeViewItemHeader h = (TreeViewItemHeader)((TreeViewItem)tView.SelectedItem).Header;
      //object temp = h.Text;

      //DataObject data = null;

      //data = new DataObject("inadt", temp);

      //if (data != null)
      //{
      //  DragDropEffects dde = DragDropEffects.Move;
      //  if (e.RightButton == MouseButtonState.Pressed)
      //  {
      //    dde = DragDropEffects.All;
      //  }
      //  DragDropEffects de = DragDrop.DoDragDrop(this.tView, data, dde);
      //}
      //_IsDragging = false;
    }
    private void tView_DragOver(object sender, DragEventArgs e)
    {
      //try
      //{
      //  Point currentPosition = e.GetPosition(tView);

      //  if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
      //   (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
      //  {
      //    // Verify that this is a valid drop and then store the drop target
      //    //TreeViewItem item = GetNearestContainer(e.OriginalSource as UIElement);
      //    //if (CheckDropTarget(draggedItem, item))
      //    //{
      //    //  e.Effects = DragDropEffects.Move;
      //    //}
      //    //else
      //    //{
      //    //  e.Effects = DragDropEffects.None;
      //    //}
      //  }
      //  e.Handled = true;
      //}
      //catch (Exception)
      //{
      //}
    }
    private void tView_MouseMove(object sender, MouseEventArgs e)
    {
      try
      {
        if (e.LeftButton == MouseButtonState.Pressed)
        {
          Point currentPosition = e.GetPosition(tView);

          if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
                  (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
          {
            //						draggedItem = (TreeViewItem)tView.SelectedItem;
            TreeViewItemHeader h = (TreeViewItemHeader)((TreeViewItem)tView.SelectedItem).Header;
            draggedItem = h.Text;
            if (draggedItem != null)
            {
              DragDropEffects finalDropEffect = DragDrop.DoDragDrop(tView, tView.SelectedValue, DragDropEffects.Move);
              //Checking target is not null and item is 
              ////dragging(moving)
              //if ((finalDropEffect == DragDropEffects.Move) &&(_target != null))
              //{
              //  // A Move drop was accepted
              //  if (!draggedItem.Header.ToString().Equals(_target.Header.ToString()))
              //  {
              //    CopyItem(draggedItem, _target);
              //    _target = null;
              //    draggedItem = null;
              //  }
              //}
            }
          }
        }
      }
      catch (Exception)
      {
      }
    }
    private void tView_MouseDown(object sender, MouseButtonEventArgs e)
    {
      //if (e.ChangedButton == MouseButton.Left)
      //{
      //  _lastMouseDown = e.GetPosition(tView);
      //}
    }


    void FillScoreCardTable()
    {
      if (isOnlyOwn)
      {
        string sql = "select * from SC where IDControlPerson=" + UserInfo.Info.id_form + " where targetyear = " + UserInfo.TargetYear.ToString();
        DataTable dt_SC = _mySql.SqlSelect(sql);
      }
      if (isOnlyDelegation)
      {
        string sql = "select SC.name1, form.fio from SC, form " + "join form on where IDControlPerson=" + UserInfo.Info.id_form + " where targetyear = " + UserInfo.TargetYear.ToString();
        DataTable dt_SC = _mySql.SqlSelect(sql);
      }
    }
    private void ch_isOnlyOwn_Checked(object sender, RoutedEventArgs e)
    {
      isOnlyOwn = true;
      FillScoreCardTable();
    }
    private void ch_isOnlyDelegation_Checked(object sender, RoutedEventArgs e)
    {
      isOnlyDelegation = true;
      FillScoreCardTable();
    }
    private void ch_isOnlyDelegation_Unchecked(object sender, RoutedEventArgs e)
    {
      isOnlyDelegation = false;
      FillScorecardTree();
    }
    private void ch_isOnlyOwn_Unchecked(object sender, RoutedEventArgs e)
    {
      isOnlyOwn = false;
      FillScorecardTree();
    }

    bool IsApprovedAll()
    {
      bool res = true;
      string sql = "SELECT SC.id, SC.isApproved, SC.name1 FROM SCTree INNER JOIN SC ON SCTree.id_SC = SC.id  where SCTree.id_parent=0" + " and SC.targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt = _mySql.SqlSelect(sql);
      foreach (DataRow dr in dt.Rows)
      {
        if (!_mySql.ToBool(dr["isApproved"]))
        {
          res = false;
          break;
        }

      }

      //if (res) b_PlanIDO.IsEnabled = true;
      //else b_PlanIDO.IsEnabled = false;

      return res;
    }


    TreeViewItem FindTreeViewItem(string name, TreeViewItem root)
    {
      TreeViewItem res = null;

      foreach (TreeViewItem objTreeviewItem in root.Items)
      {
        if ((objTreeviewItem.Name == name))
        {
          return objTreeviewItem;
        }
        FindTreeViewItem(name, objTreeviewItem);
      }
      return res;
    }
    public void AproveSC(bool IsApproved, string idSC)
    {
      string name = "n" + idSC;
      TreeViewItem item = null;
      item = FindTreeViewItem(name, (TreeViewItem)tView.Items[0]);

      if (_curr_item == null) return;
      TreeViewItemHeader h = (TreeViewItemHeader)((_curr_item).Header);
      if (h.TypeItem == TypeHeader.Goal) IsApprovedAll();

      if (IsApproved)
      {
        h.ImgSource = "/Controlling;component/Ico/SCApproved.png";
      }
      else
      {
        h.ImgSource = "/Controlling;component/Ico/SCNotApproved.png";
      }
    }
    public void ChangeNameSC(string newName, string idSC)
    {
      if (_curr_item == null) return;
      TreeViewItem item = FindTreeViewItem("n" + idSC, (TreeViewItem)tView.Items[0]);
      //if (item != null)
      //{
      //  TreeViewItemHeader h = (TreeViewItemHeader)item.Header;
      //  h.Text = newName;
      //}
      //else
      {
        TreeViewItemHeader h = (TreeViewItemHeader)_curr_item.Header;
        h.Text = newName;
      }
    }

  }
  class MenuConsts
  {
    public static int AddNewPerspective = 1;
    public static int AddExistingPerspective = 2;
    public static int AddNewSC = 3;
    public static int AddExistingSC = 4;
    public static int AddNewGoal = 5;
    public static int AddExistingGoal = 6;
    public static int Delete = 7;
    public static int AddNewIndependSC = 8;
  }
  class NodeTag
  {
    //в принципе root = node.Parent 
    //IdControlPerson, IdResponsiblePerson - для выбора показателей (не) имеющих отношение к пользователю
    public TreeViewItem root;
    public string IdControlPerson;
    public string IdResponsiblePerson;
    public bool IsApproved;
    public ScorecardElement sce;
  }

}
