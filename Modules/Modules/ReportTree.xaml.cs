﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Word = Microsoft.Office.Interop.Word;

using System.Data;
using System.IO;
using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для ReportTree.xaml
  /// </summary>
  public partial class ReportTree : UserControl
  {
    CMySql _mySql;

    public ReportTree()
    {
      InitializeComponent();
    }
    public void Set(CMySql mySql)
    {
      _mySql = mySql;
      FillReportTree();
    }

    void NewNodePlanMenuContext(TreeViewItem node)
    {
      ContextMenu itemReportsMenu = new ContextMenu();

      TreeViewItemHeader h = (TreeViewItemHeader)node.Header;
      int typeHeader = h.TypeItem;

      if (typeHeader == TypeHeader.Goal)
      {
        MenuItem item = new MenuItem(); item.Tag = node;
        item.Header = "Создать план"; item.Name = "iPlan";
        item.Click += new RoutedEventHandler(MenuItemReport_Click);
        itemReportsMenu.Items.Add(item);

        item = new MenuItem(); item.Tag = node;
        item.Header = "Создать отчёт на текущий период"; item.Name = "iReport";
        item.Click += new RoutedEventHandler(MenuItemReport_Click);
        itemReportsMenu.Items.Add(item);
      }
      node.ContextMenu = itemReportsMenu;
    }

    TreeViewItem PlanItem(string f_name, string id, string id_form, bool isApproved, TreeViewItem parent_node)
    {
      TreeViewItem node = new TreeViewItem();
      node.Tag = id_form;
      TreeViewItemHeader h = new TreeViewItemHeader("/Controlling;component/Ico/rep_not_approved.png", f_name, TypeHeader.Score, parent_node);
      if (isApproved)
      {
        h.ImgSource = "/Controlling;component/Ico/rep_approved.png";
        if (node.ContextMenu != null) node.ContextMenu.Visibility = System.Windows.Visibility.Collapsed;
      }
      node.Header = h; node.Name = "n" + id;
      node.MouseDoubleClick += new MouseButtonEventHandler(itemReports_MouseDoubleClick);

      ContextMenu menu = new ContextMenu();
      MenuItem mitem = new MenuItem();
      mitem.Header = "Удалить"; mitem.Name = "iDelete";
      mitem.Click += new RoutedEventHandler(MenuItemReport_Click);
      menu.Items.Add(mitem);
      mitem.Tag = node;

      node.ContextMenu = menu;
      return node;
    }
    private void itemReports_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      TreeViewItem item = (TreeViewItem)sender;
      string path_rep = Directory.GetCurrentDirectory() + "\\Base\\Reports";

      DataTable dt = _mySql.SqlSelect("select * from reports where id=" + item.Name.Substring(1) + " and targetyear = " + UserInfo.TargetYear.ToString());
      DataRow row = dt.Rows[0];
      string file_name = UserInfo.TempDir + "\\" + System.IO.Path.GetRandomFileName();
      try
      {
        if (File.Exists(file_name)) File.Delete(file_name);
        FileStream FS = new FileStream(file_name, FileMode.Create);
        byte[] blob = (byte[])row["file1"];
        FS.Write(blob, 0, blob.Length);
        FS.Close();
        FS = null;
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }

      Object missingObj = System.Reflection.Missing.Value;
      Object trueObj = true;
      Object falseObj = false;
      Word._Application _application = new Word.Application(); _application.Visible = true;
      Word._Document _document = null;
      try
      {
        _document = _application.Documents.Open(file_name);
      }
      catch
      {
        //     _document.Close(ref falseObj, ref  missingObj, ref missingObj);
        _application.Quit(ref missingObj, ref  missingObj, ref missingObj);
        _document = null;
        _application = null;
      }

    }

    public void MakePlanReport(string type, TreeViewItem itemReports)
    {
      string path_rep = Directory.GetCurrentDirectory() + "\\Temp";

      CReports rep = new CReports(path_rep);
      string f_name = "";
      string f_random = "";
      string f_id = "";
      if (type == "Plan")
      {
        string pathToTemplate = Directory.GetCurrentDirectory() + "\\Templates\\План работы.dotx";
        if (UserInfo.TargetYear == "2012") pathToTemplate = Directory.GetCurrentDirectory() + "\\Templates\\План работы 2012.dotx";

        f_name = "План работы " + UserInfo.TargetYear + "г.docx";

        f_random = rep.MakePlanIDO(_mySql, f_name, pathToTemplate);

        string sql = "insert into reports (fileName, dateCreation, file1, id_form, TargetYear) values('" +
          f_name + "'," + _mySql.DateToString(DateTime.Now) + ", :BlobParameter " + ", " + UserInfo.Info.id_form + ", " + UserInfo.TargetYear + ")";
        _mySql.InsertBlobOracle(sql, f_random);

        sql = "select MAX(id) as id from reports" + " where targetyear = " + UserInfo.TargetYear.ToString();
        DataRow dr = _mySql.SqlSelect(sql).Rows[0];
        f_id = dr["id"].ToString();
        //        itemReports.Items.Add(PlanItem(f_name, dr["id"].ToString(), UserInfo.id_form, false, itemReports));
      }
      if (type == "Report")
      {
        string pathToTemplate = Directory.GetCurrentDirectory() + "\\Templates\\Отчёт по работе.dotx";
        if (UserInfo.TargetYear == "2012") pathToTemplate = Directory.GetCurrentDirectory() + "\\Templates\\Отчёт по работе 2012.dotx";


        f_name = "Отчёт за " + Period.GetReportPeriod(DateTime.Now, Period.HalfYear) + " " + UserInfo.TargetYear + "г.docx";
        if (UserInfo.TargetYear == "2012") f_name = "Отчёт за " + Period.GetReportPeriod(DateTime.Now, Period.HalfYear) + " " + UserInfo.TargetYear + "г.docx";

      //  f_random = rep.MakePlanIDO(_mySql, f_name, true, pathToTemplate);

        string sql = "insert into reports (fileName, dateCreation, file1, id_form, TargetYear) values('" +
          f_name + "'," + _mySql.DateToString(DateTime.Now) + ", :BlobParameter" + ", " + UserInfo.Info.id_form + ", " + UserInfo.TargetYear + ")";
        _mySql.InsertBlobOracle(sql, f_random);

        sql = "select MAX(id) as id from reports" + " where targetyear = " + UserInfo.TargetYear.ToString();
        DataRow dr = _mySql.SqlSelect(sql).Rows[0];

        f_id = dr["id"].ToString();
        //        itemReports.Items.Add(PlanItem(f_name, dr["id"].ToString(), UserInfo.id_form, false, itemReports));
      }
      if (itemReports == null) tView.Items.Add(PlanItem(f_name, f_id, UserInfo.Info.id_form, false, itemReports));
      else itemReports.Items.Add(PlanItem(f_name, f_id, UserInfo.Info.id_form, false, itemReports));

      //      Functions.DeleteFile(f_random);
    }
    void MenuItemReport_Click(object sender, RoutedEventArgs e)
    {
      MenuItem menu_item = (MenuItem)sender;
      TreeViewItem itemReports = (TreeViewItem)menu_item.Tag;
      if ((string)itemReports.Tag != UserInfo.Info.id_form)
      {
        //MessageBox.Show("У Вас нет права работать с планом (отчётом) на данном уровне");
        //return;
      }
      MakePlanReport(menu_item.Name.Substring(1), itemReports);

      if (menu_item.Name.Substring(1) == "Delete")
      {
        string path_rep = Directory.GetCurrentDirectory() + "\\Temp";
        TreeViewItem item = (TreeViewItem)((MenuItem)e.Source).Tag;
        TreeViewItemHeader h = (TreeViewItemHeader)item.Header;
        string f_name = h.Text;
        if (File.Exists(path_rep + "\\" + f_name + ".docx"))
        {
          if (MessageBox.Show("Удалить <<" + f_name + ">>?", "Удаление", MessageBoxButton.YesNo) != MessageBoxResult.Yes) return;
          try
          {
          }
          catch (Exception exc)
          {
            MessageBox.Show(exc.Message);
            return;
          }
        }

        string sql = "delete from reports where id=" + item.Name.Substring(1);
        _mySql.SqlExec(sql);
        if (h.ParentNode == null) tView.Items.Remove(item);
        else ((TreeViewItem)h.ParentNode).Items.Remove(item);
      }

    }

    void FillReports(TreeViewItem node_staff, string id_form)
    {
      string sql = "select * from reports where id_form = " + id_form + " and TargetYear=" + UserInfo.TargetYear;
      DataTable dt_rep = _mySql.SqlSelect(sql);
      foreach (DataRow dr_rep in dt_rep.Rows)
      {

        TreeViewItem node = PlanItem(dr_rep["fileName"].ToString(), dr_rep["id"].ToString(), id_form,
                                    _mySql.ToBool(dr_rep["IsApproved"]), node_staff);

        if (node_staff == null) tView.Items.Add(node);
        else node_staff.Items.Add(node);
      }
    }
    void FillReportBranch(TreeViewItem root, string IDCONTROLPERSON)
    {
      string sql = "select SK.ID_FORM from SC " +
                   "inner join staff_kpi sk on SK.ID_KPI = SC.ID " +
                   "where SC.IDCONTROLPERSON = " + IDCONTROLPERSON + "and SC.TARGETYEAR=" + UserInfo.TargetYear +
                   "group by SK.ID_FORM";
      DataTable dt = _mySql.SqlSelect(sql); //выбираем сотрудникам которым делегирован показатель

      foreach (DataRow dr in dt.Rows)
      {
        sql = "select  UF.DOLGNOST " +
        "from SOTRUDNIK.LICHNOST_FOR_PHONE_SPR_V3 UF " +
        "inner join staff_kpi ST_KPI  on UF.DOLGNOST_ID = ST_KPI.ID_POSITION " +
        "where UF.LICHNOST_ID=" + dr["ID_FORM"].ToString() + " group by UF.DOLGNOST ";

        if (_mySql.SqlSelect(sql).Rows.Count != 0)
        {
          DataRow dr_staff = _mySql.SqlSelect(sql).Rows[0];
          TreeViewItem node_staff = new TreeViewItem(); node_staff.Tag = dr["ID_FORM"].ToString();
          TreeViewItemHeader h_staff = new TreeViewItemHeader("/Controlling;component/Ico/personal_32.png", dr_staff["DOLGNOST"].ToString(), TypeHeader.Goal, root);
          node_staff.Header = h_staff;
          NewNodePlanMenuContext(node_staff);


          if (root == null) tView.Items.Add(node_staff);
          else root.Items.Add(node_staff);
          node_staff.Visibility = System.Windows.Visibility.Visible;

          //files
          FillReports(node_staff, dr["ID_FORM"].ToString());
          FillReportBranch(node_staff, dr["ID_FORM"].ToString());
        }
      }

    }

    public void FillReportTree()
    {
      tView.Items.Clear();

      FillReports(null, UserInfo.id_head_all_department);
      FillReportBranch(null, UserInfo.id_head_all_department);

      if (tView.Items.Count > 0) ((TreeViewItem)tView.Items[0]).IsExpanded = true;
    }

    private void tView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      TreeViewItem item = (TreeViewItem)e.NewValue;

    }
  }
}
