﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

using System.IO;
using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для Navigator.xaml
  /// </summary>
  public partial class Navigator : UserControl
  {

    CMySql _mySql;

    ContextMenu itemBSCtreeMenu;
    ContextMenu itemReportsMenu;

    public BSCtree.SelectedItemEventHandler bSCtree_SelectedItem;
    public StaffTree.SelectedItemEventHandler staffTree_SelectedStaff;
    public Navigator()
    {
      InitializeComponent();

    }

    public void Set(CMySql mySql, int type, IndWrapPanel mainIWP)
    {
      if (UserInfo.Info.id_form == "-1")
      {
        itemBSCtree.Visibility = System.Windows.Visibility.Collapsed;
        itemReports.Visibility = System.Windows.Visibility.Collapsed;
      }
      _mySql = mySql;
      bSCtree.Set(_mySql, mainIWP); bSCtree.Caption = "";
      bSCtree.ChangeMode(false);

      bSCtree.SelectedSCInTree += new BSCtree.SelectedItemEventHandler(OnBSCTreeItem_Selected);
      staffTree.SelectedStaff += new StaffTree.SelectedItemEventHandler(OnStaffTreeItem_Selected);
      staffTree.Set(_mySql); staffTree.Caption = "";

      reportTree.Set(_mySql);
      //
      itemBSCtreeMenu = new ContextMenu();
      MenuItem item = new MenuItem();
      item.Header = "Добавить новую стратегическую цель"; item.Name = "i" + MenuConsts.AddNewPerspective.ToString();
      item.Click += new RoutedEventHandler(MenuItem_Click);
      itemBSCtreeMenu.Items.Add(item);

      item = new MenuItem();
      item.Header = "Добавить существующую стратегическую цель"; item.Name = "i" + MenuConsts.AddExistingPerspective.ToString();
      item.Click += new RoutedEventHandler(MenuItem_Click);
      itemBSCtreeMenu.Items.Add(item);
      //
      itemReportsMenu = new ContextMenu();
      item = new MenuItem();
      item.Header = "Создать план"; item.Name = "iPlan";
      item.Click += new RoutedEventHandler(MenuItemReport_Click);
      itemReportsMenu.Items.Add(item);

      item = new MenuItem();
      item.Header = "Создать отчёт на текущий период"; item.Name = "iReport";
      item.Click += new RoutedEventHandler(MenuItemReport_Click);
      itemReportsMenu.Items.Add(item);

      item = new MenuItem();
      item.Header = "Создать индивидуальный план"; item.Name = "iIndivReport";
      item.Click += new RoutedEventHandler(MenuItemReport_Click);
      itemReportsMenu.Items.Add(item);

      item = new MenuItem();
      item.Header = "Создать индивидуальный отчёт на текущий период"; item.Name = "iIndivReport";
      item.Click += new RoutedEventHandler(MenuItemReport_Click);
      itemReportsMenu.Items.Add(item);

      //itemBSCtree.ContextMenu = itemBSCtreeMenu;
      itemReports.ContextMenu = itemReportsMenu;
    }

    void MenuItemReport_Click(object sender, RoutedEventArgs e)
    {
      MenuItem menu_item = (MenuItem)sender;
      if (UserInfo.Info.id_form != UserInfo.id_head_all_department)
      {
        //MessageBox.Show("У Вас нет права работать с планом (отчётом) на данном уровне");
        //return;
      }
      try
      {
        //      reportTree.MakePlanReport(menu_item.Name.Substring(1), (TreeViewItem)itemReports.Items[0]);
        reportTree.MakePlanReport(menu_item.Name.Substring(1), null);
      }
      catch (Exception exc)
      { MessageBox.Show(exc.Message); }
    }
    void MenuItem_Click(object sender, RoutedEventArgs e)
    {
      //добавляется только корень
      MenuItem menu_item = (MenuItem)sender;
      bSCtree.MenuActions(menu_item);
    }

    #region BSCTree
    public void ChangePerspective(string idSCPersp)
    {
      bSCtree.ChangePerspective(idSCPersp);
    }
    #endregion
    private void OnBSCTreeItem_Selected(object sender, BSCtree.SelectedItemEventArgs args)
    {
      if (bSCtree_SelectedItem != null)
        bSCtree_SelectedItem(sender, args);
    }
    private void OnStaffTreeItem_Selected(object sender, Modules.StaffTree.SelectedItemEventArgs args)
    {
      if (staffTree_SelectedStaff != null) staffTree_SelectedStaff(sender, args);
    }

    private void tView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      //передаём событие в главный модуль
      //     if(e.NewValue.GetType() == typeof(System.String)) return;

      TreeViewItem item = (TreeViewItem)e.NewValue;
      if (item == null) return;
      if (item.Name == "itemBSCtree")
      {
        SelectPanel(SelectedItemEventArgs.isStrategicSelected);
      }
      if (item.Name == "")
      {
        SelectPanel(SelectedItemEventArgs.isStrategicSelected);
      }
      if (item.Name == "itemStaffTree")
      {
        //SelectedItemEventArgs args = new SelectedItemEventArgs();
        //args.typeItem = SelectedItemEventArgs.isStaffSelected;
        //args.obj = null;
        //OnSelectedItem(args);
      }
      //item.IsSelected = false;

    }

    #region Events
    public class SelectedItemEventArgs : EventArgs
    {
      public static int isStrategicSelected = 1;
      public static int isBSCPropertiesSelected = 2;
      public static int isPKMSelected = 3;
      public static int isPKMDeSelected = 4;
      public static int isStaffSelected = 5;
      public static int isSelectSCForUser = 6;

      public object obj;
      public int typeItem;
      public bool isEdit;
    }
    public delegate void SelectedItemEventHandler(Object sender, SelectedItemEventArgs args);
    public event SelectedItemEventHandler SelectedItem;
    protected virtual void OnSelectedItem(SelectedItemEventArgs args)
    {
      if (SelectedItem != null)
      {
        SelectedItem(this, args);
      }
    }
    #endregion Events

    void SelectPanel(int TypePanel)
    {
      SelectedItemEventArgs args = new SelectedItemEventArgs();
      args.typeItem = TypePanel;
      args.obj = null;
      OnSelectedItem(args);
    }
    private void b_itemStaffTreeEdit_Checked(object sender, RoutedEventArgs e)
    {
      //if (!IsLoaded) return;
      //Window dlg = new P_DepEdit(_mySql);
      //dlg.ShowDialog();
      //staffTree.Set(_mySql);
    }
    private void b_itemStaffTreeEdit_Unchecked(object sender, RoutedEventArgs e)
    {
      if (!IsLoaded) return;
      b_itemStaffTreeEdit.ToolTip = "Редактирование структуры";
    }

    private void b_BSCTreeEdit_Checked(object sender, RoutedEventArgs e)
    {
      if (!IsLoaded) return;
      b_BSCTreeEdit.ToolTip = "Прекратить редактирование";
      bSCtree.ChangeMode(true);
      ((TreeViewItem)tView.Items[1]).ContextMenu = itemBSCtreeMenu;
      SelectPanel(SelectedItemEventArgs.isBSCPropertiesSelected);

      if (bSCtree.SelectedBSC != null)
      {
        BSCtree.SelectedItemEventArgs args = new BSCtree.SelectedItemEventArgs() { isEdit = true, id_SC = bSCtree.SelectedBSC.idSC.ToString(), id_SC_root = bSCtree.SelectedBSC.idSCParent.ToString() };
        if (bSCtree_SelectedItem != null) bSCtree_SelectedItem(sender, args);
      }
    }
    private void b_BSCTreeEdit_Unchecked(object sender, RoutedEventArgs e)
    {
      if (!IsLoaded) return;
      b_BSCTreeEdit.ToolTip = "Редактирование";
      bSCtree.ChangeMode(false);
      ((TreeViewItem)tView.Items[1]).ContextMenu = null;
      SelectPanel(SelectedItemEventArgs.isStrategicSelected);
    }
    private void b_PKMMode_Click(object sender, RoutedEventArgs e)
    {
      if (UserInfo.Info.id_form == UserInfo.id_head_all_department)
      {
        MessageBox.Show("На верхнем уровне ПКМ не создаются");
        return;
      }
      dlgCreatePKM dlg = new dlgCreatePKM(_mySql, true, "-1");
      dlg.ShowDialog();
    }
    private void b_itemStaffTreeEdit_Click(object sender, RoutedEventArgs e)
    {
      if (!IsLoaded) return;
      //Window dlg = new P_DepEdit(_mySql);
      //dlg.ShowDialog();
      //staffTree.Set(_mySql);
    }
    private void Image_ImageFailed(object sender, ExceptionRoutedEventArgs e)
    {

    }

    public void AproveSC(bool isApproved, string idSC)
    {
      bSCtree.AproveSC(isApproved, idSC);
    }
    public void ChangeNameSC(string newName, string idSC)
    {
      bSCtree.ChangeNameSC(newName, idSC);
    }

    private void itemStaffTree_Selected(object sender, RoutedEventArgs e)
    {
      //сделать проверку типа!!!
      try
      {
        ((TreeViewItem)e.Source).IsSelected = false;
      }
      catch { }
    }
    private void itemBSCtree_Selected(object sender, RoutedEventArgs e)
    {
      //сделать проверку типа!!!
      try
      {
        ((TreeViewItem)e.Source).IsSelected = false;
      }
      catch { }
    }
    private void itemReports_Selected(object sender, RoutedEventArgs e)
    {
      //сделать проверку типа!!!
      try
      {
        ((TreeViewItem)e.Source).IsSelected = false;
      }
      catch { }

    }

    private void chb_OnlyUserSC_Checked(object sender, RoutedEventArgs e)
    {
      UserInfo.isOnlyUserSC = true;
      bSCtree.SetOnlyUserSC();
    }

    private void chb_OnlyUserSC_Unchecked(object sender, RoutedEventArgs e)
    {
      UserInfo.isOnlyUserSC = false;
      bSCtree.SetOnlyUserSC();
    }

  }


}
