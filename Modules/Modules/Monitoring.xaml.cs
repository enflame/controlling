﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

using System.Windows.Controls.DataVisualization.Charting;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для Monitoring.xaml
  /// </summary>
  public partial class Monitoring : UserControl
  {
    string _name;
    string _idSC_root;
    //string _idSC_parent;

    CMySql _mySql;

    //PointsDataCollection periodPast;
    //PointsDataCollection periodNow;
    //PointsDataCollection periodFuture;
    List<PointData> periodPast;
    List<PointData> periodNowMonth;
    List<PointData> periodNowKvartal;
    List<PointData> periodFuture;
    public Monitoring()
    {
      InitializeComponent();
      //periodFuture = new PointsDataCollection();
      //periodNow = new PointsDataCollection();
      //periodPast = new PointsDataCollection();

      periodFuture = new List<PointData>();
      periodNowMonth = new List<PointData>();
      periodNowKvartal = new List<PointData>();
      periodPast = new List<PointData>();


      string ts = new TimeSpan(20, 5, 0).ToString();
      ts = DateTime.Now.ToString();

      //Control series = new LineSeries{IndependentValuePath = "X", DependentValuePath = "Y" };
      //DependencyProperty itemsSourceProperty = LineSeries.ItemsSourceProperty;
      //DependencyProperty dataPointStyleProperty = LineSeries.DataPointStyleProperty;
      //DependencyProperty transitionDurationProperty = LineSeries.TransitionDurationProperty;
      //DependencyProperty independentAxisProperty = LineSeries.IndependentAxisProperty;
      //DependencyProperty dependentRangeAxisProperty = LineSeries.DependentRangeAxisProperty;

      ////series.SetValue(dataPointStyleProperty, (Style)Resources["SimplifiedTemplate"]);


      //ObservableCollection<PointData> points = new ObservableCollection<PointData>();
      ////List<PointData> points = new List<PointData>();
      //series.SetValue(itemsSourceProperty, points);

      //double x = 0;

      //while (x < 4)
      //{
      //  points.Add(new PointData { X = x, Y = Math.Sin(x) });
      //  x += 0.1;
      //}
      //graph.Series.Add((ISeries)series);


    }
    //public void Set(DataTable dt_SC_Period, string name)
    //{
    //  _dt_SC_Period = dt_SC_Period;
    //  _name = name;

    //  BarSeries bar = (BarSeries)graph_bar.Series[0];
    //  bar.ItemsSource = dt_SC_Period.DefaultView;
    //  bar = (BarSeries)graph_bar.Series[1];
    //  bar.ItemsSource = dt_SC_Period.DefaultView;

    //  ColumnSeries pie = (ColumnSeries)graph_gist.Series[0];
    //  pie.ItemsSource = dt_SC_Period.DefaultView;
    //  pie = (ColumnSeries)graph_gist.Series[1];
    //  pie.ItemsSource = dt_SC_Period.DefaultView;

    //  LineSeries line = (LineSeries)graph_line.Series[0];
    //  line.ItemsSource = dt_SC_Period.DefaultView;
    //  line = (LineSeries)graph_line.Series[1];
    //  line.ItemsSource = dt_SC_Period.DefaultView;

    //  tb_name.Text = name;

    //  graph_bar.Width = graph_bar.Height;
    //  graph_line.Width = graph_bar.Height;
    //}

    void UpdatePNF()
    {
      periodFuture.Clear();
      periodNowMonth.Clear();
      periodNowKvartal.Clear();
      periodPast.Clear();
      string sql = "select * from SC_Period where idSC=" + _idSC_root + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt_SC_Period = _mySql.SqlSelect(sql);
      if (dt_SC_Period.Rows.Count == 0) return;

      DateTime dtNow1 = DateTime.Parse("31.01." + DateTime.Now.Year.ToString());
      DateTime dtNow2 = DateTime.Parse("31.01." + (DateTime.Now.Year + 1).ToString());

      int yearCurr = DateTime.Parse(dt_SC_Period.Rows[0]["DateEnd"].ToString()).Year;

      for (int i = 0; i < dt_SC_Period.Rows.Count; i++)
      {
        DataRow dr = dt_SC_Period.Rows[i];

        DateTime dt = DateTime.Parse(dr["DateEnd"].ToString());
        if (dt <= dtNow1)
          if (yearCurr < dt.Year)
          {
            DataRow dr1 = dt_SC_Period.Rows[i - 1];
            periodPast.Add(new PointData() { DateEnd = PointData.ToYear(dr1["DateEnd"].ToString()), plan = Convert.ToDouble(dr1["plan"]), fact = Convert.ToDouble(dr1["fact"]) });
            yearCurr = dt.Year;
          }
        if ((dt >= dtNow1) & (dt < dtNow2))
        {
          periodNowMonth.Add(new PointData() { DateEnd = PointData.ToMonth(dr["DateEnd"].ToString()), plan = Convert.ToDouble(dr["plan"]), fact = Convert.ToDouble(dr["fact"]) });
          if (yearCurr == dt.Year)
            yearCurr = dt.Year + 1;
        }
        if (dt > dtNow2)
          if (yearCurr < dt.Year)
          {
            DataRow dr1 = dt_SC_Period.Rows[i - 1];
            periodFuture.Add(new PointData() { DateEnd = PointData.ToYear(dr1["DateEnd"].ToString()), plan = Convert.ToDouble(dr1["plan"]), fact = Convert.ToDouble(dr1["fact"]) });
            yearCurr = dt.Year;
          }
      }
      DataRow dr2 = dt_SC_Period.Rows[dt_SC_Period.Rows.Count - 1];
      periodFuture.Add(new PointData() { DateEnd = PointData.ToYear(dr2["DateEnd"].ToString()), plan = Convert.ToDouble(dr2["plan"]), fact = Convert.ToDouble(dr2["fact"]) });


      //graph_past.DataContext = periodPast;
      //graph_future.DataContext = periodFuture;
      //graph_now.DataContext = periodNow;

      ColumnSeries bar = (ColumnSeries)graph_past.Series[0];
      bar.ItemsSource = null; bar.ItemsSource = periodPast;
      bar = (ColumnSeries)graph_past.Series[1];
      bar.ItemsSource = null; bar.ItemsSource = periodPast;


      bar = (ColumnSeries)graph_now.Series[0];
      bar.ItemsSource = null; bar.ItemsSource = periodNowMonth;
      bar = (ColumnSeries)graph_now.Series[1];
      bar.ItemsSource = null; bar.ItemsSource = periodNowMonth;

      bar = (ColumnSeries)graph_future.Series[0];
      bar.ItemsSource = null; bar.ItemsSource = periodFuture;
      bar = (ColumnSeries)graph_future.Series[1];
      bar.ItemsSource = null; bar.ItemsSource = periodFuture;
    } 
    void RenewPNF()
    {
      periodFuture.Clear();
      periodNowMonth.Clear();
      periodNowKvartal.Clear();
      periodPast.Clear();

      string sql = "select MAX(plan) AS plan1, MAX(fact) AS fact1 from SC_Period where DateEnd <= " + _mySql.ToDateTime("01.01." + DateTime.Now.Year.ToString()) + " and idSC=" + _idSC_root + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt_SC_Period = _mySql.SqlSelect(sql);
      for (int i = 0; i < dt_SC_Period.Rows.Count; i++)
      {
        DataRow dr = dt_SC_Period.Rows[i];
        periodPast.Add(new PointData() { DateEnd = (DateTime.Now.Year - 1).ToString(), plan = Convert.ToDouble(dr["plan1"]), fact = Convert.ToDouble(dr["fact1"]) });
      }
      //ColumnSeries bar = (ColumnSeries)graph_past.Series[0];
      //bar.ItemsSource = dt_SC_Period.DefaultView;
      //bar = (ColumnSeries)graph_past.Series[1];
      //bar.ItemsSource = dt_SC_Period.DefaultView;

      sql = "select * from SC_Period where (DateEnd >= " + _mySql.ToDateTime("01.01." + DateTime.Now.Year.ToString()) +
        " and  DateEnd <=" + _mySql.ToDateTime("01 .12." + DateTime.Now.Year.ToString()) + ") and idSC=" + _idSC_root + " and targetyear = " + UserInfo.TargetYear.ToString();
      dt_SC_Period = _mySql.SqlSelect(sql);
      for (int i = 0; i < dt_SC_Period.Rows.Count; i++)
      {
        DataRow dr = dt_SC_Period.Rows[i];
        periodNowMonth.Add(new PointData() { DateEnd = PointData.ToYear(dr["DateEnd"].ToString()), plan = Convert.ToDouble(dr["plan"]), fact = Convert.ToDouble(dr["fact"]) });
      }
      //bar = (ColumnSeries)graph_now.Series[0];
      //bar.ItemsSource = dt_SC_Period.DefaultView;
      //bar = (ColumnSeries)graph_now.Series[1];
      //bar.ItemsSource = dt_SC_Period.DefaultView;

      sql = "select * from SC_Period where DateEnd >= " + _mySql.ToDateTime("01.01." + (DateTime.Now.Year + 1).ToString()) + " and idSC=" + _idSC_root + " and targetyear = " + UserInfo.TargetYear.ToString();
      dt_SC_Period = _mySql.SqlSelect(sql);
      for (int i = 0; i < dt_SC_Period.Rows.Count; i++)
      {
        DataRow dr = dt_SC_Period.Rows[i];
        periodFuture.Add(new PointData() { DateEnd = PointData.ToYear(dr["DateEnd"].ToString()), plan = Convert.ToDouble(dr["plan"]), fact = Convert.ToDouble(dr["fact"]) });
      }
      //bar = (ColumnSeries)graph_future.Series[0];
      //bar.ItemsSource = dt_SC_Period.DefaultView;
      //bar = (ColumnSeries)graph_future.Series[1];
      //bar.ItemsSource = dt_SC_Period.DefaultView;


      ColumnSeries bar = (ColumnSeries)graph_past.Series[0];
      bar.ItemsSource = null; bar.ItemsSource = periodPast;
      bar = (ColumnSeries)graph_past.Series[1];
      bar.ItemsSource = null; bar.ItemsSource = periodPast;


      bar = (ColumnSeries)graph_now.Series[0];
      bar.ItemsSource = null; bar.ItemsSource = periodNowMonth;
      bar = (ColumnSeries)graph_now.Series[1];
      bar.ItemsSource = null; bar.ItemsSource = periodNowMonth;

      bar = (ColumnSeries)graph_future.Series[0];
      bar.ItemsSource = null; bar.ItemsSource = periodFuture;
      bar = (ColumnSeries)graph_future.Series[1];
      bar.ItemsSource = null; bar.ItemsSource = periodFuture;
    }
    void Renew(string idSC, Chart chart)
    {
      string sql = "select * from SC_Period where idSC=" + idSC + " and targetyear = " + UserInfo.TargetYear.ToString() + " order by id";
      DataTable dt_SC_Period = _mySql.SqlSelect(sql);
      List<PointData> period = new List<PointData>();

      for (int i = 0; i < dt_SC_Period.Rows.Count; i++)
      {
        DataRow dr = dt_SC_Period.Rows[i];

        DateTime dt = DateTime.Parse(dr["DateEnd"].ToString());
        period.Add(new PointData() { DateEnd = dr["period"].ToString(), plan = Convert.ToDouble(dr["plan"]), fact = Convert.ToDouble(dr["fact"]) });
      }
      SetGraph(period, chart);
    }
    void SetGraph(List<PointData> period, Chart chart)
    {
      if (!IsLoaded) return;
      chart.Series.Clear();

      ResourceDictionary resourceDictionary = new ResourceDictionary();
      resourceDictionary.Source = new Uri("/Modules;component/Styles/ChartDictionary.xaml", UriKind.Relative);

      if ((bool)ch_isLine.IsChecked)
      {
        LineSeries plan = new LineSeries { IndependentValuePath = "DateEnd", DependentValuePath = "plan" };
        LineSeries fact = new LineSeries { IndependentValuePath = "DateEnd", DependentValuePath = "fact" };

        plan.ItemsSource = period;
        fact.ItemsSource = period;

        chart.Series.Add(plan);
        chart.Series.Add(fact);
      }
      if ((bool)ch_isGist.IsChecked)
      {
        ColumnSeries plan = new ColumnSeries { IndependentValuePath = "DateEnd", DependentValuePath = "plan" };
        ColumnSeries fact = new ColumnSeries { IndependentValuePath = "DateEnd", DependentValuePath = "fact" };

        plan.SetValue(LineSeries.DataPointStyleProperty, (Style)resourceDictionary["GistDataPointPlan"]);
        fact.SetValue(LineSeries.DataPointStyleProperty, (Style)resourceDictionary["GistDataPointFact"]);


        plan.Background = Brushes.Green;
        fact.Background = Brushes.Red;

        plan.ItemsSource = period;
        fact.ItemsSource = period;
        chart.Series.Add(plan);
        chart.Series.Add(fact);

        tb_plan_color.Style = (Style)resourceDictionary["tbPlanStyle"]; ;
        tb_fact_color.Style = (Style)resourceDictionary["tbFactStyle"]; ;
      }

    }

    public void Set(CMySql mySql, string idSC_root, string name)
    {
      ResourceDictionary resourceDictionary = new ResourceDictionary();
      resourceDictionary.Source = new Uri("/Modules;component/Styles/ChartDictionary.xaml", UriKind.Relative);

      graph_line.LegendStyle = (Style)resourceDictionary["LegendStyle"];
      graph_line.TitleStyle = (Style)resourceDictionary["TitleStyle"];

      _mySql = mySql;

      string sql = "select * from SCTree where id_parent=" + idSC_root + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt = _mySql.SqlSelect(sql);

      //графики показателей следующего уровня
      for (int i = 0; i < dt.Rows.Count; i++)
      {
        DataRow dr = dt.Rows[i];
        sql = "select name1, targetvalue from SC where id=" + dr["id_SC"].ToString() + " and targetyear = " + UserInfo.TargetYear.ToString();
        DataTable dt_SC = _mySql.SqlSelect(sql);

        if (sp_graph.Children.Count < i + 3)
        {
          Chart chart = new Chart(); chart.Margin = new Thickness(1, 1, 1, 3);
          sp_graph.Children.Add(chart);
          chart.LegendStyle = (Style)resourceDictionary["LegendStyle"];
          chart.TitleStyle = (Style)resourceDictionary["TitleStyle"];
          chart.Width = graph_line.Width;
        }
        sp_graph.Children[i + 2].Visibility = System.Windows.Visibility.Visible;
        ((Chart)sp_graph.Children[i + 2]).Title = dt_SC.Rows[0]["name1"].ToString() + " ц.зн.=" + dt_SC.Rows[0]["targetvalue"].ToString();
        Renew(dr["id_SC"].ToString(), (Chart)sp_graph.Children[i + 2]);
      }
      for (int i = dt.Rows.Count + 2; i < sp_graph.Children.Count; i++)
      {
        sp_graph.Children[i].Visibility = System.Windows.Visibility.Collapsed;
      }
      _name = name;
      _mySql = mySql;
      _idSC_root = idSC_root;

      //UpdatePNF();
      graph_line.Title = name;
      Renew(idSC_root, graph_line);

      //    tb_name.Text = _name;
    }

    private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
    {
      //graph_bar.Width = sp_graph.Width / 2;
      //graph_line.Width = sp_graph.Width / 2;

      //graph_bar.Width = sp_graph.Height;
      //graph_line.Width = sp_graph.Height;
      //graph_bar.Height = graph_bar.Width;
      //graph_line.Height = graph_line.Width;
    }

    private void ch_isGist_Checked(object sender, RoutedEventArgs e)
    {
      ChangeGraphType();
    }
    private void ch_isBar_Checked(object sender, RoutedEventArgs e)
    {
      //    SetGraph();
    }
    private void ch_isLine_Checked(object sender, RoutedEventArgs e)
    {
      ChangeGraphType();
     }

    void ChangeGraphType()
    {
      if (!IsLoaded) return;
      for (int i = 0; i < sp_graph.Children.Count; i++)
        if (sp_graph.Children[i].GetType() == typeof(Chart))
        {
          Chart chart = (Chart)sp_graph.Children[i];
          List<PointData> period = null;
          if ((bool)ch_isLine.IsChecked) period = (List<PointData>)((ColumnSeries)chart.Series[0]).ItemsSource;
          if ((bool)ch_isGist.IsChecked) period = (List<PointData>)((LineSeries)chart.Series[0]).ItemsSource;
          SetGraph(period, chart);
          Type t = sp_graph.Children[i].GetType();
        }
    }
  }

  public class PointsDataCollection : Collection<PointData>
  {

  }
  public class PointData
  {
    public static string ToYear(string s_dt)
    {
      DateTime dt = DateTime.Parse(s_dt);
      return dt.Year.ToString();
    }
    public static string ToMonth(string s_dt)
    {
      DateTime dt = DateTime.Parse(s_dt);
      switch (dt.Month)
      {
        case 1:
          return "янв.";
        case 2:
          return "февр.";
        case 3:
          return "март";
        case 4:
          return "апр.";
        case 5:
          return "май";
        case 6:
          return "июнь";
        case 7:
          return "июль";
        case 8:
          return "авг.";
        case 9:
          return "сент.";
        case 10:
          return "окт.";
        case 11:
          return "ноябрь.";
        case 12:
          return "дек.";
      }

      return dt.Month.ToString() + "." + dt.Year.ToString();
    }

    public string DateEnd { get; set; }
    public double plan { get; set; }
    public double fact { get; set; }

  }
}
