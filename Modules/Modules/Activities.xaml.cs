﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для Activities.xaml
  /// </summary>
  public partial class Activities : UserControl
  {
    DataTable dt_SC_Actions;
    CMySql _mySql;

    DataRow _dr_parentSC;
    DataRow _dr_SC;
    string _idSC;
    bool _isPKM;

    DataTable _dt_SC_PKM;
    DataRow _dr_SC_PKM;
    public Activities()
    {
      InitializeComponent();

      cb_ReportPeriod.Items.Clear();
      //foreach (string s in Period.Q)
      //{
      //  ComboBoxItem cbi = new ComboBoxItem();
      //  cbi.Content = s;
      //  cb_ReportPeriod.Items.Add(cbi);
      //}
    }
    ComboBoxItem SelectComboBoxItem(ComboBox cb, string item)
    {
      foreach (ComboBoxItem cbi in cb.Items)
      {
        if (((string)cbi.Content) == item) return cbi;
      }
      return null;
    }
    public void Save()
    {
      foreach (DataRow dr in dt_SC_Actions.Rows)
      {
        string sql = "update SC_Actions set name1='" + dr["name1"].ToString() + "', task='" + dr["task"].ToString() +
          "', begindate=" + _mySql.DateToString((DateTime)dr["BeginDate"]) + ", enddate=" + _mySql.DateToString((DateTime)dr["EndDate"]) + " where id=" + dr["id"].ToString();
        _mySql.SqlExec(sql);
      }

    }

    public void Set(DataRow dr_SC, CMySql mySql, bool isPKM)
    {
      b_Ready.Visibility = System.Windows.Visibility.Collapsed;
      _mySql = mySql;

      _isPKM = isPKM;
      _dr_SC = dr_SC;
      _idSC = _dr_SC["id"].ToString();

      if (_isPKM)
      {
        string sql = "select PKM.ID as id_pkm, P.ID as id_period, PKM.ISAPPROVED, pkm.ISFORAPPROVED, P.PERIOD  from SC_PKM pkm " +
        "left join SC_Period P on P.ID = PKM.ID_SC_PERIOD  " +
        "where pkm.idSC = " + _idSC + " and pkm.id_form_creator=" + _dr_SC["IDCONTROLPERSON"].ToString() + " and pkm.targetyear = " + UserInfo.TargetYear.ToString();
        _dt_SC_PKM = _mySql.SqlSelect(sql);
        if (_dt_SC_PKM.Rows.Count == 0)
        {
          sql = "select * from SC_Period act where act.idSC = " + _idSC;
          DataTable dt_Period = _mySql.SqlSelect(sql);
          foreach (DataRow dr in dt_Period.Rows)
          {
            sql = "insert into SC_PKM (idSC, id_SC_Period, id_form_creator, TargetYear) values (" + _idSC + ", " +
              dr["id"] + ", " + _dr_SC["IDcontrolPerson"].ToString() + ", " + UserInfo.TargetYear + ")";
            _mySql.SqlExec(sql);
          }
          Set(dr_SC, mySql, isPKM);
        }

        cb_ReportPeriod.ItemsSource = _dt_SC_PKM.DefaultView;
        cb_ReportPeriod.SelectedIndex = 0;


        _dr_parentSC = null;
        sql = "SELECT SC.IDcontrolPerson, SC.IsApproved FROM SC INNER JOIN SCTree ON SC.id = SCTree.id_parent where SCTree.id_SC=" + _dr_SC["id"].ToString() +
          " and SC.targetyear = " + UserInfo.TargetYear.ToString(); ;
        DataTable dt = _mySql.SqlSelect(sql);
        b_approve_pkm.Visibility = System.Windows.Visibility.Collapsed;
        if (dt.Rows.Count > 0)
        {//для руководителя
          _dr_parentSC = dt.Rows[0];
          if (_dr_parentSC["IDcontrolPerson"].ToString() == UserInfo.Info.id_form)
          {
            sp_PKM.Visibility = System.Windows.Visibility.Visible;
            sp_PKM_properties.Visibility = System.Windows.Visibility.Visible;
            sp_PKM_actions.Visibility = System.Windows.Visibility.Collapsed;
            b_approve_pkm.Visibility = System.Windows.Visibility.Visible;
            sp_share.Visibility = System.Windows.Visibility.Collapsed;
          }
        }
        else
        {
          sp_PKM.Visibility = System.Windows.Visibility.Visible;
          sp_PKM_properties.Visibility = System.Windows.Visibility.Visible;
          b_approve_pkm.Visibility = System.Windows.Visibility.Collapsed;
          sp_PKM_actions.Visibility = System.Windows.Visibility.Visible;
          sp_share.Visibility = System.Windows.Visibility.Visible;

          cb_ReportPeriod.SelectedItem = SelectComboBoxItem(cb_ReportPeriod, Period.GetReportPeriod(DateTime.Now, Period.Quarter));
        }
      }
      else
      {
        sp_PKM.Visibility = System.Windows.Visibility.Collapsed;

        if (_dr_SC["IDcontrolPerson"].ToString() != UserInfo.Info.id_form) sp_share.Visibility = System.Windows.Visibility.Collapsed;
        else sp_share.Visibility = System.Windows.Visibility.Visible;
      }


      if (UserInfo.Info.id_form == "-1") RenewActions();
      else RenewActions();

    }
    void RenewActions()
    {
      string Period = cb_ReportPeriod.Text;
      string sql;
      if (_isPKM)
      {
        if (cb_ReportPeriod.SelectedIndex == -1) return;
        _dr_SC_PKM = _dt_SC_PKM.Rows[cb_ReportPeriod.SelectedIndex];
        sql = "select * from SC_Actions where idSC=" + _idSC + " and idPKM = " + _dr_SC_PKM["id_pkm"].ToString() + " and targetyear = " + UserInfo.TargetYear.ToString(); ;

        sp_PKM.DataContext = _dr_SC_PKM;
      }
      else sql = "select SC_Actions.*, U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO as FIO_RESP " +
       "from SC_Actions " +
       "left join app_portal.v_tpu_users U on U.LICHNOST_ID = SC_Actions.IDRESPONSIBLEPERSON " +
       "where idSC=" + _idSC + " and idPKM = -1 and targetyear = " + UserInfo.TargetYear.ToString(); ;

      dt_SC_Actions = _mySql.SqlSelect(sql, "SC_Actions");


      dt_SC_Actions.Columns.Add("isDeleteVisible", Type.GetType("System.Boolean"));
      bool isDeleteVisible = true;
      if (_dr_SC["IDcontrolPerson"].ToString() != UserInfo.Info.id_form)
      {
        isDeleteVisible = false;
        b_NewAction.IsEnabled = false;
      }
      else
      {
        isDeleteVisible = true;
        b_NewAction.IsEnabled = true;
      }

      if (_isPKM)
      {

        if ((_dr_SC_PKM["IsApproved"].ToString() == "1") | (_dr_SC_PKM["IsForApproved"].ToString() == "1"))
        {
          isDeleteVisible = false;
          b_NewAction.Visibility = System.Windows.Visibility.Collapsed;
        }
        else
        {
          b_NewAction.Visibility = System.Windows.Visibility.Visible;
        }

      }
      else
      {

        if (_dr_SC["IsApproved"].ToString() == "1")
        {
          isDeleteVisible = false;
          b_NewAction.Visibility = System.Windows.Visibility.Collapsed;
        }
        else
        {
          b_NewAction.Visibility = System.Windows.Visibility.Visible;
        }

      }
      foreach (DataRow dr in dt_SC_Actions.Rows) dr["isDeleteVisible"] = isDeleteVisible;


      dg_Actions.ItemsSource = dt_SC_Actions.DefaultView;
      grid_Actions.DataContext = dt_SC_Actions;

    }
    void NewAction()
    {
      DataRow dr = dt_SC_Actions.NewRow();
      dr["name1"] = "";
      dr["idPKM"] = -1;
      if (_isPKM) dr["idPKM"] = _dr_SC_PKM["id_pkm"].ToString();

      dr["isReady"] = false;
      dr["isChecked"] = false;
      dr["idFileTask"] = -1;
      dr["idFileReadyReport"] = -1;
      dr["LastUpdatedDate"] = UserInfo.ServerDate;
      dr["BeginDate"] = new DateTime(Convert.ToInt32(UserInfo.TargetYear), 1, 1);
      dr["EndDate"] = new DateTime(Convert.ToInt32(UserInfo.TargetYear), 12, 31);
      dt_SC_Actions.Rows.Add(dr);
      dr["IDRESPONSIBLEPERSON"] = _dr_SC["IDcontrolPerson"];

      dr["isDeleteVisible"] = true;

      //dr["isReadyEnabled"] = false;
      //dr["isCheckedEnabled"] = false;
      //if (UserInfo.id_form == _dr_SC["IDControlPerson"].ToString()) dr["isCheckedEnabled"] = true;

      dlgActionKPI dlg = new dlgActionKPI(((DataRowView)dg_Actions.Items[dg_Actions.Items.Count - 1]));
      if ((bool)dlg.ShowDialog())
      {
        string sql = "insert into SC_Actions (idSC, name1, task, BeginDate, EndDate, idFileTask, idFileReadyReport, ReadyReport, LastUpdatedDate, IDOwnPerson, idPKM, TargetYear, IDRESPONSIBLEPERSON) values (" +
         _idSC + ", '" + dr["name1"].ToString() + "', '" + dr["task"].ToString() + "', " + _mySql.DateToString((DateTime)dr["BeginDate"]) + ", " + _mySql.DateToString((DateTime)dr["EndDate"]) + ", " +
         dr["idFileTask"].ToString() + ", " + dr["idFileReadyReport"].ToString() + ", '" + dr["ReadyReport"].ToString() +
         "', " + _mySql.DateToString(DateTime.Now) + ", " + UserInfo.Info.id_form + ", " + dr["idPKM"].ToString() + ", " + UserInfo.TargetYear + ", " + dr["IDRESPONSIBLEPERSON"].ToString() + ")";
        _mySql.SqlExec(sql);
        RenewActions();
      }
      else dt_SC_Actions.Rows.Remove(dr);
    }
    private void b_NewAction_Click(object sender, RoutedEventArgs e)
    {
      NewAction();
    }
    private void b_Action_Click(object sender, RoutedEventArgs e)
    {
      //Button b = (Button)sender;
      //DataRowView dr = (DataRowView)dg_Actions.SelectedItem;
      //if (b.Name == "b_ViewAction")
      //{
      //  dlgActionKPI dlg = new dlgActionKPI(dr);
      //  dlg.ShowDialog();
      //}
      //if (b.Name == "b_isReadyAction")
      //{
      //  dr["isReady"] = true;
      //  string sql = "update SC_Actions set isReady=true, ReadyDate='" + DateTime.Now.ToShortDateString() + "' where id = " + dr["id"].ToString();
      //  _mySql.SqlExec(sql);

      //  string mess = "Мероприятие " + dr["name1"].ToString() + " выполнено";
      //  TypeMessages.Insert(_dr_parentSC["IDControlPerson"].ToString(), UserInfo.id_form, _idSC, TypeMessages.FromActions, dr["id"].ToString(), mess, _mySql);
      //}
      //if (b.Name == "b_isCheckedAction")
      //{
      //  dr["isReady"] = true;
      //  string sql = "update SC_Actions set isChecked=true, CheckedDate='" + DateTime.Now.ToShortDateString() + "' where id = " + dr["id"].ToString();
      //  _mySql.SqlExec(sql);

      //  string mess = "Выполнение мероприятия " + dr["name1"].ToString() + " принято";
      //  TypeMessages.Insert(_dr_parentSC["IDControlPerson"].ToString(), UserInfo.id_form, _idSC, TypeMessages.FromActions, dr["id"].ToString(), mess, _mySql);

      //}
      ////

    }

    private void b_Close_Click(object sender, RoutedEventArgs e)
    {

    }

    private void cb_ReportPeriod_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!IsLoaded) return;
      if (e.AddedItems.Count == 0) return;
      //      DataRow dr = ((DataRowView)e.AddedItems[0]).Row;
      //     string p = (string)((DataView)e.AddedItems[0]).Content;
      //RenewActions(p, cb_ReportYear.Text);
      RenewActions();
    }

    private void cb_ReportYear_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!IsLoaded) return;
      string p = (string)((ComboBoxItem)e.AddedItems[0]).Content;
      //RenewActions(cb_ReportPeriod.Text, p);
    }

    private void b_sendForApproved_Click(object sender, RoutedEventArgs e)
    {
      if (_dr_parentSC == null)
      {
        MessageBox.Show("Для стратегической задачи ПКМ не формируется");
        return;
      }
      string sql = "update SC_PKM set isForApproved = 1 where id=" + _dr_SC_PKM["id_pkm"].ToString();
      _mySql.SqlExec(sql);
      _dr_SC_PKM["ISFORAPPROVED"] = 1;
      b_NewAction.Visibility = System.Windows.Visibility.Collapsed;

      TypeMessages.Insert(_dr_parentSC["IDControlPerson"].ToString(), UserInfo.Info.id_form, _dr_SC_PKM["id_pkm"].ToString(),
        TypeMessages.SendPKMForApproved, _dr_SC_PKM["id_pkm"].ToString(), UserInfo.Info.id_form, "Необходимо утвердить ПКМ", _mySql);

      ActivitiesEventArgs args = new ActivitiesEventArgs() { TypeEvent = TypeMessages.SendPKMForApproved, obj = null };
      OnActivitiesEvent(args);
    }

    #region Events
    public class ActivitiesEventArgs : EventArgs
    {
      public int TypeEvent;
      public object obj;
    }
    public delegate void ActivitiesEventHandler(Object sender, ActivitiesEventArgs args);
    public event ActivitiesEventHandler ActivitiesHandler;
    protected virtual void OnActivitiesEvent(ActivitiesEventArgs args)
    {
      if (ActivitiesHandler != null)
      {
        ActivitiesHandler(this, args);
      }
    }
    #endregion Events

    private void b_approve_pkm_Click(object sender, RoutedEventArgs e)
    {
      string sql = "update SC_PKM set isForApproved = 0, isApproved = 1 where id = " + _dr_SC_PKM["id_pkm"].ToString();
      _mySql.SqlExec(sql);
      _dt_SC_PKM.Rows[0]["isForApproved"] = false;
      _dt_SC_PKM.Rows[0]["isApproved"] = true;

      TypeMessages.Insert(_dr_SC["IDControlPerson"].ToString(), UserInfo.Info.id_form, _dr_SC["id"].ToString(), TypeMessages.SendPKMApproved, _dr_SC_PKM["id_pkm"].ToString(), "-1",
        "ПКМ " + _dr_SC["name1"].ToString() + " утверждён", _mySql);

      ActivitiesEventArgs args = new ActivitiesEventArgs() { TypeEvent = -1, obj = null };
      OnActivitiesEvent(args);

    }

    private void b_DeleteAction_Click(object sender, RoutedEventArgs e)
    {
      DataRow dr = ((DataRowView)dg_Actions.SelectedItem).Row;

      if (MessageBox.Show("Удалить \r\n" + dr["name1"].ToString() + "?", "Удаление мероприятия", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        string sql = "delete from SC_Actions where id = " + dr["id"].ToString();
        _mySql.SqlExec(sql);
        dt_SC_Actions.Rows.RemoveAt(dg_Actions.SelectedIndex);
      }
    }

    private void b_selectResponsiblePerson_Click(object sender, RoutedEventArgs e)
    {
      DataRowView dr = dg_Actions.SelectedItem as DataRowView;
      dlgSelectStaff wnd = new dlgSelectStaff(_mySql, new dlgSelectStaff.SelectStaffEventHandler(onSelectedStaff));
      Point p = PointToScreen(Mouse.GetPosition(dg_Actions));
      wnd.Left = p.X;
      wnd.Top = p.Y;

      wnd.Show();
    }

    void onSelectedStaff(Object sender, StaffTree.SelectedItemEventArgs args)
    {
      DataRowView dr = dg_Actions.SelectedItem as DataRowView;
      dr["FIO_RESP"] = args.staffInfo.fio;
      dr["IDresponsiblePerson"] = args.staffInfo.id_form;

      string sql = "update SC_Actions set IDRESPONSIBLEPERSON =" + dr["IDRESPONSIBLEPERSON"].ToString()  + " where id = " + dr["id"].ToString();
      _mySql.SqlExec(sql);
    }

  }
}
