﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
using MySql;
using GeneralClasses;

namespace Modules
{
	/// <summary>
	/// Логика взаимодействия для UserControl1.xaml
	/// полагаем level1 = 0 - Перспектива
	/// полагаем level1 = 10 - Цель
	/// полагаем level1 = 20 - Показатель
	/// </summary>
	public partial class DelegationTree : UserControl
	{
    bool _isFromKPI;
		string _idSC;
		CMySql _mySql;

		TreeViewItem _curr_item;

    public static void InsertDelegateKPItoStaff(CMySql mySql, DelegateUser dr)
    {
      string sql = "insert into staff_KPI (id_dep, id_position, id_form, id_KPI, weight, targetYear) values(" + dr.id_dep +
        ", " + dr.id_position + ", " + dr.ID_FORM + ", " + dr.id_KPI +
        ", " + dr.weight + ", " + UserInfo.TargetYear + ")";
      mySql.SqlExec(sql);
      //string mess = "Делегирован показатель " + dr["kpiName"].ToString();
      //TypeMessages.Insert(dr["id_form"].ToString(), UserInfo.Info.id_form, dr["id_KPI"].ToString(), TypeMessages.FromSC, dr["id_KPI"].ToString(), "-1", mess, mySql);
    }
    public static List<StaffInfo> GetStaff(CMySql mySql, string id_KPI)
    {
      string sql = "select * from staff_KPI where id_KPI = " + id_KPI + " and targetYear=" + UserInfo.TargetYear;
      DataTable dt = mySql.SqlSelect(sql);
      List<StaffInfo> res = new List<StaffInfo>();
      foreach (DataRow dr in dt.Rows)
      {
        StaffInfo si = StaffTree.SelectStaffByID(dr["id_form"].ToString(), mySql);
        si.TargetValue = dr["targetvalue"].ToString();
        si.KPI_ID = id_KPI;
        //si.Unit = dr["unit"].ToString();
        res.Add(si);
      }
      return res;
    }
    public static void UpdateDelegateKPItoStaff(CMySql mySql, DataRow dr)
    {
      string sql = "update staff_KPI set weight=" + dr["weight"].ToString() + ", criterion = '" + dr["criterion"].ToString() + "' where id=" + dr["id"].ToString();
      mySql.SqlExec(sql);

      string mess = "Делегирован показатель " + dr["kpiName"].ToString();
      TypeMessages.Insert(dr["id_form"].ToString(), UserInfo.Info.id_form, dr["id_KPI"].ToString(), TypeMessages.FromSC, dr["id_KPI"].ToString(), "-1", mess, mySql);
    }
    //public static void DelegateKPIStaffToHistory(CMySql mySql, DataRow dr, string reason)
    //{
    //  //string sql = "insert into staff_KPI_history (id_dep, id_position, id_form, " +
    //  //"id_KPI, weight, date_history, reason, id_form_created, id_in_origin) values(" +
    //  //dr["id_dep"].ToString() + ", " + dr["id_position"].ToString() + ", " + dr["id_form"].ToString() + ", " + dr["id_KPI"].ToString() +
    //  //", " + dr["weight"].ToString() + 
    //  //", '" + DateTime.Now.ToShortDateString() + "', '" + reason + "', " + UserInfo.id_form + ", " + dr["id"].ToString() +  ")";
    //  //mySql.SqlExec(sql);

    //  string mess = "Снято делегирование показателя " + dr["kpiName"].ToString();
    //  string sql = "insert into messages (id_form_for, id_form_from, idSC, typeMess, typeID, mess, dateMess) values(" +
    //    dr["id_form"].ToString() + ", " + UserInfo.id_form + ", " + dr["id_KPI"] + ", " + TypeMessages.FromSC.ToString() + ", " + dr["id_KPI"].ToString() +
    //    ",' " + mess + "', '" + DateTime.Now.ToShortDateString() + "')";
    //  mySql.SqlExec(sql);

    //}
		public DelegationTree()
		{
			InitializeComponent();
		}
		public void Set(CMySql mySql, string idSC, bool isFromKPI)
		{
			_mySql = mySql;
			_idSC = idSC;
      _isFromKPI = isFromKPI;
      tView.Items.Clear(); 
      FillScorecardTree(null, _idSC);
		}

		//void FillScorecardBranch(TreeViewItem root, string id_parent_form)
		//{
		//  string sql = "select * from SCTree where id_parent=" + id_root;
		//  DataTable dt = _mySql.SqlSelect(sql);

		//  foreach (DataRow dr in dt.Rows)
		//  {
		//    sql = "select * from SC where id=" + dr["id_SC"].ToString();
		//    DataTable dt_SC = _mySql.SqlSelect(sql);
		//    if (dt_SC.Rows.Count > 0)
		//    {
		//      DataRow dr_SC = dt_SC.Rows[0];//одна строка

		//      TreeViewItem node = new TreeViewItem();
		//      TreeViewItemHeader h = null;
		//      if (((int)dr_SC["Level1"]) == TypeHeader.Goal)
		//      {
		//        h = new TreeViewItemHeader("/Controlling;component/Ico/target_32.png", dr_SC["name1"].ToString(), TypeHeader.Goal, root);
		//      }
		//      if (((int)dr_SC["Level1"]) == TypeHeader.Score)
		//      {
		//        h = new TreeViewItemHeader("/Controlling;component/Ico/target_22.png", dr_SC["name1"].ToString(), TypeHeader.Score, root);
		//      }
		//      //					h.ShowPict(false);

		//      node.Header = h; node.Name = "n" + dr["id_SC"].ToString();
		//      node.Tag = root;
		//      node.MouseLeftButtonUp += new MouseButtonEventHandler(Item_MouseLeftButtonUp);


		//      root.Items.Add(node);

		//      FillScorecardBranch(node, dr["id_SC"].ToString());
		//    }
		//  }
		//}

		void FillKpiRow(DataRow dr)
		{
			string sql = "select * from form where id = " + dr["id_form"];
			dr["fio"] = _mySql.SqlSelect(sql).Rows[0]["fio"];
			sql = "select * from idoTree where id = " + dr["id_dep"];
			dr["dep"] = _mySql.SqlSelect(sql).Rows[0]["nodename"];
		}
		public void FillScorecardTree(TreeViewItem root, string id_parent_form)
		{
      //по сотрудникам относительно показателя
      string sql = "";
      if (root == null) sql = "select * from staff_KPI where id_KPI = " + _idSC + " and id_form_parent = 0" + " and targetyear = " + UserInfo.TargetYear.ToString();
      else sql = "select * from staff_KPI where id_KPI = " + _idSC + " and id_form_parent = " + id_parent_form + " and targetyear = " + UserInfo.TargetYear.ToString();
			DataTable dt = _mySql.SqlSelect(sql);
			dt.Columns.Add(new DataColumn("fio"));
			dt.Columns.Add(new DataColumn("dep"));
			foreach (DataRow dr in dt.Rows)
			{
				FillKpiRow(dr);
				TreeViewItem node = new TreeViewItem();
        KPIRow kpi_row = new KPIRow(); kpi_row.Set(dr, _isFromKPI);
        node.Header = kpi_row; node.Name = "n" + dr["id_form"].ToString();

				if (root == null)
				{
					node.Tag = null;
					tView.Items.Add(node);
				}
				else 
				{
					node.Name = "n" + dr["id_SC"].ToString();
					node.Tag = root;
//					node.MouseLeftButtonUp += new MouseButtonEventHandler(Item_MouseLeftButtonUp);
					root.Items.Add(node);
				}
        FillScorecardTree(node, dr["id_form"].ToString());
			}

	//		((TreeViewItem)tView.Items[0]).IsExpanded = true;
		}

		private void tView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			//реакция на выделение показателя
			TreeViewItem item = (TreeViewItem)e.NewValue;
			if (_curr_item == null) return;
			//TreeViewItemHeader h = (TreeViewItemHeader)((item).Header);
			//TreeViewItemHeader ch = (TreeViewItemHeader)(_curr_item.Header);
			//  if (h.Text != ch.Text)
			//  {
			//  _curr_item.IsSelected = true;
			//  }

		}
		private void Item_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			TreeViewItem item = (TreeViewItem)tView.SelectedItem;
			TreeViewItemHeader h = (TreeViewItemHeader)((item).Header);

			SelectedItemEventArgs args = new SelectedItemEventArgs();
			args.id_SC = item.Name.Substring(1);
			args.name_SC = h.Text;

			if (_curr_item != null)
			{
				TreeViewItemHeader ch = (TreeViewItemHeader)(_curr_item.Header);
				if (h.Text != ch.Text)
				{
					_curr_item = item;
					OnSelectedItem(args);
				}
			}
			else
			{
				_curr_item = item;
				OnSelectedItem(args);
			}
		}

		#region Events
		public class SelectedItemEventArgs : EventArgs
		{
			public string id_SC;
			public string name_SC;
		}
		public delegate void SelectedItemEventHandler(Object sender, SelectedItemEventArgs args);
		public event SelectedItemEventHandler SelectedItem;
		protected virtual void OnSelectedItem(SelectedItemEventArgs args)
		{
			if (SelectedItem != null)
			{
				SelectedItem(this, args);
			}
		}
		#endregion Events
	

	}

}
