﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для PKMReport.xaml
  /// </summary>
  public partial class PKMReport : UserControl
  {
    CMySql _mySql;

    TreeViewItem _curr_item;
    string _idPKM;
    public PKMReport()
    {
      InitializeComponent();
    }
    public void Set(CMySql mySql, string idPKM)
    {
      _mySql = mySql;
      _idPKM = idPKM;
      Activities1.Visibility = System.Windows.Visibility.Hidden;
      Activities1.ActivitiesHandler += new Activities.ActivitiesEventHandler(Activities1_ActivitiesHandler);
      FillTree();
    }

    void Activities1_ActivitiesHandler(object sender, Activities.ActivitiesEventArgs e)
    {
      if (e.TypeEvent == TypeMessages.SendPKMForApproved)
      {
        TreeViewItemHeader h = (TreeViewItemHeader)_curr_item.Header;
        h.ImgSource = "/Controlling;component/Ico/button_ok_32 red.png";
      }
      if (e.TypeEvent == -1)//закрыть окно
      {
        SelectedItemEventArgs args = new SelectedItemEventArgs() { id_SC ="-1", id_SC_root = "-1", name_SC = "-1" };
        OnSelectedItem(args);
      }
    }

    void FillBranch(TreeViewItem root, string id_root)
    {

      DataTable dt = BSCtree.SelectFromTreeAndSC(_mySql, id_root,-1);
      foreach (DataRow dr_SC in dt.Rows)
      {
          TreeViewItem node = new TreeViewItem();
          TreeViewItemHeader h = null;
          if (_mySql.ToInt32(dr_SC["Level1"]) == TypeHeader.Goal)
          {
            h = new TreeViewItemHeader("", dr_SC["name1"].ToString(), TypeHeader.Goal, root);
          }
          if (_mySql.ToInt32(dr_SC["Level1"]) == TypeHeader.Score)
          {
            h = new TreeViewItemHeader("", dr_SC["name1"].ToString(), TypeHeader.Score, root);
          }
          if (_mySql.ToBool(dr_SC["IsApproved"]))
          {
            h.ImgSource = "/Controlling;component/Ico/SCApproved.png";
          }
          //					h.ShowPict(false);

          node.Header = h; node.Name = "n" + dr_SC["id_SC"].ToString();
          node.Tag = root;

          root.Items.Add(node);
          root.MouseLeftButtonUp += new MouseButtonEventHandler(Item_MouseLeftButtonUp);

      }
    }
    public void FillTree()
    {
      tView.Items.Clear();

      string sql = "select * from SC where IDControlPerson=" + UserInfo.Info.id_form + " and fact<plan" + " and targetyear = " + UserInfo.TargetYear.ToString();

      if (_idPKM != "-1") sql = "select * from SC where ID=(select IDSC from SC_PKM where id=" + _idPKM + ") and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt_SC = _mySql.SqlSelect(sql);
      foreach (DataRow dr in dt_SC.Rows)
      {

        TreeViewItem root = new TreeViewItem(); root.Tag = null;
        TreeViewItemHeader h = new TreeViewItemHeader("/Controlling;component/Ico/SCNotApproved.png", dr["name1"].ToString(), TypeHeader.Perspective, null);
        root.Header = h; root.Name = "n" + dr["id"].ToString();
        tView.Items.Add(root);

        root.MouseLeftButtonUp += new MouseButtonEventHandler(Item_MouseLeftButtonUp);
//        if (!_isPKM) FillBranch(root, dr["id"].ToString());

        sql = "select * from SC_PKM where idSC=" + dr["id"].ToString() + " and targetyear = " + UserInfo.TargetYear.ToString();
        DataTable dt = _mySql.SqlSelect(sql);
        if (dt.Rows.Count > 0)
        {
          if (_mySql.ToBool(dt.Rows[0]["isForApproved"])) h.ImgSource = "/Controlling;component/Ico/button_ok_32 red.png";
          if (_mySql.ToBool(dt.Rows[0]["isApproved"])) h.ImgSource = "/Controlling;component/Ico/button_ok_32.png";
        }

      }
      if (tView.Items.Count > 0) ((TreeViewItem)tView.Items[0]).IsExpanded = true;
    }


    void SetActivities(string id_SC, string id_SC_root)
    {
      string sql = "select * from SC where id=" + id_SC + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt_SC = _mySql.SqlSelect(sql);

      sql = "select * from SC where id=" + id_SC_root + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt_SC_root = _mySql.SqlSelect(sql);

    //  if (dt_SC_root.Rows.Count > 0)
      {
        Activities1.Set(dt_SC.Rows[0], _mySql, true);
        Activities1.Visibility = System.Windows.Visibility.Visible;
      }
  //    Activities1.Visibility = System.Windows.Visibility.Hidden;

    }
    void SelectItem(TreeViewItem item)
    {
      TreeViewItemHeader h = (TreeViewItemHeader)((item).Header);

      SelectedItemEventArgs args = new SelectedItemEventArgs();
      args.id_SC_root = "-1";
      if (item.Tag != null) args.id_SC_root = ((TreeViewItem)item.Tag).Name.Substring(1);
      args.id_SC = item.Name.Substring(1);
      args.name_SC = h.Text;

      if (_curr_item != null)
      {
        TreeViewItemHeader ch = (TreeViewItemHeader)(_curr_item.Header);
        if (h.Text != ch.Text)
        {
          _curr_item = item;
          SetActivities(args.id_SC, args.id_SC_root);

          OnSelectedItem(args);
        }
      }
      else
      {
        _curr_item = item;
        SetActivities(args.id_SC, args.id_SC_root);

        OnSelectedItem(args);
      }
    }
    private void Item_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {

      TreeViewItem item = (TreeViewItem)tView.SelectedItem;
      SelectItem(item);
    }

    #region Events
    public class SelectedItemEventArgs : EventArgs
    {
      public string id_SC_root;
      public string id_SC;
      public string name_SC;
    }
    public delegate void SelectedItemEventHandler(Object sender, SelectedItemEventArgs args);
    public event SelectedItemEventHandler SelectedItem;
    protected virtual void OnSelectedItem(SelectedItemEventArgs args)
    {
      if (SelectedItem != null)
      {
        SelectedItem(this, args);
      }
    }
    #endregion Events

    private void Activities1_Loaded(object sender, RoutedEventArgs e)
    {

    }
  }
}
