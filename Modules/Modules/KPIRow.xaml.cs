﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
namespace Modules
{
	/// <summary>
	/// Логика взаимодействия для KPIRow.xaml
	/// </summary>
	public partial class KPIRow : UserControl
	{
		DataRow _row;
    public bool _isFromKPI;
		public KPIRow()
		{
			InitializeComponent();
		}

    public void SetCaption()
    {
      l_fio.Text = "ФИО";
      l_dep.Text = "Подразделение";
      l_name.Text = "Название";
      l_weight.Text = "Вес %";
      l_plan.Text = "План";
      l_fact.Text = "Факт";
      l_date_begin.Text = "Дата начала";
      l_date_end.Text = "Дата окончания";
    }
    public void Set(DataRow row, bool isFromKPI)
    {
      _row = row;
      _isFromKPI = isFromKPI;
      if (_isFromKPI)
      {
        l_fio.Text = row["fio"].ToString();
        l_dep.Text = row["dep"].ToString();
        l_name.Visibility = System.Windows.Visibility.Collapsed;
      }
      else
      {
        l_name.Text = row["name1"].ToString();
        l_dep.Visibility = System.Windows.Visibility.Collapsed;
        l_fio.Visibility = System.Windows.Visibility.Collapsed;
      }
      l_weight.Text = row["weight"].ToString();
      l_plan.Text = row["plan"].ToString();
      l_fact.Text = row["fact"].ToString();
      l_date_begin.Text = row["date_begin"].ToString();
      l_date_end.Text = row["date_end"].ToString();


      l_weight.Visibility = System.Windows.Visibility.Collapsed;
      l_plan.Visibility = System.Windows.Visibility.Collapsed;
      l_fact.Visibility = System.Windows.Visibility.Collapsed;
      l_date_begin.Visibility = System.Windows.Visibility.Collapsed;
      l_date_end.Visibility = System.Windows.Visibility.Collapsed;
    }
	}
}
