﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
using Modules;
using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для IndWrapPanel.xaml
  /// </summary>
  public partial class IndWrapPanel : UserControl
  {
    public static int IsShowSmartEvent = 1;
    UIElementCollection ScoreCardElements
    {
      get { return WPanel.Children; }
    }

    string _caption;
    public string Caption
    {
      set { _caption = value; TB_Caption.Text = _caption; }
    }
    public int CountScoreCards
    {
      get { return WPanel.Children.Count; }
    }

    int _level; // уровень в таблице SC
    int _id_SC; // id в таблице SC (id_SC в таблице SCTree) - Perspective
    CMySql _mySql;
    IndWrapPanel _wpChild;
    Monitoring _monitoring;
    ScorecardElement CurrentIndicator;

    ScorecardElement ParentIndicator; //индикатор, выбранный в родительской панели
    public IndWrapPanel()
    {
      InitializeComponent();
      //			r1.Style = (Style)this.Resources["IndicatorStyle"];
      CurrentIndicator = null;
    }
    public void Setup(CMySql mySql, int level, int id_SC, string caption, IndWrapPanel wpChild, Monitoring monitoring)
    {
      _mySql = mySql;
      _level = level;
      _id_SC = id_SC;
      _caption = caption;
      TB_Caption.Text = caption;
      _wpChild = wpChild;
      _monitoring = monitoring;

      //     UpdateElements(_id_SC);
    }
    public void RemoveAllIndicators()
    {
      if (WPanel.Children.Count > 0) ScoreCardElements.Clear();// WPanel.Children.RemoveRange(0, WPanel.Children.Count);
      if (_wpChild != null) _wpChild.RemoveAllIndicators();
    }


    public int SelectScorecardInAllIWP(ScorecardElement sce)
    {
      //      if (sce.idSC != -1) if (CurrentIndicator != null) CurrentIndicator.isChecked = false;
      if (CurrentIndicator == null) SelectScorecard(sce);
      else if (CurrentIndicator.idSC != sce.idSC) SelectScorecard(sce);

      if (_wpChild != null) _wpChild.SelectScorecardInAllIWP(sce);
      //foreach (Object o in _wpChild.ScoreCardElements)
      //{
      //  ScorecardElement se = (ScorecardElement)o;
      //  if (se.idSC == sce.idSC)
      //  {
      //    if (se.childrenSC.Count > 0)
      //    {//пермещаем элементы из дочерней панели в главную
      //      foreach (UIElement uie in _wpChild.ScoreCardElements) ScoreCardElements.Add(uie);
      //      _wpChild.ScoreCardElements.Clear();
      //      foreach (ScorecardElement child_sce in se.childrenSC) _wpChild.ScoreCardElements.Add(child_sce);
      //    }
      //    //           se.isChecked = true;

      //  }
      //}
      return sce.idSC;
    }
    void SelectScorecard(ScorecardElement sce)
    {
      foreach (Object o in ScoreCardElements)
      {
        ScorecardElement se = (ScorecardElement)o;
        if (se.idSC == sce.idSC)
        {
          if (sce.childrenSC.Count > 0)
          {
            _wpChild.ScoreCardElements.Clear();
            foreach (ScorecardElement child_sce in sce.childrenSC) _wpChild.ScoreCardElements.Add(child_sce);

            _wpChild.Caption = sce.IndName;
            _monitoring.Set(_mySql, sce.idSC.ToString(), sce.IndName);
          }

          if (CurrentIndicator != null) CurrentIndicator.isChecked = false;
          CurrentIndicator = sce;
          sce.isChecked = true;
          break;
        }
      }
    }

    public int SelectScorecard(int idSC)
    {
      //      if (idSC == -1)
      if (CurrentIndicator != null)
      {
        if (CurrentIndicator.idSC == idSC) return idSC;
        CurrentIndicator.isChecked = false;
      }
      foreach (Object o in WPanel.Children)
      {
        ScorecardElement se = (ScorecardElement)o;
        if (se.idSC == idSC)
        {
          CurrentIndicator = se;
          se.isChecked = true;
          return idSC;
        }
      }
      return -1;
    }

    public System.Windows.Visibility OnlyUserSCInChild()
    {
      if (_wpChild == null) return Visibility.Collapsed;
      bool isOnlyUserSC = UserInfo.isOnlyUserSC;
      //      foreach (ScorecardElement scElement in ScoreCardElements)
      System.Windows.Visibility isVisible = System.Windows.Visibility.Collapsed;
      {
        foreach (Object o in _wpChild.ScoreCardElements)
        {
          ScorecardElement se = (ScorecardElement)o;
          if (isOnlyUserSC)
          {
            if ((se.IdResponsiblePerson == UserInfo.Info.id_form) | (se.IdControlPerson == UserInfo.Info.id_form))
            {
              isVisible = System.Windows.Visibility.Visible;
              se.Visibility = System.Windows.Visibility.Visible;
            }
            else se.Visibility = System.Windows.Visibility.Collapsed;

          }
          else se.Visibility = System.Windows.Visibility.Visible;
        }
        //       if (isOnlyUserSC) scElement.Visibility = isVisible; else scElement.Visibility = System.Windows.Visibility.Visible;
      }
      return isVisible;
    }

    ScorecardElement AddIndicator(string Name, int idSC, int idParent, int idPerspective)
    {
      ScorecardElement se = new ScorecardElement();
      WPanel.Children.Add(se);
      se.SetIndicator(Name, idSC, idParent, idPerspective, _mySql, new ScorecardElement.ScorecardElementdEventHandler(OnScorecardElementEvent));
      //			se.CheckedChanged += new RoutedPropertyChangedEventHandler<bool>(ScorecardElement_CheckedChanged);
      return se;
    }
    public void AddIndicator(ScorecardElement sce)
    {
      ScoreCardElements.Add(sce);
    }
    void UpdateElements(int id_parent, int idPerspective)
    {

      string sql = "select * from SCTree where id_parent = " + id_parent.ToString() + " and targetyear = " + UserInfo.TargetYear.ToString() + " order by id";
      DataTable dt = _mySql.SqlSelect(sql, "");
      if (dt.Rows.Count == 0) return;

      {
        //не соблюдается порядок сортировки по мере введения показателей
        //sql = "select * from SC where id = " + dt.Rows[0]["id_SC"].ToString();
        //for (int i = 1; i < dt.Rows.Count; i++)
        //  sql += " or id = " + dt.Rows[i]["id_SC"].ToString();

        //dt = _mySql.SqlSelect(sql, "");
      }
      foreach (DataRow dr in dt.Rows)
      {
        sql = "select * from SC where id = " + dr["id_SC"].ToString() + " and targetyear = " + UserInfo.TargetYear.ToString();
        DataRow drSC = _mySql.SqlSelect(sql, "").Rows[0];
        ScorecardElement se = AddIndicator(drSC["name1"].ToString(), _mySql.ToInt32(drSC["id"]), id_parent, idPerspective);

        se.IdControlPerson = drSC["IdControlPerson"].ToString();
        se.IdResponsiblePerson = drSC["IdResponsiblePerson"].ToString();
      }
    }

    #region Обновление дочерней панели
    public void UpdateChildElements(ScorecardElement parentIndicator)
    {
      //для вызова только из родительской панели панели
      WPanel.Children.Clear();
      ParentIndicator = parentIndicator;
      if (ParentIndicator != null)
      {
        UpdateElements(ParentIndicator.idSC, parentIndicator.idPerspective);
      }
    }

    ScorecardElement NewIndicator(int level, int idSC_parent_ind, int idPerspective, string name, int id_SC = -1)
    {
      //string sql;
      //DataTable dt;
      //if (id_SC == -1)
      //{
      //  sql = "insert into SC (level1, name1, TargetYear) values (" + Scorecard.KPI.ToString() + ", '" + name + "')";
      //  _mySql.SqlExec(sql);
      //  sql = "select id from SC" + " where targetyear = " + UserInfo.TargetYear.ToString() + " order by id";
      //  dt = _mySql.SqlSelect(sql);
      //  id_SC = (int)dt.Rows[dt.Rows.Count - 1]["id"]; //дочерний для таблицы SCTree
      //}

      //sql = "insert into SCTree (id_parent, id_SC) values (" + idSC_parent_ind + ", " + id_SC.ToString() + ")";
      //_mySql.SqlExec(sql);

      //sql = "select * from SCTree where id_parent=" + _id_SC.ToString() + " and id_SC=" + id_SC.ToString() + " and targetyear = " + UserInfo.TargetYear.ToString();
      //dt = _mySql.SqlSelect(sql);

      //ScorecardElement se = AddIndicator(name, Convert.ToInt32(id_SC), _id_SC, idPerspective);
      //return se;
      return null;
    }
    void OnAddNewChildElement(Object sender, ChildElements.ChildElementsEventArgs args)
    {

      if (_wpChild == null)
      {

        if (args.idParent == -1)
        {
          //         ScorecardElement se = NewIndicator(Scorecard.KPI, ParentIndicator.idSC, "Введите Название Индикатора");
          //         se.EditName();
        }
        //       else NewIndicator(Scorecard.KPI, ParentIndicator.idSC, args.Name, args.idParent);
      }
      else
      {
        //for (int i = 0; i < WPanel.Children.Count; i++)
        //  if (((ScorecardElement)WPanel.Children[i]).idBase == args.idParent)
        //  {
        //    MessageBox.Show("Индикатор \r\n" + args.Name +"\r\nв панели уже имеется", "Сообщение", MessageBoxButton.OK); // не отображается ?
        //    return;
        //  }
        //ScorecardElement se = AddIndicator(args.Name);
        //se._idBase = args.idParent;

        //string sql = "insert into bsc_kpi (id_BSC, id_KPI) values (" + ParentIndicator._idBase.ToString() + ", " + se._idBase.ToString() + ")";
        //_mySql.SqlExec(sql);

      }
    }
    #endregion Обновление дочерней панели

    private void b_AddNewIndicator_Click(object sender, RoutedEventArgs e)
    {

      if (_wpChild != null)
      {
        //ScorecardElement se = NewIndicator(Scorecard.BSC, _id_SC, "Введите Название Индикатора");
        //se.EditName();
      }
      else if (ParentIndicator != null)
      {
        string sql = "select * from SC where level1=" + Scorecard.KPI + " and targetyear = " + UserInfo.TargetYear.ToString();

        DataTable dt = _mySql.SqlSelect(sql, "");
        ChildElements wnd = new ChildElements(dt, new ChildElements.ChildElementdEventHandler(OnAddNewChildElement));
        Point p = PointToScreen(Mouse.GetPosition(b_AddNewIndicator));
        wnd.Left = p.X;
        wnd.Top = p.Y;

        wnd.Show();
      }
    }

    void DeleteIndicator(string id_parent)
    {
      string sql = "select * from SCTree where id_parent = " + id_parent + " and targetyear = " + UserInfo.TargetYear.ToString();
      DataTable dt = _mySql.SqlSelect(sql);
      foreach (DataRow dr in dt.Rows)
      {
        DeleteIndicator(dr["id_SC"].ToString());
        sql = "delete from SCTree where id = " + dr["id"].ToString();
        _mySql.SqlExec(sql);
      }
      sql = "delete from SCTree where id_SC = " + id_parent;
      _mySql.SqlExec(sql);
    }
    private void b_DelNewIndicator_Click(object sender, RoutedEventArgs e)
    {
      DeleteIndicator(CurrentIndicator.idSC.ToString());
      WPanel.Children.Remove(CurrentIndicator);
    }

    public void SelectIndicator1(ScorecardElement se)
    {
      if (CurrentIndicator != null) if (CurrentIndicator.idSC != se.idSC) CurrentIndicator.SelectIndicator(false);
      CurrentIndicator = se;
      if (_wpChild != null)
      {
        _wpChild.Setup(_mySql, Scorecard.KPI, _id_SC, "Показатели следующего уровня", null, null);//level = Scorecard.KPI
        _wpChild.UpdateChildElements(se);
        se.Visibility = OnlyUserSCInChild();
      }
      else
      {
      }
    }
    #region Обработка событий от элементов
    void SelectIndicator(ScorecardElement se)
    {
      if (CurrentIndicator != null) if (CurrentIndicator.idSC != se.idSC) CurrentIndicator.SelectIndicator(false);
      CurrentIndicator = se;
      if (_wpChild != null)
      {
        _wpChild.Setup(_mySql, Scorecard.KPI, _id_SC, "Показатели следующего уровня", null, null);//level = Scorecard.KPI
        _wpChild.UpdateChildElements(se);
        se.Visibility = OnlyUserSCInChild();

        IndWrapPanelEventArgs args = new IndWrapPanelEventArgs();
        args.Element = CurrentIndicator; args.Event = IsShowSmartEvent;
        OnIndWrapPanelEvent(args);
      }

      b_DelNewIndicator.IsEnabled = true;
    }
    void OnScorecardElementEvent(Object sender, ScorecardElement.ScorecardElementEventArgs e)
    {
      //возникает при действии с индикатором
      ScorecardElement se = e.Element;
      if (e.Event == ScorecardElement.IsCheckedEvent)
      {
        SelectIndicator(se);
      }
      if (e.Event == ScorecardElement.IsUnCheckedEvent)
      {
        if (_wpChild != null) _wpChild.UpdateChildElements(null);
        b_DelNewIndicator.IsEnabled = false;
      }
      if (e.Event == ScorecardElement.IsNameEditEvent)
      {
        if (ParentIndicator != null)
        {
          if (MessageBox.Show("Изменение будет применено ко всем показателям. \r\n Продолжить?", "Предупреждение", MessageBoxButton.YesNo) == MessageBoxResult.No)
            return;
        }
        string sql = "Update SC set name1 = '" + se.IndName + "' where id=" + se.idSC.ToString();
        _mySql.SqlExec(sql);
      }

    }
    #endregion
    #region События Панели
    public class IndWrapPanelEventArgs : EventArgs
    {
      int _event;
      ScorecardElement _se;

      public int Event
      {
        get { return _event; }
        set { _event = value; }
      }
      public ScorecardElement Element
      {
        get { return _se; }
        set { _se = value; }
      }
      public IndWrapPanelEventArgs()
      {
        _event = -1;
        _se = null;
      }
    }
    public delegate void IndWrapPanelEventHandler(Object sender, IndWrapPanelEventArgs args);
    public event IndWrapPanelEventHandler IndWrapPanelEvent;
    protected virtual void OnIndWrapPanelEvent(IndWrapPanelEventArgs e)
    {
      if (IndWrapPanelEvent != null)
      {
        IndWrapPanelEvent(this, e);
      }
    }
    #endregion

  }
}
public class Scorecard
{
  public static int Perspective = 0;
  public static int BSC = 20;
  public static int KPI = 40;
}

