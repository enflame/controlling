﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для dlgPeriod.xaml
  /// </summary>
  public partial class dlgPeriod : Window
  {

    string _periodType;
    bool isClosing;

    public List<string> period;
    public int yearFrom;
    public int yearTo;

    public dlgPeriod(string periodType, int targetYear)
    {
      InitializeComponent();

      _periodType = periodType;
      if (periodType == Period.Year)
      {
        tv_period.Visibility = System.Windows.Visibility.Collapsed;
      }
      if (_periodType == Period.HalfYear)
      {
      }
      if (_periodType == Period.Quarter)
      {
      }
      if (_periodType == Period.Month)
      {
      }

      period = new List<string>();

      UpDown_yearFrom.Value = DateTime.Now.Year;
      UpDown_yearTo.Value = targetYear;
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      isClosing = true;
    }

    private void Window_Deactivated(object sender, EventArgs e)
    {
      if (!isClosing) Close();
    }

    void GetMonth(TreeViewItem item)
    {
      if (item == null) return;
      if (item.Items.Count == 0)
      {
        period.Add((string)item.Header);
        return;
      }
      foreach (Object o in item.Items)
      {
        GetMonth((TreeViewItem)o);
      }
    }
    private void b_Select_Click(object sender, RoutedEventArgs e)
    {
      TreeViewItem item = (TreeViewItem)tv_period.SelectedItem;
      if (_periodType == Period.Year)
      {
        int d1 = (int)UpDown_yearFrom.Value;
        int d2 = (int)UpDown_yearTo.Value;
        while (d1 <= d2)
        {
          period.Add(d1.ToString());
          d1++;
        }
      }
      if (_periodType == Period.Month) GetMonth(item);

      DialogResult = true;

    }

    private void b_cancel_Click(object sender, RoutedEventArgs e)
    {
      period = null;
      DialogResult = false;

    }

    private void tv_period_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      tb_period.Text = (string)(((TreeViewItem)e.NewValue).Header);
    }

    private void IntegerUpDown_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      yearFrom = (int)UpDown_yearFrom.Value;
    }

    private void b_cancel_Click_1(object sender, RoutedEventArgs e)
    {
      Close();
    }
  }
}
