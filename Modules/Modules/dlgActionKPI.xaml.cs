﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using Microsoft.Win32;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для dlgActionKPI.xaml
  /// </summary>
  public partial class dlgActionKPI : Window
  {
    DataRowView _dr;
    public dlgActionKPI(DataRowView dr)
    {
      InitializeComponent();


      _dr = dr;
      grid_window.DataContext = dr;
    }

    private void b_task_attachment_Click(object sender, RoutedEventArgs e)
    {
      OpenFileDialog dlg = new OpenFileDialog();
      if ((bool)dlg.ShowDialog())
      {
        _dr["fileNameTask"] = dlg.FileName;
      }
    }

    private void b_Save_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = true;
    }

    private void b_ready_attachment_Click(object sender, RoutedEventArgs e)
    {
      OpenFileDialog dlg = new OpenFileDialog();
      if ((bool)dlg.ShowDialog())
      {
        _dr["b_ready_attachment"] = dlg.FileName;
      }
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {

    }

  }
}
