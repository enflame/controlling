﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для dlgAddValue.xaml
  /// </summary>
  public partial class dlgAddValue : Window
  {
    public string Value;
    public dlgAddValue(string Caption)
    {
      InitializeComponent();
      tb_Caption.Text = Caption;
    }

    private void b_ok_Click(object sender, RoutedEventArgs e)
    {
      Value = tb_Value.Text;
      DialogResult = true;
    }

    private void b_cancel_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = false;
    }
  }
}
