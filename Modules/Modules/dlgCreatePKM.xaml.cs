﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using Modules;
using MySql;
namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для dlgCreatePKM.xaml
  /// </summary>
  public partial class dlgCreatePKM : Window
  {

    public dlgCreatePKM(CMySql mySql, bool isPKM, string idSC)
    {
      //если idSC!=-1 то для утверждения
      InitializeComponent();

      pKMReport.Set(mySql, idSC);
      pKMReport.SelectedItem += new PKMReport.SelectedItemEventHandler(pKMReport_SelectedItem);
    }
    void pKMReport_SelectedItem(object sender, PKMReport.SelectedItemEventArgs e)
    {
      if (e.name_SC == "-1")//закрыть окно
      {
        Close();
      }
    }

    private void b_Close_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }

  }
}
