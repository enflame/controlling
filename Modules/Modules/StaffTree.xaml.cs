﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
using MySql;
using GeneralClasses;
namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для StaffTree.xaml
  /// </summary>
  public partial class StaffTree : UserControl
  {
    public string Caption
    {
      set
      {
        if (value == "") TB_tViewName.Visibility = System.Windows.Visibility.Collapsed;
        else
        {
          TB_tViewName.Text = value;
          TB_tViewName.Visibility = System.Windows.Visibility.Visible;
        }
      }
    }

    public string Id_Selected_staff;

    CMySql _mySql;


    public StaffTree()
    {
      InitializeComponent();
    }
    public void Set(CMySql mySql)
    {
      _mySql = mySql;

      FillStructTree();
    }
    DataTable SelectStaff(string PODRAZDELENIE_ID)
    {
      string sql = "select  P.PARENT_ID AS id_parent_dep, P.ID as id_dep, P.NAZVANIE AS dep_name, " +
                   "UF.PODRAZDELENIE_ID AS id_dep, UF.DOLGNOST_ID AS id_position, UF.DOLGNOST, " +
                   "U.LICHNOST_ID, U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO, u.login, u.email  " +
                   "from SOTRUDNIK.LICHNOST_FOR_PHONE_SPR_V3 UF " +
                   "inner join tpu.podrazdelenie_tpu_v P on P.ID =UF.PODRAZDELENIE_ID " +
                   "inner join app_portal.v_tpu_users U on U.LICHNOST_ID=UF.LICHNOST_ID " +
                   "where P.ID=" + PODRAZDELENIE_ID + " order by FAMILIYA";
      return _mySql.SqlSelect(sql);
    }
    public static StaffInfo SelectStaffByID(string LICHNOST_ID, CMySql mySql)
    {
      string sql = "select  P.PARENT_ID AS id_parent_dep, P.ID as id_dep, P.NAZVANIE AS dep_name, " +
                   "UF.PODRAZDELENIE_ID AS id_dep, UF.DOLGNOST_ID AS id_position, UF.DOLGNOST, " +
                   "U.LICHNOST_ID, U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO, u.login, u.email  " +
                   "from SOTRUDNIK.LICHNOST_FOR_PHONE_SPR_V3 UF " +
                   "inner join tpu.podrazdelenie_tpu_v P on P.ID =UF.PODRAZDELENIE_ID " +
                   "inner join app_portal.v_tpu_users U on U.LICHNOST_ID=UF.LICHNOST_ID " +
                   "where U.LICHNOST_ID=" + LICHNOST_ID;
      DataRow dr = mySql.SqlSelect(sql).Rows[0];
      StaffInfo si = new StaffInfo();
      si.fio = dr["FIO"].ToString();
      si.id_form = dr["LICHNOST_ID"].ToString();
      return si;
    }

    DataTable SelectDeps(string PARENT_ID)
    {
   /*   
    string sql = "select  P.ID, P.PARENT_ID AS id_parent_dep, P.NAZVANIE AS dep_name " +
                   "from tpu.podrazdelenie_tpu_v P " +
                   "where P.PARENT_ID=" + PARENT_ID + "and (P.NAZVANIE NOT LIKE '%Филиал%' and P.NAZVANIE NOT LIKE '%Представительство%')";
   */
      string sql = "select  P.ID, P.PARENT_ID AS id_parent_dep, P.NAZVANIE AS dep_name " +
                   "from tpu.podrazdelenie_tpu_v P " +
                   "where P.PARENT_ID=" + PARENT_ID + " and P.REC_STATUS = 1";
      return _mySql.SqlSelect(sql);
    }

    void FillStructBranch(TreeViewItem root)
    {
      string id = root.Name.Substring(1);

      DataTable dt = SelectDeps(id);
      foreach (DataRow dr in dt.Rows)
      {
        TreeViewItem item = new TreeViewItem();
        item.Name = "I" + dr["id"].ToString();
        TreeViewItemHeader h = new TreeViewItemHeader("", dr["dep_name"].ToString(), -1, root);
        item.Header = h;
        root.Items.Add(item);
        FillStructBranchWithStaff(item);
        FillStructBranch(item);
      }
    }
    StackPanel NewLb_staffItem(string id_staff, string id_form, string fio, string id_position, string position)
    {
      StackPanel sp = new StackPanel(); sp.Name = "S" + id_staff; sp.Orientation = Orientation.Horizontal; sp.Background = Brushes.Transparent;
      Image img = new Image(); img.Height = img.Width = 16; img.Source = Functions.ToImageSource("/Controlling;component/Ico/user_alt2_32.png");
      TextBlock t_fio = new TextBlock(); t_fio.Name = "F" + id_form; t_fio.Text = fio; t_fio.FontWeight = FontWeights.UltraBold;
      TextBlock t_pos = new TextBlock(); t_pos.Name = "P" + id_position; t_pos.Text = position;
      t_pos.Margin = new Thickness(5, 1, 1, 0); t_pos.FontWeight = FontWeights.Light;
      sp.Children.Add(img); sp.Children.Add(t_fio); sp.Children.Add(t_pos);

      return sp;
    }
    void FillStructBranchWithStaff(TreeViewItem root)
    {
      string id_dep = root.Name.Substring(1);
      DataTable dt_s = SelectStaff(id_dep);

      StaffListInGrid sl = new StaffListInGrid(); sl.Set(dt_s); sl.StaffTrree_SelectedItem += SelectedStaff;
      sl.Margin = new Thickness(0, -5, 0, 0);
      TreeViewItem item = new TreeViewItem();
      item.Header = sl;
      root.Items.Add(item);
    }
    void FillStructTree()
    {
      tView.Items.Clear();
      string sql = "select P.ID, P.PARENT_ID AS id_parent_dep, P.NAZVANIE AS dep_name " +
                  "from tpu.podrazdelenie_tpu_v P where P.ID=7331";
      DataTable dt = _mySql.SqlSelect(sql);

      foreach (DataRow dr in dt.Rows)
      {
        TreeViewItem item = new TreeViewItem();
        item.Name = "I" + dr["id"].ToString();
        TreeViewItemHeader h = new TreeViewItemHeader("/Controlling;component/Ico/boss_32.png", dr["dep_name"].ToString(), -1, null);
        item.Header = h;
        tView.Items.Add(item);
        FillStructBranchWithStaff(item);
        FillStructBranch(item);
      }
      if (tView.Items.Count > 0) ((TreeViewItem)tView.Items[0]).IsExpanded = true;
    }

    private void TreeViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      TreeViewItem item = (TreeViewItem)e.Source;
      SelectedItemEventArgs args = (SelectedItemEventArgs)item.Tag;
      Id_Selected_staff = item.Name.Substring(1);
      StackPanel sp = (StackPanel)item.Header;
      args.isMouseDoubleClick = true;

      OnSelectedStaff(args);

    }
    private void tView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      //отображение сотруднико подразделения
      if (e.NewValue == null) return;
      TreeViewItem item = (TreeViewItem)e.NewValue;
      if (item.Header.GetType() != typeof(System.Windows.Controls.StackPanel))
      {
        return;
      }

      SelectedItemEventArgs args = (SelectedItemEventArgs)item.Tag;
      Id_Selected_staff = item.Name.Substring(1);
      StackPanel sp = (StackPanel)item.Header;

      OnSelectedStaff((SelectedItemEventArgs)item.Tag);
    }

    #region Events
    public class SelectedItemEventArgs : EventArgs
    {
      public StaffInfo staffInfo = null;
      public bool isMouseDoubleClick;
    }
     public delegate void SelectedItemEventHandler(Object sender, SelectedItemEventArgs args);

    public event SelectedItemEventHandler SelectedStaff;
    protected virtual void OnSelectedStaff(SelectedItemEventArgs args)
    {
      if (SelectedStaff != null)
      {
        SelectedStaff(this, args);
      }
    }
    #endregion Events

    private void tView_MouseWheel(object sender, RoutedEventArgs e)
    {
      MouseWheelEventArgs eargs = (MouseWheelEventArgs)e;
      double x = (double)eargs.Delta;
      double y = scrollViewer.VerticalOffset;
      scrollViewer.ScrollToVerticalOffset(y - x);
    }

    private void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
    {
      tView.AddHandler(MouseWheelEvent, new RoutedEventHandler(tView_MouseWheel), true);
    }

  }
}
