﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using Word = Microsoft.Office.Interop.Word;
using System.Reflection;
using System.Data;

using System.Windows.Media.TextFormatting;
using System.Windows;
using MySql;
using GeneralClasses;

namespace Modules
{
  public class CReports
  {
    Word._Application _application;
    Word._Document _document;

    Object missingObj = System.Reflection.Missing.Value;
    Object trueObj = true;
    Object falseObj = false;
    List<MergeData> _mergeData;

    string _pathReports;
    public CReports(string pathReports)
    {
      _application = null;
      _document = null;
      _pathReports = pathReports;
    }
    //public void CellMerge(ref Word.Table tbl, Rectangle rect)
    //{

    //  string text = tbl.Cell(rect.Y, rect.X).Range.Text;
    //  Word.Font font = tbl.Cell(rect.Y, rect.X).Range.Font;
    //  Word.WdParagraphAlignment alignement = tbl.Cell(rect.Y, rect.X).Range.ParagraphFormat.Alignment;
    //  tbl.Cell(rect.Y, rect.X).Merge(tbl.Cell(rect.Bottom, rect.Right));
    //  tbl.Cell(rect.Y, rect.X).Range.Text = text;
    //  tbl.Cell(rect.Y, rect.X).Range.Font = font;
    //  tbl.Cell(rect.Y, rect.X).Range.ParagraphFormat.Alignment = alignement;
    //}

    public bool FindReplace(string find, string replacement)
    {
      try
      {
        object replaceAll = Word.WdReplace.wdReplaceAll;
        _application.Selection.Find.ClearFormatting();
        _application.Selection.Find.Text = find;
        _application.Selection.Find.Replacement.ClearFormatting();
        _application.Selection.Find.Replacement.Text = replacement;
        _application.Selection.Find.Execute(
            ref missingObj, ref missingObj, ref missingObj, ref missingObj, ref missingObj,
            ref missingObj, ref missingObj, ref missingObj, ref missingObj, ref missingObj,
            ref replaceAll, ref missingObj, ref missingObj, ref missingObj, ref missingObj);
        return true;
      }
      catch
      {
        return false;
      }
    }
    string GetPKM(string idSC, CMySql mySql)
    {
      string sql = "SELECT U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO, " +
                     "SC_Actions.id, SC_Actions.IDPKM, SC_Actions.idSC, SC_Actions.name1, SC_Actions.task, SC_Actions.BeginDate, SC_Actions.EndDate " +
                     "FROM SC_Actions " +
                     "INNER JOIN app_portal.v_tpu_users U ON U.LICHNOST_ID = SC_Actions.IDOwnPerson " +
                     "where idSC=" + idSC + " order by SC_Actions.ID";

      DataTable dt_actions = mySql.SqlSelect(sql);

      string res = "";

      for (int i = 0; i < dt_actions.Rows.Count; i++)
      {
        DataRow dr_action = dt_actions.Rows[i];
        if (dr_action["IDPKM"].ToString() != "-1") res += dr_action["name1"].ToString() +
          "\r\n\t" + DateTime.Parse(dr_action["BeginDate"].ToString()).ToShortDateString() +
          "\r\n\t" + DateTime.Parse(dr_action["EndDate"].ToString()).ToShortDateString() + "\r\n";
      }
      return res;
    }
    List<RowData> FillActions(int num_collumn, string idSC, CMySql mySql, bool isMerge, bool isPKM)
    {
      Word.Table table = _document.Tables[1];
      Word.Row row = _document.Tables[1].Rows[_document.Tables[1].Rows.Count];
      List<RowData> res = new List<RowData>();

      string sql = "SELECT U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO, " +
                   "SC_Actions.id, SC_Actions.IDPKM, SC_Actions.idSC, SC_Actions.name1, SC_Actions.task, SC_Actions.BeginDate, SC_Actions.EndDate " +
                   "FROM SC_Actions " +
                   "INNER JOIN app_portal.v_tpu_users U ON U.LICHNOST_ID = SC_Actions.IDOwnPerson " +
                   "where idSC=" + idSC + " and  SC_Actions.IDPKM=-1 order by SC_Actions.ID";

      if (isPKM)
      {
        sql = "SELECT U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO, " +
                     "SC_Actions.id, SC_Actions.IDPKM, SC_Actions.idSC, SC_Actions.name1, SC_Actions.task, SC_Actions.BeginDate, SC_Actions.EndDate " +
                     "FROM SC_Actions " +
                     "INNER JOIN app_portal.v_tpu_users U ON U.LICHNOST_ID = SC_Actions.IDOwnPerson " +
                     "where idSC=" + idSC + " order by SC_Actions.ID";
      }
      DataTable dt_actions = mySql.SqlSelect(sql);


      for (int i = 0; i < dt_actions.Rows.Count; i++)
      {
        DataRow dr_action = dt_actions.Rows[i];
        if (dr_action["IDPKM"].ToString() != "-1") row.Cells[num_collumn].Range.Text = "ПКМ: " + dr_action["name1"].ToString();
        else row.Cells[num_collumn].Range.Text = dr_action["name1"].ToString();
        row.Cells[num_collumn + 1].Range.Text = dr_action["task"].ToString();
        row.Cells[num_collumn + 2].Range.Text = DateTime.Parse(dr_action["BeginDate"].ToString()).ToShortDateString() + "\r\n" + DateTime.Parse(dr_action["EndDate"].ToString()).ToShortDateString();
        row.Cells[num_collumn + 3].Range.Text = dr_action["fio"].ToString();

        res.Add(new RowData() { index = _document.Tables[1].Rows.Count, id_parent = dr_action["id"].ToString() });

        if (i < dt_actions.Rows.Count - 1) row = table.Rows.Add();
      }

      if ((isMerge) & (res.Count > 0))
      {
        _mergeData.Add(new MergeData() { r_from = res[0].index, c_from = num_collumn - 1, r_to = res[res.Count - 1].index, c_to = num_collumn - 1 });
        _mergeData.Add(new MergeData() { r_from = res[0].index, c_from = num_collumn - 1 + 5, r_to = res[res.Count - 1].index, c_to = num_collumn - 1 + 5 });
        _mergeData.Add(new MergeData() { r_from = res[0].index, c_from = num_collumn - 1 + 6, r_to = res[res.Count - 1].index, c_to = num_collumn - 1 + 6 });
        _mergeData.Add(new MergeData() { r_from = res[0].index, c_from = num_collumn - 1 + 7, r_to = res[res.Count - 1].index, c_to = num_collumn - 1 + 7 });
        _mergeData.Add(new MergeData() { r_from = res[0].index, c_from = num_collumn - 1 + 8, r_to = res[res.Count - 1].index, c_to = num_collumn - 1 + 8 });
        _mergeData.Add(new MergeData() { r_from = res[0].index, c_from = num_collumn - 1 + 9, r_to = res[res.Count - 1].index, c_to = num_collumn - 1 + 9 });
      }
      return res;
    }
    void InsertGoal(DataRow dr_goal, Word.Row row, int num_collumn)
    {
    }
    void FillScorecardBranchOld(int num_collumn, string id_root, CMySql mySql, bool isMerge, bool isReport, bool isDelegated)
    {

      List<RowData> res = new List<RowData>();
      if (num_collumn > 2) return;
      Word.Table table = _document.Tables[1];

      DataTable dt_goals = null; ;
      if (isDelegated) BSCtree.SelectSCDelegated(mySql, id_root, UserInfo.Info.id_form, ref dt_goals);//только один раз
      else dt_goals = BSCtree.SelectFromTreeAndSC(mySql, id_root, -1);

      Word.Row row = _document.Tables[1].Rows[_document.Tables[1].Rows.Count];
      for (int i = 0; i < dt_goals.Rows.Count; i++)
      {
        DataRow dr_goal = dt_goals.Rows[i];
        row.Cells[num_collumn].Range.Text = dr_goal["name1"].ToString();

        res.Add(new RowData() { index = _document.Tables[1].Rows.Count, id_parent = dr_goal["id_SC"].ToString() });
        if (num_collumn == 2)
        {
          if (isReport)
          {
            //            FillActions(num_collumn + 1, dr_goal["id_SC"].ToString(), mySql, true, true);
            FillActions(num_collumn + 1, dr_goal["id_SC"].ToString(), mySql, true, false);
            string pkm = GetPKM(dr_goal["id_SC"].ToString(), mySql);
            row.Cells[row.Cells.Count].Range.Text = pkm;
          }
          else FillActions(num_collumn + 1, dr_goal["id_SC"].ToString(), mySql, true, false);

          if (isReport)
          {
            string sql = "select * from SC_Period where idsc = " + dr_goal["id_SC"].ToString();
            DataTable dt = mySql.SqlSelect(sql);
            int numCol = num_collumn + 5;
            foreach (DataRow dr in dt.Rows)
            {
              double plan = Functions.ToDouble(dr["plan"].ToString());
              double fact = Functions.ToDouble(dr["fact"].ToString());
              try
              {
                row.Cells[numCol].Range.Text = plan.ToString() + "\r\n" + fact.ToString();

                row.Cells[numCol].Range.Font.Bold = 5;
                if (plan > fact)
                {
                  row.Cells[numCol].Range.Font.Color = Word.WdColor.wdColorDarkRed;
                  row.Cells[numCol].Range.Font.Shadow = 5;
                }
                else row.Cells[numCol].Range.Font.Color = Word.WdColor.wdColorDarkGreen;
              }
              catch
              {
              }
              numCol++;
            }
          }
        }

        FillScorecardBranchOld(num_collumn + 1, dr_goal["id_SC"].ToString(), mySql, true, isReport, false);
        //res.Add(new RowData() { index = _document.Tables[1].Rows.Count, id_parent = dr_goal["id_SC"].ToString() });
        res.Add(new RowData() { index = _document.Tables[1].Rows.Count, id_parent = dr_goal["name1"].ToString() });

        if (i < dt_goals.Rows.Count - 1) row = table.Rows.Add();
      }
      if ((isMerge) & (dt_goals.Rows.Count > 0))
      {
        _mergeData.Add(new MergeData() { r_from = res[0].index, c_from = num_collumn - 1, r_to = res[res.Count - 1].index, c_to = num_collumn - 1 });
      }

    }

    void AddRowMerge(MergeData md)
    {
      MergeData md_parent = md.parent;
      while (md_parent != null)
      {
        if (md.r_to != md_parent.r_to)
        {
          md_parent.r_to += (md.r_to - md.r_from + 1);
        }
        md_parent = md_parent.parent;
      }
      _mergeData.Add(md);
    }
    void FillScorecardBranchPlan(int num_collumn, string id_root, CMySql mySql, int level, int id_resp_person, MergeData md_parent)
    {

      if (level > 3) return;
      Word.Table table = _document.Tables[1];
      Word.Row row = _document.Tables[1].Rows[_document.Tables[1].Rows.Count];

      DataTable dt_goals = null;
      //подзадачи
      dt_goals = BSCtree.SelectFromTreeAndSC(mySql, id_root, id_resp_person);
      bool isAddRow = false;
      if (num_collumn == 1) row = table.Rows.Add();
      MergeData md_task = null;

      for (int i = 0; i < dt_goals.Rows.Count; i++)
      {
        DataRow dr_goal = dt_goals.Rows[i];

        string sql = "select * from SC_Actions where idSC=" + dr_goal["id_SC"].ToString() + " and idPKM = -1 and targetyear = " + UserInfo.TargetYear.ToString();
       DataTable dt_SC_Actions = mySql.SqlSelect(sql, "SC_Actions");
       string str = (i + 1) + ". " + dr_goal["name1"].ToString() + " (" + dr_goal["targetvalue"].ToString() + " " + dr_goal["unit"].ToString() + ")";
       foreach (DataRow dr in dt_SC_Actions.Rows)
       {
         str = str + dr["name1"].ToString();
       }
        if (isAddRow) row = table.Rows.Add();
        row.Cells[num_collumn].Range.Text = str;


        md_task = new MergeData() { r_from = row.Index, c_from = num_collumn, r_to = row.Index, c_to = num_collumn, txt = dr_goal["name1"].ToString() };
        if (num_collumn > 1) md_task.parent = md_parent;
        AddRowMerge(md_task);

         //отв. исполнитель
        List<StaffInfo> si = DelegationTree.GetStaff(mySql, dr_goal["id_SC"].ToString());
        for (int j = 0; j < si.Count; j++)
        {
          StaffInfo s = si[j];
          row.Cells[num_collumn + 1].Range.Text = s.fio + " (" + s.TargetValue + " " + dr_goal["unit"].ToString() + ")";

          MergeData md_staff = new MergeData() { r_from = row.Index, c_from = num_collumn + 1, r_to = row.Index, c_to = num_collumn + 1, parent = md_task, txt = s.fio };
          AddRowMerge(md_staff);

          FillScorecardBranchPlan(num_collumn + 2, dr_goal["id_SC"].ToString(), mySql, level + 1, Convert.ToInt32(s.id_form), md_staff);
          if ((j + 1) < si.Count) row = table.Rows.Add();


        }
        isAddRow = true;
      }

    }
    public string MakePlanIDO(CMySql mySql, string f_name, string pathToTemplate)
    {
      //возвращает случайное имя файла
      //      if (!Functions.DeleteFile(_pathReports + "\\" + f_name)) return;

      try
      {
        _application = new Word.Application();
        DataRow drow = mySql.SqlSelect("select * from shablons where shablon_name='PLAN'").Rows[0];
        string file_name = UserInfo.TempDir + "\\" + System.IO.Path.GetRandomFileName();
        try
        {
          if (File.Exists(file_name)) File.Delete(file_name);
          FileStream FS = new FileStream(file_name, FileMode.Create);
          byte[] blob = (byte[])drow["file_data"];
          FS.Write(blob, 0, blob.Length);
          FS.Close();
          FS = null;
        }
        catch (Exception exc)
        {
          MessageBox.Show(exc.Message);
        }

        //создаем путь к файлу
        Object templatePathObj = file_name;
        //если вылетим на этом этапе, приложение останется открытым
        _document = _application.Documents.Add(ref  templatePathObj, ref missingObj, ref missingObj, ref missingObj);
      }
      catch (Exception error)
      {
        _document.Close(ref falseObj, ref  missingObj, ref missingObj);
        _application.Quit(ref missingObj, ref  missingObj, ref missingObj);
        _document = null;
        _application = null;
        throw error;
      }
      _application.Visible = true;
      Word.Table table = _document.Tables[1];
      _mergeData = new List<MergeData>();

      //_mergeData.Add(new MergeData() { r_from = 1, c_from = 9, r_to = 2, c_to = 9 });
      //_mergeData.Add(new MergeData() { r_from = 1, c_from = 7, r_to = 1, c_to = 8 });

      DataTable dt_persp = BSCtree.SelectFromTreeAndSC(mySql, "0", -1);

      int counter = 1;
      Word.Row row = table.Rows[2]; //первая строка для заполнения
      foreach (DataRow dr in dt_persp.Rows)
      {
        row.Cells[1].Range.Text = "Стратегическая цель: " + dr["name1"].ToString();
        MergeData md = new MergeData() { r_from = row.Index, c_from = 1, r_to = row.Index, c_to = row.Cells.Count };
        AddRowMerge(md);

        FillScorecardBranchPlan(1, dr["id_SC"].ToString(), mySql, 1, -1, md);

        object beforeRow = table.Rows[1];

        row = table.Rows.Add();

        counter++;
      }
      foreach (MergeData mg in _mergeData)
      {
        if ((mg.r_from != mg.r_to) | (mg.c_from != mg.c_to))
        {
          try
          {
            table.Cell(mg.r_from, mg.c_from).Merge(table.Cell(mg.r_to, mg.c_to));
          }
          catch
          {
            //          MessageBox.Show(mg.ToString());
          }

        }
      }
      //      _mergeData.Add(new MergeData() { r_from = 1, c_from = 1, r_to = 1, c_to = 2 });
      Word.Table table1 = _document.Tables[1];
      //      table1.Cell(1, 1).Merge(table.Cell(1, 2));
      FindReplace("@year@", UserInfo.TargetYear);
      dlgAddPerspective dlg = new dlgAddPerspective("Название");

      dlg.NameSC = "План работы ИнЭО на " + UserInfo.TargetYear + " год";
      dlg.ShowDialog();
      FindReplace("@caption@", dlg.NameSC);

      string f_rnd = _pathReports + "\\" + System.IO.Path.GetRandomFileName();
      _document.SaveAs(f_rnd);
      _document.Close(ref falseObj, ref  missingObj, ref missingObj);
      _application.Quit(ref missingObj, ref  missingObj, ref missingObj);

      return f_rnd;
    }
    public string MakeReportIDO(CMySql mySql, string f_name, string pathToTemplate)
    {
      //возвращает случайное имя файла
      //      if (!Functions.DeleteFile(_pathReports + "\\" + f_name)) return;

      try
      {
        _application = new Word.Application();
        // создаем путь к файлу
        Object templatePathObj = pathToTemplate;
        // если вылетим на этом этапе, приложение останется открытым
        _document = _application.Documents.Add(ref  templatePathObj, ref missingObj, ref missingObj, ref missingObj);
      }
      catch (Exception error)
      {
        _document.Close(ref falseObj, ref  missingObj, ref missingObj);
        _application.Quit(ref missingObj, ref  missingObj, ref missingObj);
        _document = null;
        _application = null;
        throw error;
      }
      _application.Visible = true;
      Word.Table table = _document.Tables[1];
      _mergeData = new List<MergeData>();

      //form caption
      _mergeData.Add(new MergeData() { r_from = 1, c_from = 1, r_to = 2, c_to = 1 });
      _mergeData.Add(new MergeData() { r_from = 1, c_from = 2, r_to = 2, c_to = 2 });//Стратегическая задача 
      _mergeData.Add(new MergeData() { r_from = 1, c_from = 3, r_to = 2, c_to = 3 });//Планируемая деятельность
      _mergeData.Add(new MergeData() { r_from = 1, c_from = 4, r_to = 2, c_to = 4 });//Плановый результат мероприятий, показатель
      _mergeData.Add(new MergeData() { r_from = 1, c_from = 5, r_to = 2, c_to = 5 });//Срок начала/окончания работы
      _mergeData.Add(new MergeData() { r_from = 1, c_from = 6, r_to = 2, c_to = 6 });//Исполнитель (и)

      if (UserInfo.TargetYear == "2012") _mergeData.Add(new MergeData() { r_from = 1, c_from = 9, r_to = 2, c_to = 9 });
      else _mergeData.Add(new MergeData() { r_from = 1, c_from = 11, r_to = 2, c_to = 11 });//Отчёт по отклонениям

      if (UserInfo.TargetYear == "2012") _mergeData.Add(new MergeData() { r_from = 1, c_from = 7, r_to = 1, c_to = 8 });
      else _mergeData.Add(new MergeData() { r_from = 1, c_from = 7, r_to = 1, c_to = 10 });//Отметка о динамике показателя

      DataTable dt_persp = BSCtree.SelectFromTreeAndSC(mySql, "0", -1);

      int counter = 1;
      Word.Row row = table.Rows[3]; //первая строка для заполнения
      foreach (DataRow dr in dt_persp.Rows)
      {
        row.Cells[1].Range.Text = "Стратегическая цель №" + counter.ToString() + ": " + dr["name1"].ToString();
        _mergeData.Add(new MergeData() { r_from = row.Index, c_from = 1, r_to = row.Index, c_to = row.Cells.Count });

        row = table.Rows.Add();
        //       try
        {
          //  if (UserInfo.Info.id_form == UserInfo.id_head_all_department) 
          //   FillScorecardBranch(1, dr["id_SC"].ToString(), mySql, false, true, false);
          //  else FillScorecardBranch(1, dr["id_SC"].ToString(), mySql, false, isReport, true);
        }
        //       catch (Exception exc)
        {
          //        MessageBox.Show("FillScorecardBranch: " + exc.Message);
        }
        object beforeRow = table.Rows[1];

        row = table.Rows.Add();

        counter++;
      }
      foreach (MergeData mg in _mergeData)
      {
        if ((mg.r_from != mg.r_to) | (mg.c_from != mg.c_to))
        {
          try
          {
            table.Cell(mg.r_from, mg.c_from).Merge(table.Cell(mg.r_to, mg.c_to));
          }
          catch
          {
            //          MessageBox.Show(mg.ToString());
          }

        }
      }
      //      _mergeData.Add(new MergeData() { r_from = 1, c_from = 1, r_to = 1, c_to = 2 });
      Word.Table table1 = _document.Tables[1];
      //      table1.Cell(1, 1).Merge(table.Cell(1, 2));
      FindReplace("@year@", UserInfo.TargetYear);
      dlgAddPerspective dlg = new dlgAddPerspective("Название");

      dlg.NameSC = "Отчёт о работе Института дистанционного образования в " + UserInfo.TargetYear + " году";
      dlg.ShowDialog();
      FindReplace("@caption@", dlg.NameSC);

      string f_rnd = _pathReports + "\\" + System.IO.Path.GetRandomFileName();
      _document.SaveAs(f_rnd);
      _document.Close(ref falseObj, ref  missingObj, ref missingObj);
      _application.Quit(ref missingObj, ref  missingObj, ref missingObj);

      return f_rnd;
    }
    class RowData
    {
      public int index { get; set; }
      public string id_parent { get; set; }
    }
    class MergeData
    {
      public MergeData parent = null;
      public int r_from { get; set; }
      public int c_from { get; set; }
      public int r_to { get; set; }
      public int c_to { get; set; }
      public string txt { get; set; }

      override public string ToString()
      {
        return "c_from = " + c_from + "\r\n" + "c_to = " + c_to + "\r\n" + "r_from = " + r_from + "\r\n" + "r_to = " + r_to;
      }
    }
  }
}
