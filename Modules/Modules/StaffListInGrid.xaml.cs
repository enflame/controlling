﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для StaffListInGrid.xaml
  /// </summary>
  public partial class StaffListInGrid : UserControl
  {
    public StaffTree.SelectedItemEventHandler StaffTrree_SelectedItem;

    DataTable _dt_staff;
    public StaffListInGrid()
    {
      InitializeComponent();

    }
    public void Set(DataTable dt)
    {
      dt.Columns.Add("IsHighlighter", Type.GetType("System.Int32"));
      int i = 0;
      while (i < dt.Rows.Count)
      {
        dt.Rows[i]["IsHighlighter"] = 0;
        i++;
        if (i < dt.Rows.Count) dt.Rows[i]["IsHighlighter"] = 1;
        i++;
      }
      dg_KPI.ItemsSource = dt.DefaultView;
      _dt_staff = dt;
    }

    GeneralClasses.StaffInfo GetStaffInfo(DataRow dr_s)
    {
      GeneralClasses.StaffInfo StaffInfo = new GeneralClasses.StaffInfo();

      StaffInfo.email = dr_s["EMAIL"].ToString();
      StaffInfo.login = dr_s["LOGIN"].ToString();
      StaffInfo.id_form = dr_s["LICHNOST_ID"].ToString();
      StaffInfo.id_position = dr_s["id_position"].ToString();
      StaffInfo.fio = dr_s["fio"].ToString();
      StaffInfo.position = dr_s["DOLGNOST"].ToString();
      StaffInfo.id_dep = dr_s["id_dep"].ToString();
      StaffInfo.dep = dr_s["dep_name"].ToString();

      return StaffInfo;
    }
    private void dg_KPI_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      if (dg_KPI.SelectedIndex == -1) return;

      if (StaffTrree_SelectedItem != null)
      {
        DataRow dr_s = _dt_staff.Rows[dg_KPI.SelectedIndex];
        StaffTree.SelectedItemEventArgs args = new StaffTree.SelectedItemEventArgs();
        args.staffInfo = GetStaffInfo(dr_s);
        args.isMouseDoubleClick = true;

        StaffTrree_SelectedItem(sender, args);
      }
    }

    private void dg_KPI_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (dg_KPI.SelectedIndex == -1) return;

      if (StaffTrree_SelectedItem != null)
      {
        DataRow dr_s = _dt_staff.Rows[dg_KPI.SelectedIndex];
        StaffTree.SelectedItemEventArgs args = new StaffTree.SelectedItemEventArgs();
        args.staffInfo = GetStaffInfo(dr_s);
        StaffTrree_SelectedItem(sender, args);
      }

    }
  }
}
