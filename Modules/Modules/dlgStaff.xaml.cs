﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MySql;
namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для dlgStaff.xaml
  /// </summary>
  public partial class dlgStaff : Window
  {
    StaffTree.SelectedItemEventArgs _args;
    CMySql _mySql;
    System.Windows.Threading.DispatcherTimer timer;

    public dlgStaff(StaffTree.SelectedItemEventArgs args, CMySql mySql)
    {
      InitializeComponent();
      _args = args;
      _mySql = mySql;
    }
    public void ShowControl(bool isShow)
    {
      staffProperties.ShowControl(isShow);
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      staffProperties.Set(_args, _mySql);

      timer = new System.Windows.Threading.DispatcherTimer();

      timer.Tick += new EventHandler(timerTick);
      timer.Interval = new TimeSpan(0, 0, 1);
      timer.Start();
    }
    private void timerTick(object sender, EventArgs e)
    {
      staffProperties.Set(_args, _mySql);
      timer.Stop();
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }

    private void staffProperties_Loaded(object sender, RoutedEventArgs e)
    {
      staffProperties.Set(_args, _mySql);
    }
  }
}
