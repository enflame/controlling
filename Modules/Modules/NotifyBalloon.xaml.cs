﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MySql;
namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для NotifyBalloon.xaml
  /// </summary>
  /// 
  public partial class NotifyBalloon : UserControl
  {
    CMySql _mySql;
    public NotifyBalloon()
    {
      InitializeComponent();
    }
    public void Set(CMySql mySql)
    {
      _mySql = mySql;
    }
    public void SetMessages(int countMessages)
    { 
    }

    private void OnButtonClick(object sender, RoutedEventArgs e)
    {
    }

    private void b_Messages_Click(object sender, RoutedEventArgs e)
    {
      dlgMessages dlg = new dlgMessages(_mySql);

      dlg.ShowDialog();

    }
  }
}
