﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Modules;
using MySql;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для dlgSelectStaff.xaml
  /// </summary>
  public partial class dlgSelectStaff : Window
  {
    bool isClosing;
    StaffTree.SelectedItemEventArgs staff_args;
    public dlgSelectStaff(CMySql mySql, SelectStaffEventHandler selectStaffEvent)
    {
      InitializeComponent();

      SelectStaffEvent += selectStaffEvent;
      staffTree.SelectedStaff += new StaffTree.SelectedItemEventHandler(staffTree_SelectedStaff);
      staff_args = null;
      staffTree.Set(mySql);
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      isClosing = true;
    }

    private void Window_Deactivated(object sender, EventArgs e)
    {
      if (!isClosing) Close();
    }

    private void b_Select_Click(object sender, RoutedEventArgs e)
    {
      if (staff_args != null) OnSelectStaffEvent(staff_args);
      if (!isClosing) Close();
    }
    void staffTree_SelectedStaff(Object sender, StaffTree.SelectedItemEventArgs args)
    {
      staff_args = args;
      if (args.isMouseDoubleClick)
      {
        OnSelectStaffEvent(staff_args);
        Close();
      }

    }
    #region События для передачи данных в родительское окно
    public delegate void SelectStaffEventHandler(Object sender, StaffTree.SelectedItemEventArgs args);
    public event SelectStaffEventHandler SelectStaffEvent;
    protected virtual void OnSelectStaffEvent(StaffTree.SelectedItemEventArgs e)
    {
      if (SelectStaffEvent != null)
      {
        SelectStaffEvent(this, e);
      }
    }

    #endregion События
  }
}
