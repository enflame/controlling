﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using System.Windows.Interop;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для ChildElements.xaml
  /// </summary>
  public partial class ChildElements : Window
  {
    bool isClosing;

    public ChildElements(DataTable dt, ChildElementdEventHandler OnChildElementsEvent)
    {
      InitializeComponent();
      //HwndSource source = HwndSource.FromVisual(owner) as HwndSource;
      //if (source != null)
      //{
      //  WindowInteropHelper helper = new WindowInteropHelper(this);
      //  helper.Owner = source.Handle;
      //}

      foreach (DataRow dr in dt.Rows)
      {
        Button b = new Button();
        b.Name = "b" + dr["id"].ToString();
        b.Content = dr["name1"].ToString();
        b.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
        b.Click += new RoutedEventHandler(b_ExistingElement_Click);
        b.Style = (Style)this.Resources["ButtonStyle"];
        SPanel.Children.Add(b);
      }
      isClosing = false;
      ChildElementsEvent += OnChildElementsEvent;
    }

    private void b_ExistingElement_Click(object sender, RoutedEventArgs e)
    {
      Button b = (Button)e.Source;
      string id = b.Name;
      id = id.Substring(1);

      ChildElementsEventArgs args = new ChildElementsEventArgs();
      args.idParent = Convert.ToInt32(id);
      args.Name = (string)b.Content;
      OnChildElementsEvent(args);
    }
    private void b_newElement_Click(object sender, RoutedEventArgs e)
    {
      ChildElementsEventArgs args = new ChildElementsEventArgs();
      args.idParent = -1;
      OnChildElementsEvent(args);
    }
    private void b_Close_Click(object sender, RoutedEventArgs e)
    {
      if (!isClosing) Close();
    }
    private void Window_Deactivated(object sender, EventArgs e)
    {
      if (!isClosing) Close();
    }
    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      isClosing = true;
    }

    #region События для передачи данных в дочернюю панель
    public class ChildElementsEventArgs : EventArgs
    {
      int _idParent;
      string _name;
      public int idParent
      {
        get { return _idParent; }
        set { _idParent = value; }
      }
      public string Name
      {
        get { return _name; }
        set { _name = value; }
      }
      public ChildElementsEventArgs()
      {
        idParent = -1;
      }
    }
    public delegate void ChildElementdEventHandler(Object sender, ChildElementsEventArgs args);
    public event ChildElementdEventHandler ChildElementsEvent;
    protected virtual void OnChildElementsEvent(ChildElementsEventArgs e)
    {
      if (ChildElementsEvent != null)
      {
        ChildElementsEvent(this, e);
      }
    }

    #endregion События

  }
}
