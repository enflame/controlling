﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using MySql;
using GeneralClasses;

namespace Modules
{
  /// <summary>
  /// Логика взаимодействия для dlgSelectFromNavigator.xaml
  /// </summary>
  public partial class dlgSelectFromNavigator : Window
  {
    bool isClosing;
    CMySql _mySql;
    public SelectedItemEventArgs nav_args;
    public static int TargetYear = 0;
    int _TargetYear { get { return dlgSelectFromNavigator.TargetYear; } }
    public dlgSelectFromNavigator(CMySql mySql, int typeHeader, bool isAllInTree)
    {
      InitializeComponent();
      _mySql = mySql;
      tView.Items.Clear();
      nav_args = null;
      if (dlgSelectFromNavigator.TargetYear == 0) dlgSelectFromNavigator.TargetYear = Convert.ToInt32(UserInfo.TargetYear);
      //      Set(typeHeader, isAllInTree);
      string sql = "select distinct(targetyear) from sc";
      DataTable dt = _mySql.SqlSelect(sql);
      foreach (DataRow dr in dt.Rows) cb_targetyear.Items.Add(dr[0].ToString());
      cb_targetyear.SelectedItem = _TargetYear.ToString();

      FillScorecardTree();
    }

    void FillBranchKPI(TreeViewItem root, int level, bool isAllInTree)
    {
      string sql = "";
      if (isAllInTree) sql = "select * from SC where level1=" + level.ToString() + " and targetyear = " + _TargetYear;
      else sql = "select * from SC where level1=" + level.ToString() + " and isInTree = " + Convert.ToInt32(isAllInTree).ToString() + " and targetyear = " + _TargetYear;
      DataTable dt_SC = _mySql.SqlSelect(sql);
      foreach (DataRow dr_SC in dt_SC.Rows)
      {
        TreeViewItem node = new TreeViewItem();
        TreeViewItemHeader h = null;
        if (level == TypeHeader.Perspective)
        {
          h = new TreeViewItemHeader("/Controlling;component/Ico/stock_mail_flag_for_followup_32.png", dr_SC["name1"].ToString(), TypeHeader.Goal, root);
        }
        if (level == TypeHeader.Goal) h = new TreeViewItemHeader("/Controlling;component/Ico/target_32.png", dr_SC["name1"].ToString(), TypeHeader.Goal, root);
        if (level == TypeHeader.Score) h = new TreeViewItemHeader("/Controlling;component/Ico/target_22.png", dr_SC["name1"].ToString(), TypeHeader.Score, root);

        node.Header = h; node.Name = "n" + dr_SC["id"].ToString();
        node.Tag = root;
        node.MouseDoubleClick += new MouseButtonEventHandler(TreeViewItem_MouseDoubleClick);
        root.Items.Add(node);
      }
    }

    private void TreeViewItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      TreeViewItemHeader h = (TreeViewItemHeader)(sender);

      nav_args = new SelectedItemEventArgs();
      nav_args.id_SC = h.Name.Substring(1);
      nav_args.name_SC = h.Text;
      nav_args.typeItem = h.TypeItem;

      tb_SCName.Text = h.Text;
    }
    private void TreeViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      TreeViewItem item = (TreeViewItem)e.Source;
      TreeViewItemHeader h = (TreeViewItemHeader)((item).Header);

      nav_args = new SelectedItemEventArgs();
      nav_args.id_SC = item.Name.Substring(1);
      nav_args.name_SC = h.Text;
      nav_args.typeItem = h.TypeItem;


      OnSelectedItem(nav_args);
      //      DialogResult = true;
    }

    public void Set(int type, bool isOnlyinTree)
    {
      //_mySql = mySql;

      TreeViewItem root = null;
      TreeViewItemHeader h = null;
      if ((type == TypeHeader.Perspective) | (type == TypeHeader.All))
      {
        root = new TreeViewItem(); root.Tag = null;
        h = new TreeViewItemHeader("/Controlling;component/Ico/stock_mail_flag_for_followup_32.png", "Стратегические цели", TypeHeader.Perspective, null);
        root.Header = h;
        root.Name = "n0";
        tView.Items.Add(root);
        FillBranchKPI(root, TypeHeader.Perspective, isOnlyinTree);
      }
      if ((type == TypeHeader.Goal) | (type == TypeHeader.All))
      {
        root = new TreeViewItem(); root.Tag = null;
        h = new TreeViewItemHeader("/Controlling;component/Ico/stock_mail_flag_for_followup_32.png", "Цели", TypeHeader.Goal, null);
        root.Header = h;
        root.Name = "n0";
        tView.Items.Add(root);
        FillBranchKPI(root, TypeHeader.Goal, isOnlyinTree);
      }
      if ((type == TypeHeader.Score) | (type == TypeHeader.All))
      {
        root = new TreeViewItem(); root.Tag = null;
        h = new TreeViewItemHeader("/Controlling;component/Ico/stock_mail_flag_for_followup_32.png", "Показатели", TypeHeader.Score, null);
        root.Header = h;
        root.Name = "n0";
        tView.Items.Add(root);
        FillBranchKPI(root, TypeHeader.Score, isOnlyinTree);
      }

      ((TreeViewItem)tView.Items[0]).IsExpanded = true;
    }
    void FillScorecardBranch(TreeViewItem root, string id_root, string id_perspective)
    {
      string sql = "select SCTree.id_parent, SCTree.id_SC, SC.name1, SC.level1, SC.IsApproved, SC.UNIT , SC.IDCONTROLPERSON, SC.IDRESPONSIBLEPERSON, " +
                  "U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO " +
                  "from SCTree " +
                  "left join SC on SC.ID = SCTree.id_SC " +
                  "left join app_portal.v_tpu_users U on U.LICHNOST_ID = SC.IDCONTROLPERSON " +
                   "where SCTree.id_parent=" + id_root + " and SCTree.targetyear = " + _TargetYear + " order by SCTree.id";
      DataTable dt = _mySql.SqlSelect(sql);

      foreach (DataRow dr_SCTree in dt.Rows)
      {

        TreeViewItem node = new TreeViewItem();
        TreeViewItemHeader h = null;
        int level1 = _mySql.ToInt32(dr_SCTree["Level1"]);

        if (level1 == TypeHeader.Goal)
        {
          h = new TreeViewItemHeader("/Controlling;component/Ico/SCNotApproved.png", dr_SCTree["name1"].ToString(), TypeHeader.Goal, root);
        }
        if (level1 == TypeHeader.Score)
        {
          h = new TreeViewItemHeader("/Controlling;component/Ico/SCNotApproved.png", dr_SCTree["name1"].ToString(), TypeHeader.Score, root);
        }
        if (_mySql.ToBool(dr_SCTree["IsApproved"]))
        {
          h.ImgSource = "/Controlling;component/Ico/SCApproved.png";
          if (node.ContextMenu != null) node.ContextMenu.Visibility = System.Windows.Visibility.Collapsed;
        }

        h.Name = "n" + dr_SCTree["id_SC"].ToString();
        node.Header = h; node.Name = "n" + dr_SCTree["id_SC"].ToString();
        node.Tag = root;
        node.MouseDoubleClick += new MouseButtonEventHandler(TreeViewItem_MouseDoubleClick);
        h.MouseLeftButtonDown += new MouseButtonEventHandler(TreeViewItem_MouseLeftButtonDown);

        root.Items.Add(node);

        FillScorecardBranch(node, dr_SCTree["id_SC"].ToString(), id_perspective);
      }
    }
    public void FillScorecardTree()
    {
      tView.Items.Clear();

      string sql = "select SCTree.id_parent, SCTree.id_SC, SC.name1, SC.level1, SC.IsApproved, SC.UNIT , SC.IDCONTROLPERSON, SC.IDRESPONSIBLEPERSON, " +
                  "U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO " +
                  "from SCTree " +
                  "left join SC on SC.ID = SCTree.id_SC " +
                  "left join app_portal.v_tpu_users U on U.LICHNOST_ID = SC.IDCONTROLPERSON " +
                  "where SCTree.id_parent=0 and SCTree.targetyear = " + _TargetYear + " order by SCTree.id";
      DataTable dt = _mySql.SqlSelect(sql);
      foreach (DataRow dr in dt.Rows)
      {


        TreeViewItem root = new TreeViewItem(); root.Tag = null;
        TreeViewItemHeader h = new TreeViewItemHeader("/Controlling;component/Ico/stock_mail_flag_for_followup_32.png", dr["name1"].ToString(), TypeHeader.Perspective, null);
        root.Header = h; root.Name = "n" + dr["id_SC"].ToString();
        h.Name = "n" + dr["id_SC"].ToString();

        root.MouseDoubleClick += new MouseButtonEventHandler(TreeViewItem_MouseDoubleClick);
        h.MouseLeftButtonDown += new MouseButtonEventHandler(TreeViewItem_MouseLeftButtonDown);

        tView.Items.Add(root);

        FillScorecardBranch(root, dr["id_SC"].ToString(), dr["id_SC"].ToString());
        root.IsExpanded = true;
      }
      if (tView.Items.Count > 0) ((TreeViewItem)tView.Items[0]).IsExpanded = true;
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      isClosing = true;
    }
    private void Window_Deactivated(object sender, EventArgs e)
    {
      if (!isClosing) Close();
    }
    private void b_Select_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = true;
    }
    void Navig_SelectedItem(Object sender, SelectedItemEventArgs args)
    {
      nav_args = args;
      //OnSelectKPIEvent(kpi_args);
    }

    private void b_Close_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = false;
    }


    private void tView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      //реакция на выделение показателя
      TreeViewItem item = (TreeViewItem)e.NewValue;
      TreeViewItemHeader h = (TreeViewItemHeader)((item).Header);

      nav_args = new SelectedItemEventArgs();
      nav_args.id_SC = item.Name.Substring(1);
      nav_args.name_SC = h.Text;
      nav_args.typeItem = h.TypeItem;


      OnSelectedItem(nav_args);
    }
    #region Events
    public class SelectedItemEventArgs : EventArgs
    {
      public string id_SC;
      public string name_SC;

      public object obj;
      public int typeItem;

      public bool isMouseDoubleClick;
    }
    public delegate void SelectedItemEventHandler(Object sender, SelectedItemEventArgs args);
    public event SelectedItemEventHandler SelectedItem;
    protected virtual void OnSelectedItem(SelectedItemEventArgs args)
    {
      if (SelectedItem != null)
      {
        SelectedItem(this, args);
      }
    }
    #endregion Events

    private void cb_targetyear_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      dlgSelectFromNavigator.TargetYear = Convert.ToInt32((string)cb_targetyear.SelectedItem);
      FillScorecardTree();
    }
  }
}
