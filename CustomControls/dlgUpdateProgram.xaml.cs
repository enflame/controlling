﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using Modules;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.IO;
using MySql;
namespace CustomControls
{
  /// <summary>
  /// Логика взаимодействия для dlgUpdateProgram.xaml
  /// </summary>
  public partial class dlgUpdateProgram : Window
  {
    DataTable dt_programm_update;
    CMySql _mySql;
    public dlgUpdateProgram(CMySql mySql)
    {
      _mySql = mySql;
      InitializeComponent();

      dt_programm_update = _mySql.SqlSelect("select * from programm_update order by id");
      dg_files.ItemsSource = dt_programm_update.DefaultView;

      dt_change_date.Value = DateTime.Now;

      if (dt_programm_update.Rows.Count > 0)
      {
        tb_desc.Text = dt_programm_update.Rows[0]["DESCR"].ToString();
        dt_change_date.Value = (DateTime)dt_programm_update.Rows[0]["change_date"];
      }


  //    dt_programm_update.Columns.Add("LocalPath", Type.GetType("System.String"));
      foreach (DataRow dr in dt_programm_update.Rows)
      {
        dr["FILE_NAME"] = dr["FILE_NAME"].ToString();
      }

    }

    string SelectFile()
    {
      string f = "";
      CommonOpenFileDialog dlg = new CommonOpenFileDialog();
      if (dlg.ShowDialog(this) == CommonFileDialogResult.Ok)
      {
        f = System.IO.Path.GetFileName(dlg.FileName);
        string ext = System.IO.Path.GetExtension(f);
        switch (ext)
        {
          case ".dotx":
            f = "Templates\\" + f;
            break;
          case ".dot":
            f = "Templates\\" + f;
            break;
          default:
            break;

        }
      }
      return f;
    }
    private void b_select_file_Click(object sender, RoutedEventArgs e)
    {
      string f = SelectFile();
      if (f != "")
      {
        int index = dg_files.SelectedIndex;
        dt_programm_update.Rows[index]["FILE_NAME"] = f;
      }
    }

    private void b_update_Click(object sender, RoutedEventArgs e)
    {
      _mySql.SqlExec("delete from programm_update");
      foreach (DataRow dr in dt_programm_update.Rows)
      {
        string sql = "insert into programm_update (FILE_NAME, change_date, File_Source) values('" +
           dr["FILE_NAME"].ToString() + "'," + _mySql.DateToString((DateTime)dt_change_date.Value) + ", :BlobParameter )";
        _mySql.InsertBlobOracle(sql, Directory.GetCurrentDirectory() + "\\" + dr["FILE_NAME"].ToString());
      }
      string sql1 = "update programm_update set descr = '" + tb_desc.Text + "' where id=(select min(id) from programm_update)";
      _mySql.SqlExec(sql1);

      MessageBox.Show("Updates finished");
    }

    private void b_addFile_Click(object sender, RoutedEventArgs e)
    {
      string f = SelectFile();
      if (f != "")
      {
        DataRow dr = dt_programm_update.NewRow();
        dr["FILE_NAME"] = f;
        dt_programm_update.Rows.Add(dr);
      }

    }
  }
}
