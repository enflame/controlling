﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;

using Modules;
using MySql;
using GeneralClasses;

namespace CustomControls
{
	/// <summary>
	/// Логика взаимодействия для dlgSmartTaskEdit.xaml
	/// </summary>
	public partial class dlgSmartTaskEdit : Window
	{
    TaskAttrib taskAttrib;
    CMySql _mySql;


		bool is_form_setup;
		bool is_form_approved;
		bool is_form_checked;
		bool is_form_delegated;
		public dlgSmartTaskEdit(CMySql mySql, StaffInfo staffInfo, string id_smart_task)
		{
			InitializeComponent();

      _mySql = mySql;
      taskAttrib = new TaskAttrib();
      taskAttrib.id_form = staffInfo.id_form;
      taskAttrib.id_dep = staffInfo.id_dep;
			taskAttrib.id_smart_task = id_smart_task;

      tb_form_clerk.Text = staffInfo.fio;
    }

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			if (taskAttrib.id_smart_task == "")
			{
				dp_date_begin.DisplayDate = DateTime.Now;
				dp_date_end.DisplayDate = DateTime.Now;

				dp_date_begin.SelectedDate = DateTime.Now;
				dp_date_end.SelectedDate = DateTime.Now;
			}
			else
			{
        string sql = "select * from staff_kip where id = " + taskAttrib.id_smart_task + " and targetyear = " + UserInfo.TargetYear.ToString();
				DataRow dr = _mySql.SqlSelect(sql).Rows[0];
			}
		}

		private void b_close_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
			Close();
		}


    string GetStaff()
    {
      string res = "";

      dlgSelectStaff wnd = new dlgSelectStaff(_mySql, new dlgSelectStaff.SelectStaffEventHandler(onSelectedStaff));
      Point p = PointToScreen(Mouse.GetPosition(this));
      wnd.Left = p.X;
      wnd.Top = p.Y;

      wnd.Show();

      return res;
    }
    void onSelectedStaff(Object sender, StaffTree.SelectedItemEventArgs args)
    {
        
      if (is_form_setup)
      {
        tb_form_setup.Text = args.staffInfo.fio;
        taskAttrib.id_form_setup = args.staffInfo.id_form;
      }
      if (is_form_checked)
      {
        tb_form_checked.Text = args.staffInfo.fio;
        taskAttrib.id_form_checked = args.staffInfo.id_form;
      }
      if (is_form_approved)
      {
        tb_form_approved.Text = args.staffInfo.fio;
        taskAttrib.id_form_approved = args.staffInfo.id_form;
      }
      if (is_form_delegated)
      {
        //tb_form_setup.Text = args.fio;
        //taskAttrib.id_form_approved = args.id_staff;
      }
    }
    private void b_form_setup_Click(object sender, RoutedEventArgs e)
    {
      is_form_setup = true;
      is_form_approved = false;
      is_form_checked = false;
      is_form_delegated = false;
      GetStaff();
    }
    private void b_form_approved_Click(object sender, RoutedEventArgs e)
    {
      is_form_setup = false;
      is_form_approved = true;
      is_form_checked = false;
      is_form_delegated = false;
      GetStaff();
    }
    private void b_form_checked_Click(object sender, RoutedEventArgs e)
    {
      is_form_setup = false;
      is_form_approved = false;
      is_form_checked = true;
      is_form_delegated = false;
      GetStaff();
    }

		private void b_ok_Click(object sender, RoutedEventArgs e)
		{
			if (taskAttrib.id_smart_task == "")
			{
				string sql = "insert into staff_smart_task (id_form, id_dep, name1, unit, id_form_setup, " + 
					"id_form_approved, id_form_checked, id_form_delegated, isSetup, isApproved, isChecked, isSolved, plan, " +
					"fact, date_begin, date_end, algoritm, end_report, descr, targetyear) values(" + taskAttrib.id_form + ", " + taskAttrib.id_dep + ", '" + tb_name.Text + "', '" + 
					cb_unit.Text + "', " + taskAttrib.id_form_setup + ", " + taskAttrib.id_form_approved + ", " + taskAttrib.id_form_checked + ", " + taskAttrib.id_form_delegated +
					", " + cb_isSetup.IsChecked.ToString() + ", " + cb_isApproved.IsChecked.ToString() + ", " + cb_isChecked.IsChecked.ToString() + ", " + cb_isSolved.IsChecked.ToString() + ", " + UpDown_plan.Value.ToString() +
          ", " + UpDown_fact.Value.ToString() + ", '" + dp_date_begin.Text + "', '" + dp_date_end.Text + "', '" + tb_algoritm.Text + "', '" + tb_end_report.Text + "', " + UserInfo.TargetYear + ", '')";
				_mySql.SqlExec(sql);
			}
			else 
			{
			}

			DialogResult = true;
			Close();
		}


	}

 }
