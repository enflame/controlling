﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Modules;
using MySql;
namespace CustomControls
{
  /// <summary>
  /// Логика взаимодействия для dlgKPIProperties.xaml
  /// </summary>
  public partial class dlgKPIProperties : Window
  {
    public dlgKPIProperties(string idSC, string idSC_root, CMySql mySql, Monitoring monitoring)
    {
      InitializeComponent();
      kPIProperties.Set(idSC, idSC_root, mySql, null);
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
    }
  }
}
