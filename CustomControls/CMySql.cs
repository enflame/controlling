using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Odbc;
using System.Windows;

namespace MySql
{
  public class CMySql
  {
    OdbcConnection odbcCon_;
		OdbcCommand _odbcCom;
    public CMySql()
    {
      odbcCon_ = null;
    }
    public void ConnectToDB(string server, string user, string passw, string db_name, int type_db)
    {
      string connStr;
      switch (type_db)
      {
        case 0: connStr = "Driver={MySQL ODBC 3.51 Driver};Server=" + server +
                                  ";uid=" + user + "; pwd=" + passw + ";Database=" + db_name + ";Trusted_Connection=yes;";
          break;
        //case 1: connStr = "Driver={Microsoft Access Driver (*.mdb)};DBQ=" + db_name+".mdb";
        //        break;
        case 1: connStr = "Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=" + db_name;
          break;
        //  �Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=path to mdb/accdb file� 
        default: return;
      }
      try
      {
        odbcCon_ = new OdbcConnection(connStr);
        odbcCon_.Open();
        _odbcCom = new OdbcCommand();
      }
      catch (Exception exc) 
      {
        string s = exc.Message;
      }
    }

    public DataSet SqlSelect(string sql, string table_name = "")
    {
      DataSet ds;
      ds = new DataSet();

      OdbcCommand odbcCom = new OdbcCommand();
      odbcCom.Connection = odbcCon_;
      odbcCom.CommandText = sql;

      OdbcDataAdapter odbcDA = new OdbcDataAdapter();
      odbcDA.SelectCommand = odbcCom;
      odbcDA.TableMappings.Add(table_name, table_name);
      try
      {
        odbcDA.Fill(ds);
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }
      return ds;
    } 
    public int SqlExec(string sql)
    {
      if (sql == "")
        return -1;
      try
      {
        //OdbcCommand odbcCom = new OdbcCommand();
        _odbcCom.Connection = odbcCon_;
        _odbcCom.CommandType = CommandType.Text;
        _odbcCom.CommandText = sql;

        _odbcCom.ExecuteNonQuery();
        return 1;
      }
      catch (Exception exc)
      {
        odbcCon_.Close();
        return -1;
      }
    }
    public DataRow FindRow(DataTable table, string field, string value)
    {
      for (int i = 0; i < table.Rows.Count; i++)
      {
        if (table.Rows[i][table.Columns["id"]].ToString() == value)
        {
          return table.Rows[i];
        }
      }
      return null;
    }

    public static string MyValidate(string str)
    {
      StringBuilder s = new StringBuilder();
      s.Append(str);
      s.Replace("''", "###");
      s.Replace("'", "''");
      s.Replace("###", "''");

      s.Replace("\0", "###");

      return s.ToString();
    }
		public static string QuotedStr(string str)
		{
			StringBuilder s = new StringBuilder();
			return s.ToString();
		}
	}
}
