﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;

namespace CustomControls
{
  /// <summary>
  /// Логика взаимодействия для dlgSelectLoginUser.xaml
  /// </summary>
  public partial class dlgSelectLoginUser : Window
  {
    public DataRow _dr;
    DataTable _dt;
    public dlgSelectLoginUser(DataTable dt)
    {
      InitializeComponent();

      _dt = dt;
      dg_SelectUser.ItemsSource = dt.DefaultView;
    }

    void SelectUser()
    {
      if (dg_SelectUser.SelectedIndex == -1) return;
      _dr = _dt.Rows[dg_SelectUser.SelectedIndex];
      DialogResult = true;
    }
    private void b_ok_Click(object sender, RoutedEventArgs e)
    {
      SelectUser();
    }

    private void b_cancel_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }

    private void dg_SelectUser_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      SelectUser();
    }
  }
}
