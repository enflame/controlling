﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
namespace CustomControls
{
	/// <summary>
	/// Логика взаимодействия для ChildElements.xaml
	/// </summary>
	public partial class ChildElements : Window
	{
		public ChildElements(DataTable dt)
		{
			InitializeComponent();

			foreach (DataRow dr in dt.Rows)
			{
				Button b = new Button();
				b.Name = "b" + dr["id"].ToString();
				b.Content = dr["name1"].ToString();
				b.Click += new RoutedEventHandler(b_ExistingElement_Click);
				lb.Items.Add(b);
			}
		}

		private void b_ExistingElement_Click(object sender, RoutedEventArgs e)
		{
			Button b = (Button)e.Source;
		}

		private void b_newElement_Click(object sender, RoutedEventArgs e)
		{

		}
	}
}
