﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CustomControls
{
	/// <summary>
	/// Логика взаимодействия для TreeViewItemHeader.xaml
	/// </summary>
	public class TypeHeader
	{
		/// полагаем level1 = 0 - Перспектива
		/// полагаем level1 = 20 - Цель
		/// полагаем level1 = 40 - Показатель
		public static int Perspective = 0;
		public static int Goal = 20;
		public static int Score = 40;

    public static int All = -1;
  }

	public partial class TreeViewItemHeader : UserControl
	{

		public string ImgSource
		{
			set
			{
				img.Source = Functions.ToImageSource(value);
			}
		}
		public string Text
		{
			get { return tb.Text; }
			set
			{
				tb.Text = value;
			}
		}
    public int TypeItem; //TypeHeader

		TreeViewItem ParentNode;

		public TreeViewItemHeader(string imgSource, string text, int typeItem, TreeViewItem parentNode)
		{
			InitializeComponent();

			ImgSource = imgSource;
			Text = text;
			ParentNode = parentNode;
      TypeItem = typeItem;

			if (SelectedItem != null)
			{
        //ContextMenu menu = new ContextMenu();
        //MenuItem item = new MenuItem();
        //item.Header = "Добавить Ключевой/Следящий показатель";
        //item.Click += new RoutedEventHandler(MenuItem_Click);
        //menu.Items.Add(item);
        //this.ContextMenu = menu;
			}
		}

    public void ShowState(bool isShow)
    {
			if (isShow) IndProgress.Visibility = System.Windows.Visibility.Visible;
			else IndProgress.Visibility = System.Windows.Visibility.Collapsed;
    }
    public void ShowPict(bool isShow)
    {
      if (isShow) img.Visibility = System.Windows.Visibility.Visible;
      else img.Visibility = System.Windows.Visibility.Collapsed;
    }

    void MenuItem_Click(object sender, RoutedEventArgs e)
		{
			SelectedItemEventArgs args = new SelectedItemEventArgs();
			args.ParentNode = ParentNode;
			OnSelectedItem(args);
		}
		#region Events
		public class SelectedItemEventArgs : EventArgs
		{
			public TreeViewItem ParentNode;
		}
		public delegate void SelectedItemEventHandler(Object sender, SelectedItemEventArgs args);
		public event SelectedItemEventHandler SelectedItem;
		protected virtual void OnSelectedItem(SelectedItemEventArgs args)
		{
			if (SelectedItem != null)
			{
				SelectedItem(this, args);
			}
		}
		#endregion Events
	}
}
