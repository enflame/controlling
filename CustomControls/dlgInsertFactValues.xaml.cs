﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using Modules;
using MySql;
namespace CustomControls
{
  /// <summary>
  /// Логика взаимодействия для dlgInsertFactValues.xaml
  /// </summary>
  public partial class dlgInsertFactValues : Window
  {
    public dlgInsertFactValues(CMySql mySql)
    {
      InitializeComponent();

      FactValues.Set(mySql);
    }

    private void b_Close_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }
  }
}
