﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.DirectoryServices;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Windows;

namespace ADSI
{
  public class CADSI
  {
    //When you create a new group, you can use flags from the ADS_GROUP_TYPE_ENUMadsi.ads_group_type_enum enumeration to assign a group type to the group, such as global (ADS_GROUP_TYPE_GLOBAL_GROUP), domain local (ADS_GROUP_TYPE_DOMAIN_LOCAL_GROUP), local (ADS_GROUP_TYPE_LOCAL_GROUP), universal (ADS_GROUP_TYPE_UNIVERSAL_GROUP) or security-enabled (ADS_GROUP_TYPE_SECURITY_ENABLED). If you do not specify a group type, the default is to create a global, secured group (ActiveDs.ADS_GROUP_TYPE_ENUM.ADS_GROUP_TYPE_GLOBAL_GROUP | ActiveDs.ADS_GROUP_TYPE_ENUM.ADS_GROUP_TYPE_SECURITY_ENABLED). For more information about the ADS_GROUP_TYPE_ENUMadsi.ads_group_type_enum enumeration, see "ADS_GROUP_TYPE_ENUM" in the MSDN Library at     const int UF_SCRIPT = 0x0001;

    const int UF_ACCOUNTDISABLE = 0x0002;
    const int UF_HOMEDIR_REQUIRED = 0x0008;
    const int UF_LOCKOUT = 0x0010;
    const int UF_PASSWD_NOTREQD = 0x0020;
    const int UF_PASSWD_CANT_CHANGE = 0x0040;
    const int UF_TEMP_DUPLICATE_ACCOUNT = 0x0100;
    const int UF_NORMAL_ACCOUNT = 0x0200;
    const int UF_ENCRYPTED_TEXT_PASSWORD_ALLOWED = 128;    // 0x80
    const int UF_INTERDOMAIN_TRUST_ACCOUNT = 2048;    // 0x800
    const int UF_WORKSTATION_TRUST_ACCOUNT = 4096;    // 0x1000
    const int UF_SERVER_TRUST_ACCOUNT = 8192;    // 0x2000
    const int UF_DONT_EXPIRE_PASSWD = 65536;    // 0x10000
    const int UF_MNS_LOGON_ACCOUNT = 131072;    // 0x20000
    const int UF_SMARTCARD_REQUIRED = 262144;    // 0x40000
    const int UF_TRUSTED_FOR_DELEGATION = 524288;    // 0x80000
    const int UF_NOT_DELEGATED = 1048576;    // 0x100000
    const int UF_USE_DES_KEY_ONLY = 2097152;    // 0x200000
    const int UF_DONT_REQUIRE_PREAUTH = 4194304;    // 0x400000
    const int UF_PASSWORD_EXPIRED = 8388608;    // 0x800000
    const int UF_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION = 16777216;    // 0x1000000

    public string domen_;
    public string adsi_path_ = "";
    public string domen_admin_ = "";
    public string domen_admin_pwd_ = "";
    string filterAttribute_;
    public string error_;

    string _connectS;
    public CADSI(string domen, string domen_admin, string domen_admin_pwd)
    {
      _connectS = "LDAP://" + domen + "/";
      string path = "";
      string[] a_str = domen.Split('.');
      if (a_str.Length == 0) return;
      foreach (string s in a_str) path = path + "DC=" + s + ",";
      adsi_path_ = path.Remove(path.LastIndexOf(","), 1);

      domen_admin_ = domen_admin;
      domen_admin_pwd_ = domen_admin_pwd;

      //string path = "LDAP://";
      //string[] a_str = domain.Split('.');
      //if (a_str.Length == 0) return;
      //foreach (string s in a_str) path = path + "DC=" + s + ",";
      //path_ = path.Remove(path.LastIndexOf(","), 1);
    }

    SearchResult Find(string name, string filter)
    {
      //      name = "eugene";
      DirectoryEntry entry = new DirectoryEntry(_connectS + adsi_path_, domen_admin_, domen_admin_pwd_);

      DirectorySearcher mySearcher = new System.DirectoryServices.DirectorySearcher(entry);
      mySearcher.Filter = "(" + filter + "=" + name + ")";
      SearchResult resEnt = null;
      try
      {
        resEnt = mySearcher.FindOne();
      }
      catch
      {
        //MessageBox.Show(exc.Message);
      }
      return resEnt;
    }

    public bool SiteSpecificAuthenticationMethod(string username, string pwd)
    {
      DirectoryEntry entry = new DirectoryEntry(adsi_path_, username, pwd);
      //    DirectoryEntry entry = new DirectoryEntry("LDAP://DC=elti,DC=student,DC=tpu,DC=ru", "admin", "fyfrjylf");
      try
      {
        // Bind to the native AdsObject to force authentication. 
        Object obj = entry.NativeObject;
        DirectorySearcher search = new DirectorySearcher(entry);
        search.Filter = "(SAMAccountName=" + username + ")";
        search.PropertiesToLoad.Add("cn");
        SearchResult result = search.FindOne();
        if (null == result)
        {
          return false;
        }
        // Update the new path to the user in the directory
        adsi_path_ = result.Path;
        filterAttribute_ = (String)result.Properties["cn"][0];
        //       name_ = (String)result.Properties["cn"][0];
      }
      catch (Exception ex)
      {
        error_ = "Error authenticating user. " + ex.Message;
        return false;
      }
      return true;
    }
    public string[] FindUser(string SAMAccountName)
    {
      DirectoryEntry entry = new DirectoryEntry(_connectS + adsi_path_, domen_admin_, domen_admin_pwd_);
      DirectorySearcher search = new DirectorySearcher(entry);
      search.Filter = "(SAMAccountName=" + SAMAccountName + ")";
      SearchResult resEnt = null;
      try
      {
        resEnt = search.FindOne();
      }
      catch(Exception exc)
      {
        System.Windows.Forms.MessageBox.Show(exc.Message);
        return null;
      }
      if (resEnt == null)
      {

        return null;
      }
      System.Collections.IEnumerator en = resEnt.Properties.PropertyNames.GetEnumerator();
      List<string> list = new List<string>();
      while (en.MoveNext())
      {
        string curr = en.Current.ToString();
        list.Add(curr);
      }
      return list.ToArray();
    }
    public string GetGroups()
    {
      DirectorySearcher search = new DirectorySearcher(adsi_path_);
      search.Filter = "(cn=" + filterAttribute_ + ")";
      search.PropertiesToLoad.Add("memberOf");
      StringBuilder groupNames = new StringBuilder();
      try
      {
        SearchResult result = search.FindOne();
        int propertyCount = result.Properties["memberOf"].Count;
        String dn;
        int equalsIndex, commaIndex;

        for (int propertyCounter = 0; propertyCounter < propertyCount;
             propertyCounter++)
        {
          dn = (String)result.Properties["memberOf"][propertyCounter];

          equalsIndex = dn.IndexOf("=", 1);
          commaIndex = dn.IndexOf(",", 1);
          if (-1 == equalsIndex)
          {
            return null;
          }
          groupNames.Append(dn.Substring((equalsIndex + 1),
                            (commaIndex - equalsIndex) - 1));
          groupNames.Append("|");
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Error obtaining group names. " + ex.Message);
      }
      return groupNames.ToString();
    }
    //public List<ADUInfo> GetADTree(TreeNode node, string path)
    //{
    //  List<ADUInfo> res = new List<ADUInfo>();
    //  string connectS = _connectS + path;
    //  DirectoryEntry entry = new DirectoryEntry(connectS, domen_admin_, domen_admin_pwd_);

    //  foreach (DirectoryEntry child in entry.Children)
    //  {
    //    string str = child.Name;// +" " + child.Path;
    //    int equalsIndex = child.Name.IndexOf("=", 1);

    //    string ADValue = child.Name.Substring(equalsIndex + 1);
    //    TreeNode node1 = node.Nodes.Add(ADValue);
    //    if (child.Name.Substring(0, equalsIndex) == "OU")
    //    {
    //      string newpath = child.Name + "," + path;
    //      try
    //      {
    //        GetADTree(node1, newpath);
    //      }
    //      catch (Exception exc)
    //      {
    //        //MessageBox.Show(exc.Message + "\r\n" + newpath);
    //      }
    //    }
    //    if (child.Name.Substring(0, equalsIndex) == "CN")
    //    {
    //      //        public string info;
    //      //public string description;
    //      //public string userprincipalname;
    //      //public string samaccountname;
    //      //public string mailnickname;
    //      //string[] str = GetPropertyValue(ADValue, "", "CN")
    //    }
    //  }
    //  return res;
    //}
    public string[] GetListOfNameProperties(string name, string filter)
    {
      SearchResult resEnt = Find(name, filter);
      if (resEnt == null) return null;
      System.Collections.IEnumerator en = resEnt.Properties.PropertyNames.GetEnumerator();
      List<string> list = new List<string>();
      while (en.MoveNext())
      {
        string curr = en.Current.ToString();
        list.Add(curr);
      }
      return list.ToArray();
    }
    public string[] GetUserInfo(string name)
    {
      string[] res = new string[5] { "", "", "", "", "" };
      try
      {
        SearchResult resEnt = Find(name, "cn");
        if (resEnt == null) return null;
        System.Collections.IEnumerator en = resEnt.Properties.PropertyNames.GetEnumerator();
        if (resEnt.Properties["homedrive"].Count > 0) res[0] = resEnt.Properties["homedrive"][0].ToString();
        if (resEnt.Properties["homedirectory"].Count > 0)
        {
          res[1] = resEnt.Properties["homedirectory"][0].ToString();
          res[1] = res[1].Substring(0, res[1].LastIndexOf("\\") + 1);
        }
        res[2] = resEnt.Properties["samAccountName"][0].ToString();
        res[3] = resEnt.Properties["name"][0].ToString();
        if (resEnt.Properties["description"].Count > 0) res[4] = resEnt.Properties["description"][0].ToString();
      }
      catch
      {
        //MessageBox.Show(exc.Message);
      }
      return res;
    }
    public string[] GetPropertyValue(string name, string property, string RDN)
    {
      SearchResult resEnt = Find(name, RDN);
      //    SearchResult resEnt = Find(name, "SAMAccountName");
      if (resEnt == null) return null;

      int propertyCount = resEnt.Properties[property].Count;
      List<string> list = new List<string>();
      for (int propertyCounter = 0; propertyCounter < propertyCount; propertyCounter++)
      {
        string dn = (String)resEnt.Properties[property][propertyCounter].ToString();
        list.Add(dn);
      }

      return list.ToArray();
    }
    public string AddNewUser(string user_from, string Principal, string givenname, string initials, string sn,
                          string login, string pwd, string homedrive, string homedirectory, string description)
    {

      if (homedirectory.IndexOf(login) == -1)
      {
        //MessageBox.Show("Неверная домашняя директория");
        return "";
      }
      string name;
      SearchResult resEntFrom = Find(user_from, "cn");
      if (resEntFrom == null) return "";


      string adspath = resEntFrom.Properties["adspath"][0].ToString();
      string path = adspath.Substring(adspath.IndexOf(",") + 1);
      DirectoryEntry entry = new DirectoryEntry("LDAP://" + path, domen_admin_, domen_admin_pwd_);
      DirectoryEntries entries = entry.Children;
      try
      {
        name = givenname + " " + initials + "." + " " + sn + ".";
        if (initials == "") name = givenname + " " + sn + ".";
        if (sn == "") name = givenname + " " + initials + ".";
        if ((initials == "") & (sn == "")) name = givenname;
        DirectoryEntry obUser = entries.Add("CN=" + name, "User");
        obUser.Properties["userPrincipalName"].Value = Principal;
        obUser.Properties["name"].Value = name;
        obUser.Properties["displayname"].Value = name;
        //       obUser.Properties["cn"].Value = name;
        obUser.Properties["givenname"].Value = givenname;
        if (initials != "") obUser.Properties["initials"].Value = initials + ".";
        if (sn != "") obUser.Properties["sn"].Value = sn + ".";
        obUser.Properties["samAccountName"].Value = login;
        obUser.Properties["userAccountControl"].Value = UF_PASSWD_NOTREQD | UF_NORMAL_ACCOUNT | UF_DONT_EXPIRE_PASSWD;
        if (homedrive != "") obUser.Properties["homedrive"].Value = homedrive;
        if (homedirectory != "") obUser.Properties["homedirectory"].Value = homedirectory;
        if (description != "") obUser.Properties["description"].Value = description;


        obUser.CommitChanges();

        object[] password = new object[] { pwd };
        object obRet = obUser.Invoke("setPassword", password);

        //adding to groups
        SearchResult resEntNew = Find(name, "cn");
        if (resEntNew == null) return "";
        int count = resEntFrom.Properties["memberOf"].Count;
        for (int i = 0; i < count; i++)
        {
          string group = resEntFrom.Properties["memberOf"][i].ToString();
          int commaIndex = group.IndexOf(",", 1);
          string g = group.Substring(0, commaIndex);
          string g1 = group.Substring(commaIndex + 1);

          DirectoryEntry group_entry = new DirectoryEntry("LDAP://" + g1, domen_admin_, domen_admin_pwd_);
          DirectoryEntry grp = group_entry.Children.Find(g, "group");
          grp.Properties["member"].Add(resEntNew.Properties["distinguishedName"][0].ToString());
          grp.CommitChanges();
          //         group_entry.CommitChanges();
        }
      }
      catch
      {
        //MessageBox.Show(exc.Message);
        return "";
      }


      try
      {
        DirectoryInfo dirinfo = Directory.CreateDirectory(homedirectory);
        MyDirectorySecurity.RemoveInheritablePermissons(homedirectory);
        MyDirectorySecurity.RemoveAllPermissons(homedirectory);

        FileSystemRights fsr = FileSystemRights.ReadData | FileSystemRights.WriteData | FileSystemRights.AppendData |
          FileSystemRights.ReadExtendedAttributes | FileSystemRights.WriteExtendedAttributes | FileSystemRights.ExecuteFile |
          FileSystemRights.ReadAttributes | FileSystemRights.WriteAttributes | FileSystemRights.Delete | FileSystemRights.ReadPermissions |
          FileSystemRights.Synchronize;

        MyDirectorySecurity.AddDirectorySecurity(homedirectory, domen_ + @"\Domain Admins", fsr,
          InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);

        fsr = FileSystemRights.ReadData | FileSystemRights.WriteData | FileSystemRights.AppendData |
          FileSystemRights.ReadExtendedAttributes | FileSystemRights.WriteExtendedAttributes | FileSystemRights.ExecuteFile |
          FileSystemRights.ReadAttributes | FileSystemRights.WriteAttributes | FileSystemRights.Delete | FileSystemRights.ReadPermissions |
          FileSystemRights.Synchronize;
        MyDirectorySecurity.AddDirectorySecurity(homedirectory, domen_ + @"\" + login, fsr,
          InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);
      }
      catch
      {
        //MessageBox.Show(exc.Message);
        return "";
      }
      return name;
    }
    public bool DeleteUser(string name)
    {
      try
      {
        SearchResult Ent = Find(name, "cn");
        if (Ent == null) return false;
        string dn = Ent.Properties["distinguishedName"][0].ToString();
        string homedirectory = "";
        if (Ent.Properties["homedirectory"] != null)
          if (Ent.Properties["homedirectory"][0] != null) homedirectory = Ent.Properties["homedirectory"][0].ToString();
        int commaIndex = dn.IndexOf(",", 1);
        //string g = group.Substring(0, commaIndex);
        string g1 = dn.Substring(commaIndex + 1);

        DirectoryEntry entry_parent = new DirectoryEntry("LDAP://" + g1, domen_admin_, domen_admin_pwd_);
        DirectoryEntry entry_child = new DirectoryEntry("LDAP://" + dn, domen_admin_, domen_admin_pwd_);
        entry_parent.Children.Remove(entry_child);
        //grp.Properties["member"].Add(Ent.Properties["distinguishedName"][0].ToString());
        //grp.CommitChanges();
        //         group_entry.CommitChanges();

        if (homedirectory != "") Directory.Delete(homedirectory, true);
      }
      catch
      {
        //MessageBox.Show(exc.Message);
        return false;
      }
      return true;
    }
    public bool ChangePassword(string name, string new_passw)
    {
      SearchResult resEnt = Find(name, "cn");
      if (resEnt == null) return false;
      object[] password = new object[] { new_passw };
      try
      {
        string adspath = resEnt.Properties["adspath"][0].ToString();
        DirectoryEntry entry = new DirectoryEntry(adspath, domen_admin_, domen_admin_pwd_);
        object o = entry.Invoke("setPassword", password);
      }
      catch
      {
        //MessageBox.Show(exc.Message);
        return false;
      }
      return true;
    }
  }
  public class ADUInfo
  {
    public string info;
    public string description;
    public string userprincipalname;
    public string samaccountname;
    public string mailnickname;

  }
  class MyDirectorySecurity
  {
    public static void RemoveInheritablePermissons(string FileName)
    {
      // Create a new DirectoryInfo object. 
      DirectoryInfo dInfo = new DirectoryInfo(FileName);
      // Get a DirectorySecurity object that represents the  
      // current security settings. 
      DirectorySecurity dSecurity = dInfo.GetAccessControl();
      // Add the FileSystemAccessRule to the security settings. 
      const bool IsProtected = true;
      const bool PreserveInheritance = false;
      dSecurity.SetAccessRuleProtection(IsProtected, PreserveInheritance);
      // Set the new access settings. 
      dInfo.SetAccessControl(dSecurity);
    }
    public static void RemoveAllPermissons(string FileName)
    {
      DirectoryInfo dInfo = new DirectoryInfo(FileName);
      DirectorySecurity ds = dInfo.GetAccessControl();
      foreach (FileSystemAccessRule fsr in ds.GetAccessRules(true, true, typeof(NTAccount)))
      {
        ds.RemoveAccessRule(fsr);
      }
      dInfo.SetAccessControl(ds);
    }
    public static void RemoveDirectorySecurity(string FileName, string Account, FileSystemRights Rights,
                                               AccessControlType ControlType)
    {
      // Create a new DirectoryInfo object. 
      DirectoryInfo dInfo = new DirectoryInfo(FileName);
      // Get a DirectorySecurity object that represents the  
      // current security settings. 
      DirectorySecurity dSecurity = dInfo.GetAccessControl();
      // Add the FileSystemAccessRule to the security settings.  
      dSecurity.RemoveAccessRule(new FileSystemAccessRule(Account,
                                                          Rights,
                                                          ControlType));
      // Set the new access settings. 
      dInfo.SetAccessControl(dSecurity);
    }
    public static void AddDirectorySecurity(string FileName, string Account, FileSystemRights Rights,
                                            InheritanceFlags Inheritance, PropagationFlags Propogation,
                                            AccessControlType ControlType)
    {
      // Create a new DirectoryInfo object. 
      DirectoryInfo dInfo = new DirectoryInfo(FileName);
      // Get a DirectorySecurity object that represents the  
      // current security settings. 
      DirectorySecurity dSecurity = dInfo.GetAccessControl();
      // Add the FileSystemAccessRule to the security settings.  
      dSecurity.AddAccessRule(new FileSystemAccessRule(Account,
                                                       Rights,
                                                       Inheritance,
                                                       Propogation,
                                                       ControlType));
      // Set the new access settings. 
      dInfo.SetAccessControl(dSecurity);
    }
    //{ 
    //    string ADDomain = "Domain"; 
    //    string ADUser = "Chris"; 
    //    string Path = @"c:\temp\chris"; 

    //    if (Directory.Exists(Path) == false) 
    //        Directory.CreateDirectory(Path); 

    //    // Remove any inheritable permissions from the path 
    //    RemoveInheritablePermissons(Path); 

    //    // Add the access control entries for the path 
    //    AddDirectorySecurity(Path, ADDomain + "\\" + ADUser, FileSystemRights.Modify, 
    //                         InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, 
    //                         PropagationFlags.None, AccessControlType.Allow); 
    //    AddDirectorySecurity(Path, ADDomain + "\\Domain Users", FileSystemRights.Delete, InheritanceFlags.None, 
    //                         PropagationFlags.None, AccessControlType.Deny); 
    //    AddDirectorySecurity(Path, ADDomain + "\\Domain Admins", FileSystemRights.FullControl, 
    //                         InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, 
    //                         PropagationFlags.None, AccessControlType.Allow); 
    //} 
  }
}
