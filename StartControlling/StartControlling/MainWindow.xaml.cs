﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Diagnostics;
using System.IO;
using System.Data;
using System.Data.Odbc;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
namespace StartControlling
{
  /// <summary>
  /// Логика взаимодействия для MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    CMySql _mySql;
    //string _fileName;
    DataTable _dt;

    DateTime last_update_date;
    public MainWindow()
    {
      InitializeComponent();
      _mySql = new CMySql();
      if (_mySql.ConnectToDB("", "", "", Directory.GetCurrentDirectory() + "\\base\\Controlling.accdb", 1) == -1) Close();

      if (File.Exists("update_settings")) last_update_date = DateTime.Parse(File.ReadAllText("update_settings"));
      else
      {
        last_update_date = DateTime.Now - new TimeSpan(1,0,0,0);
        File.WriteAllText("update_settings", DateTime.Now.ToShortDateString());
      }
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      _dt = _mySql.SqlSelect("select * from programm_update where change_date > to_date('" + _mySql.ToDateTime(last_update_date.ToShortDateString()) + "', 'dd.MM.yyyy') order by id").Tables[0];
      if (_dt.Rows.Count > 0)
      {
        dg_files.ItemsSource = _dt.DefaultView;
        tb_descr.DataContext = _dt.DefaultView;
      }
      else
      {
        ProcessStartInfo info = new ProcessStartInfo("controlling.exe");
        Process.Start(info);
        Close();
      }
    }
    private void b_update_Click(object sender, RoutedEventArgs e)
    {
      string start_file = "controlling.exe";
      foreach (DataRow row in _dt.Rows)
      {
        string file_name = Directory.GetCurrentDirectory() + "\\" + row["file_name"].ToString();
        try
        {
          if (File.Exists(file_name)) File.Delete(file_name);
          FileStream FS = new FileStream(file_name, FileMode.Create);
          byte[] blob = (byte[])row["file_source"];
          FS.Write(blob, 0, blob.Length);
          FS.Close();
          FS = null;

          if (file_name.Substring(file_name.Length - 3) == "exe") start_file = file_name;
          if (file_name.Substring(file_name.Length - 3) == "msi") start_file = file_name;
        }
        catch (Exception exc)
        {
          MessageBox.Show(exc.Message);
          Close();
        }
      }

      File.WriteAllText("update_settings", ((DateTime)_dt.Rows[0]["change_date"]).ToShortDateString());
      if (start_file == "StartControlling.exe")
      {
      }
      else
      {
        ProcessStartInfo info = new ProcessStartInfo(start_file);
        Process.Start(info);
      }

      Close();
    }
    private void b_close_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }
    //private void updatedata()
    //{
    //  //use filestream object to read the image. 
    //  //read to the full length of image to a byte array. 
    //  //add this byte as an oracle parameter and insert it into database. 
    //  try
    //  {
    //    //proceed only when the image has a valid path 
    //    if (_fileName != "")
    //    {
    //      FileStream fls;
    //      fls = new FileStream(@_fileName, FileMode.Open, FileAccess.Read);
    //      //a byte array to read the image 
    //      byte[] blob = new byte[fls.Length];
    //      fls.Read(blob, 0, System.Convert.ToInt32(fls.Length));
    //      fls.Close();

    //      OracleCommand cmnd;
    //      string sql = "insert into emp(name,photo) values(" + txtid.Text + "," + "'" + txtname.Text + "'," + " :BlobParameter )";

    //      //insert the byte as oracle parameter of type blob 
    //      OracleParameter blobParameter = new OracleParameter();
    //      blobParameter.OracleType = OracleType.Blob;
    //      blobParameter.ParameterName = "BlobParameter";
    //      blobParameter.Value = blob;
    //      cmnd = _mySql.oracleCommand; cmnd.CommandText = sql;
    //      cmnd.Parameters.Add(blobParameter);
    //      cmnd.ExecuteNonQuery();
    //      //cmnd.Dispose(); 
    //      //conn.Close(); 
    //      //conn.Dispose(); 
    //    }
    //  }
    //  catch (Exception ex)
    //  {
    //    MessageBox.Show(ex.Message);
    //  }
    //}
  }
  public class CMySql
  {
    public const int DB_MYSQL = 0;
    public const int DB_ACCESS = 1;
    public const int DB_ORACLE = 2;

    object Connection;
    object Command;
    public OracleCommand oracleCommand
    {
      get
      {
        if (_type_db == DB_ORACLE) return (OracleCommand)Command;
        else return null;
      }
    }
    int _type_db;
    public CMySql()
    {
      Connection = null;
    }
    public int ConnectToDB(string server, string user, string passw, string db_name, int type_db)
    {
      string connStr = ""; ;
      _type_db = type_db;
      _type_db = 2;
      switch (_type_db)
      {
        case DB_MYSQL: connStr = "Driver={MySQL ODBC 3.51 Driver};Server=" + server +
                                  ";uid=" + user + "; pwd=" + passw + ";Database=" + db_name + ";Trusted_Connection=yes;";
          break;
        //case 1: connStr = "Driver={Microsoft Access Driver (*.mdb)};DBQ=" + db_name+".mdb";
        //        break;
        case DB_ACCESS: connStr = "Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=" + db_name;
          break;
        //  “Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=path to mdb/accdb file” 
        case DB_ORACLE:
          connStr = "User Id=APP_IDOIS;Password=kjhgTYUI8765;Data Source=db.tpu.ru:1521/TPU;";
          break;
        default: return -1;
      }
      try
      {
        if (_type_db == DB_ORACLE)
        {
          Connection = new OracleConnection(connStr);
          ((OracleConnection)Connection).Open();
          Command = new OracleCommand();
          ((OracleCommand)Command).Connection = (OracleConnection)Connection;

          DataTable dt = SqlSelect("select * from form").Tables[0];
          DataRow dr = dt.Rows[0];

        }
        else
        {
          Connection = new OdbcConnection(connStr);
          ((OdbcConnection)Connection).Open();
          Command = new OdbcCommand();
          ((OdbcCommand)Command).Connection = (OdbcConnection)Connection;
        }
        return 1;
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
        return -1;
      }
    }

    DataSet SqlSelectOracle(string sql, string table_name = "")
    {
      DataSet ds = new DataSet();

      OracleCommand command = new OracleCommand();
      OracleDataAdapter DA = new OracleDataAdapter();

      command.Connection = (OracleConnection)Connection;
      command.CommandText = sql;

      DA.SelectCommand = command;
      DA.TableMappings.Add(table_name, table_name);
      try
      {
        DA.Fill(ds);
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }
      return ds;
    }
    public DataSet SqlSelect(string sql, string table_name = "")
    {
      if (_type_db == DB_ORACLE) return SqlSelectOracle(sql, table_name);

      DataSet ds = new DataSet();

      OdbcCommand command = (OdbcCommand)Command;
      OdbcDataAdapter DA = new OdbcDataAdapter();

      command.CommandText = sql;

      DA.SelectCommand = command;
      DA.TableMappings.Add(table_name, table_name);
      try
      {
        DA.Fill(ds);
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }
      return ds;
    }
    public DataRow FindRow(DataTable table, string field, string value)
    {
      for (int i = 0; i < table.Rows.Count; i++)
      {
        if (table.Rows[i][table.Columns["id"]].ToString() == value)
        {
          return table.Rows[i];
        }
      }
      return null;
    }

    int SqlExecOracle(string sql)
    {
      try
      {
        OracleCommand command = (OracleCommand)Command;
        command.CommandText = sql;
        command.ExecuteNonQuery();
        return 1;
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
        return -1;
      }
    }
    public int SqlExec(string sql)
    {
      if (sql == "") return -1;
      if (_type_db == DB_ORACLE) return SqlExecOracle(sql);
      try
      {
        OdbcCommand command = (OdbcCommand)Command;
        command.CommandText = sql;
        command.ExecuteNonQuery();
        return 1;
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
        return -1;
      }
    }

    public static string MyValidate(string str)
    {
      StringBuilder s = new StringBuilder();
      s.Append(str);
      s.Replace("''", "###");
      s.Replace("'", "''");
      s.Replace("###", "''");

      s.Replace("\0", "###");

      return s.ToString();
    }
    public string ToDateTime(string date)
    {
      string res = date;
      if (_type_db == DB_ACCESS)
      {
        StringBuilder cb = new StringBuilder(date);
        cb.Replace('.', '/');
        return "#" + cb.ToString() + "#";
      }
      return res;
    }

    public int ToInt32(object value)
    {
      return Convert.ToInt32(value.ToString());
    }
    public bool ToBool(object value)
    {
      if (ToInt32(value) > 0) return true;
      else return false;
    }

  }
}
