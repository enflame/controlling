﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using MySql;
using GeneralClasses;
namespace KIP
{
  public class CStaffKIP
  {
    public static int IsAll = 0;
    public static int IsCurrent = 1;
    public static int IsFinished = 2;

    public string id_form;
    public string id_dep;
    //  public List<CKIP> kips;

    CMySql _mySql;
    public CStaffKIP(StaffInfo staffInfo, CMySql mySql)
    {
      this.id_form = staffInfo.id_form;
      this.id_dep = staffInfo.id_dep;

      _mySql = mySql;
    }

    public List<CKIP> GetKIPs(int SelectType)
    {
      string sql = "";
      if ((id_form == UserInfo.Info.id_form) & (id_dep == UserInfo.Info.id_dep))
        sql = "select id, date_begin, date_end, date_reg, date_begin_fact, date_end_fact, state, summary, descr, grade, grade_success, grade_unsuccess, k1, k2, level1, weight, id_dep_boss, id_form_boss, id_dep_clerk, id_form_clerk, targetyear from staff_kip where id_form_clerk=" + id_form + " and id_dep_clerk = " + id_dep + " and targetyear = " + UserInfo.TargetYear.ToString() + " and  state in (";
      else
        sql = "select id, date_begin, date_end, date_reg, date_begin_fact, date_end_fact, state, summary, descr, grade, grade_success, grade_unsuccess, k1, k2, level1, weight, id_dep_boss, id_form_boss, id_dep_clerk, id_form_clerk, targetyear from staff_kip where id_form_clerk=" + id_form + " and id_form_boss=" + UserInfo.Info.id_form + " and id_dep_clerk = " + id_dep + " and targetyear = " + UserInfo.TargetYear.ToString() + " and  state in (";

      List<CKIP> kips = new List<CKIP>();
      if (SelectType == CStaffKIP.IsAll) sql = sql + "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)";
      if (SelectType == CStaffKIP.IsCurrent) sql = sql + "0,1,3,4,6,10,11)";
      if (SelectType == CStaffKIP.IsFinished) sql = sql + "8)";

      DataTable dt = _mySql.SqlSelect(sql);
      foreach (DataRow dr in dt.Rows)
      {
        kips.Add(new CKIP(dr));
      }

      return kips;
    }
  }
  public class CKIP
  {
    //текущие
    public static int IsReg = 0;
    public static int IsAccepted = 1;
    public static int IsOnTransfer = 3;
    public static int IsAcceptTransfer = 4;
    public static int IsOnAcceptedRes = 6;
    public static int IsNear = 10;
    public static int IsOnDate = 11;
    //
    public static int IsNotAccepted = 2;
    public static int IsNotAcceptTransfer = 5;
    public static int IsNotAcceptedRes = 7;
    public static int IsAcceptedRes = 8;
    public static int IsCanceled = 9;
    public static int IsExpired = 12;

    public static string StateToString(int state)
    {
      string res = "Не известное состояние";
      if (state == CKIP.IsAccepted) return "Задача принята на выполнение";
      if (state == CKIP.IsAcceptedRes) return "Результаты работы приняты";
      if (state == CKIP.IsAcceptTransfer) return "Перенос даты одобрен";
      if (state == CKIP.IsCanceled) return "Задача отменена";
      if (state == CKIP.IsExpired) return "Задача просрочена";
      if (state == CKIP.IsNear) return "Срок выполнения задачи подходит к концу";
      if (state == CKIP.IsNotAccepted) return "Задача не принята исполнителем";
      if (state == CKIP.IsNotAcceptedRes) return "Результаты работы не приняты";
      if (state == CKIP.IsNotAcceptTransfer) return "Перенос даты отклонён";
      if (state == CKIP.IsOnAcceptedRes) return "На утверждении результатов";
      if (state == CKIP.IsOnDate) return "Сегодня заканчивается срок выполнения задачи";
      if (state == CKIP.IsOnTransfer) return "Запрошен перенос даты на более поздний срок";
      if (state == CKIP.IsReg) return "Задача на подтверждении у исполнителя";

      return res;
    }
    public static string StateToString1(int state)
    {
      string res = "Не известное состояние";
      if (state == CKIP.IsAcceptTransfer) return "Преренос даты одобрен";
      if (state == CKIP.IsCanceled) return "Задача отменена";
      if (state == CKIP.IsExpired) return "Задача просрочена";
      if (state == CKIP.IsNear) return "Срок выполнения задачи подходит к концу";
      if (state == CKIP.IsNotAccepted) return "Задача не принята исполнителем";
      if (state == CKIP.IsNotAcceptedRes) return "Результаты не приняты";
      if (state == CKIP.IsNotAcceptTransfer) return "Перенос даты отклонён";
      if (state == CKIP.IsOnAcceptedRes) return "Результаты работы не приняты";
      if (state == CKIP.IsOnDate) return "Сегодня заканчивается срок выполнения задачи";
      if (state == CKIP.IsOnTransfer) return "Запрошен перенос даты на более поздний срок";
      if (state == CKIP.IsReg) return "Задача на подтверждении у исполнителя";

      return res;
    }

    //
    bool IsBoss
    {
      get
      {

        if (UserInfo.Info.id_form == id_form_boss) return true;
        return false;
      }
    }
    public System.Windows.Visibility IsB_AcceptTask
    {
      get
      {
        if ((state == IsReg) & (UserInfo.Info.id_form == id_form_clerk)) return System.Windows.Visibility.Visible;
        return System.Windows.Visibility.Collapsed;
      }
    }
    public System.Windows.Visibility IsB_open_end_docVis
    {
      get
      {
        if (state != CKIP.IsOnAcceptedRes) return System.Windows.Visibility.Collapsed;
        if (state != CKIP.IsNotAcceptedRes) return System.Windows.Visibility.Collapsed;
        if (state != CKIP.IsAcceptedRes) return System.Windows.Visibility.Collapsed;
        return System.Windows.Visibility.Visible;
      }
    }
    public System.Windows.Visibility IsB_cancel_kipVis
    {
      get
      {
        if (!IsBoss) return System.Windows.Visibility.Collapsed;
        if (state == CKIP.IsOnAcceptedRes) return System.Windows.Visibility.Collapsed;
        if (state == CKIP.IsCanceled) return System.Windows.Visibility.Collapsed;
        return System.Windows.Visibility.Visible;
      }
    }
    public System.Windows.Visibility IsB_finish_kipVis
    {
      get
      {
        if (IsBoss) return System.Windows.Visibility.Collapsed;
        if (state == CKIP.IsAccepted) return System.Windows.Visibility.Visible;
        else return System.Windows.Visibility.Collapsed;
      }
    }
    public System.Windows.Visibility IsB_change_end_date_kip
    {
      get
      {
        if (IsBoss) return System.Windows.Visibility.Collapsed;
        if (state == CKIP.IsAccepted) return System.Windows.Visibility.Visible;
        return System.Windows.Visibility.Collapsed;
      }
    }
    public System.Windows.Visibility IsB_AcceptRes_kip
    {
      get
      {
        if (!IsBoss) return System.Windows.Visibility.Collapsed;
        if (state == CKIP.IsOnAcceptedRes) return System.Windows.Visibility.Visible;
        return System.Windows.Visibility.Collapsed;
      }
    }
 
    public System.Windows.Visibility IsB_finish_kipEn
    {
      get
      {
        if (!IsBoss) return System.Windows.Visibility.Collapsed;
        if (state == CKIP.IsOnAcceptedRes) return System.Windows.Visibility.Collapsed;
        if (state == CKIP.IsCanceled) return System.Windows.Visibility.Collapsed;
        return System.Windows.Visibility.Visible;
      }
    }


    public bool IsB_open_base_docEnabled { get { return true; } }

    public string ImageSourceState
    {
      get
      {
        if (state == CKIP.IsCanceled) return "/Modules;component/Images/button_cancel_22.png";
        if (state == CKIP.IsReg) return "/Modules;component/Images/kate_48.png";
        if (state == CKIP.IsOnTransfer) return "/Modules;component/Images/dialog_ok_48.png";
        if (state == CKIP.IsAcceptedRes) return "/Modules;component/Images/button_ok_22.png";
        return "/Modules;component/Images/show.png";

      }
    }
    public int RowColor
    {
      get
      {
        if (state == CKIP.IsCanceled) return -1;
        if (state == CKIP.IsAcceptedRes) return 3;
        TimeSpan dt5 = new TimeSpan(5, 0, 0, 0);
        TimeSpan dt1 = new TimeSpan(1, 0, 0, 0);
        TimeSpan dt = DateEnd - DateTime.Now;
        if (dt <= dt1) return 1; //вышел срок
        if (dt <= dt5) return 2;//прибижение срока к dt5
        return -1;
      }
    }
    //

    public int ID;

    public DateTime DateBegin { get; set; }
    public DateTime DateEnd { get; set; }


    public string DateBeginFact { get; set; }//дата принятия к выполнению
    public DateTime DateReg { get; set; }//дата создания документа
    public string DateEndFact { get; set; }
    public int state { get; set; }
    public string State
    {
      get
      {
        if (state == CKIP.IsReg) return "На подтверждении у исполнителя c " + DateReg.ToShortDateString();
        if (state == CKIP.IsAccepted) return "Принято к выполнению ";
        if (state == CKIP.IsAccepted) return "Не принято к выполнению ";

        if (state == CKIP.IsOnTransfer) return "Запрос на перенос конечной даты ";
        if (state == CKIP.IsAcceptTransfer) return "Перенос конечной даты утверждён";
        if (state == CKIP.IsNotAcceptTransfer) return "Перенос конечной даты не утверждён";


        if (state == CKIP.IsOnAcceptedRes) return "На утверждении результатов";
        if (state == CKIP.IsNotAcceptedRes) return "Результат не принят ";
        if (state == CKIP.IsAcceptedRes) return "Результат принят ";
        if (state == CKIP.IsCanceled) return "Отменена ";

        return "Не известно";
      }
    }

    public string Content { get; set; }//Содержание поручения
    public string Descr { get; set; }//Содержание поручения

    public int Level { get; set; }
    public double Grade { get; set; }//Балл
    public double K1 { get; set; }//(К1) Коэф. сложности
    public double Weight { get; set; }//
    public double K2 { get; set; }//(К2) Коэф. срочности
    public double Grade_success { get; set; }//Баллы для принятого отчёта
    public double Grade_unsuccess { get; set; }//Баллы для не выполненного отчёта

    string id_form_clerk;
    string id_dep_clerk;
    string id_form_boss;
    string id_dep_boss;

    public CKIP()
    {
      DateReg = DateTime.Now;
      DateBegin = DateTime.Now;
      DateEnd = DateTime.Now;
    }
    public CKIP(DataRow drtask)
      : base()
    {
      DateReg = (DateTime)drtask["Date_Reg"];
      DateBegin = (DateTime)drtask["Date_Begin"];
      DateEnd = (DateTime)drtask["Date_End"];
      //    DateApprovalRes = (DateTime)drtask["DateApprovalRes"];
      string s = drtask["DATE_begin_FACT"].ToString();
      if (s != "") DateBeginFact = ((DateTime)drtask["DATE_begin_FACT"]).ToShortDateString();
      else DateBeginFact = "----";
      s = drtask["DATE_END_FACT"].ToString();
      if (s != "") DateEndFact = ((DateTime)drtask["Date_End_Fact"]).ToShortDateString();
      else DateEndFact = "----";

      Content = drtask["Summary"].ToString();
      Descr = drtask["descr"].ToString();
      state = Convert.ToInt32(drtask["State"]);

      Level = Convert.ToInt32(drtask["Level1"]);
      Grade = Convert.ToDouble(drtask["Grade"]);
      K1 = Convert.ToDouble(drtask["K1"]);
      K2 = Convert.ToDouble(drtask["K2"]);
      Weight = Convert.ToDouble(drtask["Weight"]);
      Grade_success = Convert.ToDouble(drtask["Grade_success"]);
      Grade_unsuccess = Convert.ToDouble(drtask["Grade_unsuccess"]);

      ID = Convert.ToInt32(drtask["id"]);

      id_form_clerk = drtask["id_form_clerk"].ToString();
      id_dep_clerk = drtask["id_dep_clerk"].ToString();
      id_form_boss = drtask["id_form_boss"].ToString();
      id_dep_boss = drtask["id_dep_boss"].ToString();


    }

    public void SetEvent(int state, CMySql mySql)
    {
      string sql;
      string descr = "";

      string d = "";
      //       if (state == CKIP.IsAcceptTransfer) tb_State.Text = "Преренос даты одобрен";
      if (state == CKIP.IsCanceled) d = "Отменена задачи";
      //if (state == CKIP.IsExpired) tb_State.Text =  "Задача просрочена";
      //if (state == CKIP.IsNear) tb_State.Text =  "Срок выполнения задачи подходит к концу";
      if (state == CKIP.IsNotAccepted) d = "Отказ от задачи";
      if (state == CKIP.IsAcceptedRes) d = "Принять результаты";
      if (state == CKIP.IsNotAcceptedRes) d = "Отказ от результатов";
      if (state == CKIP.IsNotAcceptTransfer) d = "Отклонение переноса даты";
      if (state == CKIP.IsOnAcceptedRes) d = "Отправить резултаты на рассмотрение";
      //if (state == CKIP.IsOnDate) tb_State.Text =  "Сегодня заканчивается срок выполнения задачи";
      if (state == CKIP.IsOnTransfer) d = "Запрос переноса даты на более поздний срок";
      if (state == CKIP.IsReg)
      {
        // Обрабатывается при создании
      }
      if (state == CKIP.IsAccepted) descr = "Принятие задачи";

      TypeMessages.Insert(id_form_clerk.ToString(), id_form_boss.ToString(), ID.ToString(), TypeMessages.KIP, state.ToString(), "-1", d, mySql); 
      if (descr == "")
      {
        wndChangeState wnd = new wndChangeState(d);

        if (!(bool)wnd.ShowDialog()) return;

        descr = wnd.tb_text.Text;
        string pathToFile = wnd.fileSelect.PathToFile;
        if (pathToFile != "")
        {
          sql = "insert into kip_history  (dateevent, descr, kipid, state, docbase) values (SYSDATE, '" + descr + "'," + ID + ", " + state + ", :BlobParameter)";
          mySql.InsertBlobOracle(sql, pathToFile);
        }
        else
        {
          sql = "insert into kip_history (dateevent, descr, kipid, state) values (SYSDATE, '" + descr + "'," + ID + ", " + state + ")";
          mySql.SqlExec(sql);
        }
      }
      else
      {
        sql = "insert into kip_history (dateevent, descr, kipid, state) values (SYSDATE, '" + descr + "'," + ID + ", " + state + ")";
        mySql.SqlExec(sql);
      }

      sql = "update staff_kip set state = " + state + " where id = " + ID;
      mySql.SqlExec(sql);
    }


  }
  public class CKIPHistory
  {
    public int id;
    int state;
    public string State { get { return CKIP.StateToString(state); } }
    public string Descr { get; set; }
    public DateTime DateEvent { get; set; }

    public CKIPHistory(DataRow dr)
    {
      id = Convert.ToInt32(dr["id"]);
      state = Convert.ToInt32(dr["state"]);
      Descr = dr["descr"].ToString();
      DateEvent = (DateTime)dr["DateEvent"];
    }
  }
}
