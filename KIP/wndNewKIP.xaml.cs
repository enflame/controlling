﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using GeneralClasses;
using MySql;
using System.IO;
namespace KIP
{
  /// <summary>
  /// Логика взаимодействия для wndNewKIP.xaml
  /// </summary>
  public partial class wndNewKIP : Window
  {
    StaffInfo _staffInfo_responsible;

    public string file_doc_base;
    public string sql;
    public string body; //для почты

    CKIP _kip;
    CMySql _mySql;
    public wndNewKIP(StaffInfo staffInfo_responsible, CMySql mySql, CKIP kip = null)
    {
      InitializeComponent();

      _staffInfo_responsible = staffInfo_responsible;
      tb_chief.Text = UserInfo.Info.fio;
      tb_responsible.Text = staffInfo_responsible.fio;

      datePicker_reg_date.SelectedDate = DateTime.Now;
      datePicker_begin_date.SelectedDate = DateTime.Now;
      datePicker_end_date.SelectedDate = DateTime.Now;

      _kip = kip;
      _mySql = mySql;
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      capt_main.Text = "Основные параметры";
      capt_doc.Text = "Документы для основания";
      capt_text.Text = "Текст";
      capt_attachment.Text = "Вложения";
      capt_desc.Text = "Примечание";

      sp_actions.Visibility = System.Windows.Visibility.Collapsed;
      sp_newkip.Visibility = System.Windows.Visibility.Visible;


      if (_kip != null)
      {
        sp_actions.DataContext = _kip;
        sp_actions.Visibility = System.Windows.Visibility.Visible;
        sp_newkip.Visibility = System.Windows.Visibility.Collapsed;

        datePicker_begin_date.SelectedDate = _kip.DateBegin;
        datePicker_end_date.SelectedDate = _kip.DateEnd;
        datePicker_reg_date.SelectedDate = _kip.DateReg;
        tb_text.Text = _kip.Content;
        tb_desc.Text = _kip.Descr;
        UpDown_Grade.Value = (decimal)_kip.Grade;
        UpDown_Grade_success.Value = (decimal)_kip.Grade_success;
        UpDown_Grade_unsuccess.Value = (decimal)_kip.Grade_unsuccess;
        UpDown_K1.Value = (decimal)_kip.K1;
        UpDown_K2.Value = (decimal)_kip.K2;
        UpDown_Level.Value = (decimal)_kip.Level;
        UpDown_Weight.Value = (decimal)_kip.Weight;


      }
    }

    private void b_cancel_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }

    private void b_apply_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        file_doc_base = fs_DocBase.PathToFile;
        if (File.Exists(file_doc_base))
          sql = "insert into staff_kip(date_begin, date_end, date_reg, state, summary, descr, grade, grade_success, grade_unsuccess, k1, k2, level1, weight, id_dep_boss, id_form_boss, id_dep_clerk, id_form_clerk, targetyear, doc_base) values (" + CMySql.to_dateORA((DateTime)datePicker_begin_date.SelectedDate) + "," + CMySql.to_dateORA((DateTime)datePicker_end_date.SelectedDate) + "," + CMySql.to_dateORA((DateTime)datePicker_reg_date.SelectedDate) + ", " + CKIP.IsReg + ", '" + tb_text.Text + "', '" + tb_desc.Text + "', " + UpDown_Grade.Value + ", " + UpDown_Grade_success.Value + ", " + UpDown_Grade_unsuccess.Value + ", " + UpDown_K1.Value + ", " + UpDown_K2.Value + ", " + UpDown_Level.Value + ", " + UpDown_Weight.Value + ", " + UserInfo.Info.id_dep + ", " + UserInfo.Info.id_form + ", " + _staffInfo_responsible.id_dep + ", " + _staffInfo_responsible.id_form + ", " + UserInfo.TargetYear + ", :BlobParameter)";
        else
          sql = "insert into staff_kip(date_begin, date_end, date_reg, state, summary, descr, grade, grade_success, grade_unsuccess, k1, k2, level1, weight, id_dep_boss, id_form_boss, id_dep_clerk, id_form_clerk, targetyear) values (" + CMySql.to_dateORA((DateTime)datePicker_begin_date.SelectedDate) + "," + CMySql.to_dateORA((DateTime)datePicker_end_date.SelectedDate) + "," + CMySql.to_dateORA((DateTime)datePicker_reg_date.SelectedDate) + ", " + CKIP.IsReg + ", '" + tb_text.Text + "', '" + tb_desc.Text + "', " + UpDown_Grade.Value + ", " + UpDown_Grade_success.Value + ", " + UpDown_Grade_unsuccess.Value + ", " + UpDown_K1.Value + ", " + UpDown_K2.Value + ", " + UpDown_Level.Value + ", " + UpDown_Weight.Value + ", " + UserInfo.Info.id_dep + ", " + UserInfo.Info.id_form + ", " + _staffInfo_responsible.id_dep + ", " + _staffInfo_responsible.id_form + ", " + UserInfo.TargetYear + ")";

        body = UserInfo.Info.fio + ": \r\n" + tb_text.Text;

        if (file_doc_base == "") _mySql.SqlExec(sql);
        else _mySql.InsertBlobOracle(sql, file_doc_base);
        UserInfo.SendMail.SendMailMessage(_staffInfo_responsible.email, "КИП Новая задача от " + UserInfo.Info.fio, body);
        TypeMessages.Insert(_staffInfo_responsible.id_form.ToString(), UserInfo.Info.id_form.ToString(), "-1", TypeMessages.KIP, CKIP.IsReg.ToString(), "-1", "КИП Новая задача от " + UserInfo.Info.fio, _mySql);
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }

      DialogResult = true;
    }

    private void b_AcceptTask_Click(object sender, RoutedEventArgs e)
    {
      if (MessageBox.Show("Принять  задачу: \r\n" + _kip.Content, "Новая задача", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        _kip.SetEvent(CKIP.IsAccepted, _mySql);
        Close();
      }
    }
    private void b_finish_kip_Click(object sender, RoutedEventArgs e)
    {
      if (MessageBox.Show("Завершить задачу: \r\n" + _kip.Content, "Завершение", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        _kip.SetEvent(CKIP.IsOnAcceptedRes, _mySql);
        Close();
      }
    }
    private void b_cancel_kip_Click(object sender, RoutedEventArgs e)
    {
      if (MessageBox.Show("Отменить задачу: \r\n" + _kip.Content, "Отмена задания", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        _kip.SetEvent(CKIP.IsCanceled, _mySql);
        Close();
      }
    }
    private void b_NotAcceptRes_kip_Click(object sender, RoutedEventArgs e)
    {
      if (MessageBox.Show("Не принимать результаты: \r\n" + _kip.Content, "Завершение", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        _kip.SetEvent(CKIP.IsNotAcceptedRes, _mySql);
        Close();
      }
    }
    private void b_change_end_date_kip_Click(object sender, RoutedEventArgs e)
    {

    }
    private void b_AcceptRes_kip_Click(object sender, RoutedEventArgs e)
    {
      if (MessageBox.Show("Принять результаты: \r\n" + _kip.Content, "Завершение", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        _kip.SetEvent(CKIP.IsAcceptedRes, _mySql);
        Close();
      }

    }
  }
}
