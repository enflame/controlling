﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using GeneralClasses;
using MySql;
using System.Data;
namespace KIP
{
  /// <summary>
  /// Логика взаимодействия для wndKIPHistory.xaml
  /// </summary> 
  public partial class wndKIPHistory : Window
  {
    CMySql _mySql;
    public wndKIPHistory(CMySql mySql, int kipid, string content)
    {
      InitializeComponent();
      _mySql = mySql;
      string sql = "select * from kip_history where kipid=" + kipid + "order by id desc";
      List<CKIPHistory> d = new List<CKIPHistory>();
      foreach (DataRow dr in _mySql.SqlSelect(sql).Rows)
      {
        d.Add(new CKIPHistory(dr));
      }

      dg_smart_task.ItemsSource = d;
      txtKIPName.Text = content;
    }
    private void b_open_base_doc_Click(object sender, RoutedEventArgs e)
    {
      CKIPHistory kip = (CKIPHistory)dg_smart_task.SelectedItem;
      string sql = "select docbase from kip_history where id = " + kip.id ;
      string o = _mySql.SqlSelect(sql).Rows[0][0].ToString();
      if (o != "") CMSWord.OpenFromBlob(UserInfo.TempDir + "\\" + System.IO.Path.GetRandomFileName(), (byte[])_mySql.SqlSelect(sql).Rows[0][0]);
    }

    private void b_close_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }
  }
}
