﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KIP
{
  /// <summary>
  /// Логика взаимодействия для wndChangeState.xaml
  /// </summary>
  public partial class wndChangeState : Window
  {
    public wndChangeState(string state)
    {
      InitializeComponent();
      tb_State.Text = state;
    }

    private void b_ok_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = true;
    }

    private void b_cancek_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = false;
    }
  }
}
