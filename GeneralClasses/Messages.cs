﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MySql;
using System.Data;
namespace GeneralClasses
{
  public class TypeMessages
  {
    public static int SendPKMForApproved = 1;
    public static int FromActions = 2;
    public static int FromSC = 3;
    public static int SendPKMApproved = 4;

    //KIP
    public static int KIP = 5;
  
    public static void Insert(string id_form_for, string id_form_from, string idSC, int typeMess, string typeID, string typeID1, string mess, CMySql mySql)
    {
      string sql = "insert into messages (id_form_for, id_form_from, idSC, typeMess, typeID, typeID1, mess, dateMess, TargetYear) values(" +
        id_form_for + ", " + id_form_from + ", " + idSC + ", " + typeMess.ToString() + ", " + typeID + ", " + typeID1 +
        ",' " + mess + "', '" + DateTime.Now.ToShortDateString() + "', " + UserInfo.TargetYear + ")";
      mySql.SqlExec(sql);
    }
    public static DataTable GetMessages(bool isOwnMess, bool isAllMess, CMySql mySql)
    {
      string sql = "SELECT  U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO, " +
                   "messages.id, messages.idsc, messages.dateMess, messages.mess, messages.isEnded, messages.typeMess, messages.typeID, messages.typeID1 " +
                   "FROM messages " +
                   "INNER JOIN app_portal.v_tpu_users U ON U.LICHNOST_ID =messages.id_form_from " +
                   "where messages.id_form_for = " + UserInfo.Info.id_form + " and messages.targetyear = " + UserInfo.TargetYear.ToString();

      if (isOwnMess)
      {
        sql = "SELECT  U.FAMILIYA||' '||U.IMYA||' '||U.OTCHESTVO AS FIO, " +
                   "messages.id, messages.idsc, messages.dateMess, messages.mess, messages.isEnded, messages.typeMess, messages.typeID, messages.typeID1 " +
                   "FROM messages " +
                   "INNER JOIN app_portal.v_tpu_users U ON U.LICHNOST_ID =messages.id_form_from " +
                   "where messages.id_form_from = " + UserInfo.Info.id_form + " and messages.targetyear = " + UserInfo.TargetYear.ToString();
      }
      if (!isAllMess) sql += " and isEnded=0";

      return mySql.SqlSelect(sql);
    }

  
  }
  public class Messages
  {
  }
}
