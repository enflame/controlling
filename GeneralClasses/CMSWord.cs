﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using Word = Microsoft.Office.Interop.Word;

namespace GeneralClasses
{
  public class CMSWord
  {
    public static void OpenFromBlob(string file_name, byte[] blob)
    {
      FileStream FS = null;
      try
      {
        if (File.Exists(file_name)) File.Delete(file_name);
        FS = new FileStream(file_name, FileMode.Create);
        FS.Write(blob, 0, blob.Length);
        FS.Close();
        FS = null;
      }
      catch (Exception exc)
      {
        if (FS != null) FS.Close();
        throw exc;
      }

      Object missingObj = System.Reflection.Missing.Value;
      Object trueObj = true;
      Object falseObj = false;
      Word._Application _application = new Word.Application(); _application.Visible = true;
      Word._Document _document = null;
      try
      {
        _document = _application.Documents.Open(file_name);
      }
      catch (Exception exc)
      {
        _application.Quit(ref missingObj, ref  missingObj, ref missingObj);
        throw exc;
      }
    }

  }
}
