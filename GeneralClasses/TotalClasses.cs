﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace GeneralClasses
{
  public class StaffInfo
  {
    public string login { get; set; }
    public string id_position { get; set; }
    public string id_dep { get; set; }//PODRAZDELENIE_ID
    public string id_form { get; set; } //LICHNOST_ID

    public string fio { get; set; }//FIO
    public string position { get; set; }//DOLGNOST
    public string dep { get; set; }//NAZVANIE
    public string email { get; set; }

    public string KPI_ID { get; set; }

    public string TargetValue { get; set; }

  }
  public class UserInfo
  {
    public static StaffInfo Info = new StaffInfo();

    public static string id_head_all_department;
    public static string TargetYear;
    public static string server; //строка подключения
    public static DateTime ServerDate;
    public static TimeSpan TimeSpanBeforeWarn = new TimeSpan(3, 0, 0, 0); //3 дня
    public static DateTime DateWarn
    {
      get { return ServerDate.Add(TimeSpanBeforeWarn); }
    }

    public static bool isOnlyUserSC = true;

    public static string DefaultAppPath
    {
      get
      {
        return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
      }
    }
    public static string TempDir
    {
      get
      {
        string dir = DefaultAppPath + "\\Controlling";
        if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
        dir = dir + "\\Temp";
        if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
        return dir;
      }
    }
    public static string UserName
    {
      get
      {
        string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        int ind = userName.LastIndexOf('\\');
        userName = userName.Substring(ind + 1);
        return userName;
      }
    }

    public static Net.SendMail SendMail;

  }
  public class SCPeriod
  {
    public int id { get; set; }

    public string Period { get; set; }
    public DateTime DateBegin { get; set; }
    public DateTime DateEnd { get; set; }
    public double Plan { get; set; }
    public double Fact { get; set; }
    public double hiCriticalLevel { get; set; }
    public double lowCriticalLevel { get; set; }
    public string StrFormat { get; set; }
    public bool isFormula { get; set; }
    public bool isApproved { get; set; }

  }
  public class SC
  {
    //public string name;
    //public string period;
    //public string unit;
    //public int precision;
    //public double targetValue;
    //DateTime targetDate;
    //public double lowCriticalLevel;
    //public double hiCriticalLevel;
    //public int IDcontrolPerson;
    //public int IDresponsiblePerson;


    //public string controlPerson;
    //public string responsiblePerson;
  }

  class TotalClasses
  {
  }
}
